/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */

/* ---------------------------------------------------------------------
 *
 * Copyright (C) 2009 - 2015 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------

 *
 * Author: Abner Salgado, Texas A&M University 2009
 */

/**
 * Navier Stokes spectral problem
 * Author: Alexey Ozeritskiy
 */

// @sect3{Include files}

// We start by including all the necessary deal.II header files and some C++
// related ones. Each one of them has been discussed in previous tutorial
// programs, so we will not get into details here.
#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/function.h>
#include <deal.II/base/multithread_info.h>
#include <deal.II/base/parallel.h>
#include <deal.II/base/parameter_handler.h>
#include <deal.II/base/point.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/thread_management.h>
#include <deal.II/base/utilities.h>
#include <deal.II/base/work_stream.h>

#include <deal.II/lac/affine_constraints.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_gmres.h>
#include <deal.II/lac/sparse_direct.h>
#include <deal.II/lac/sparse_ilu.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

#include <deal.II/lac/arpack_solver.h>

#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/tria.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/manifold_lib.h>

#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_tools.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/vector_tools.h>

#include <cmath>
#include <fstream>
#include <iostream>

// Finally this is as in all previous programs:
namespace Step35 {
using namespace dealii;

// @sect3{Run time parameters}
//
// Since our method has several parameters that can be fine-tuned we put
// them into an external file, so that they can be determined at run-time.
//
// This includes, in particular, the formulation of the equation for the
// auxiliary variable $\phi$, for which we declare an <code>enum</code>.
// Next, we declare a class that is going to read and store all the
// parameters that our program needs to run.
namespace RunTimeParameters {
enum MethodFormulation { METHOD_STANDARD, METHOD_ROTATIONAL };

class Data_Storage {
  public:
    Data_Storage();
    ~Data_Storage();
    void read_data(const char* filename);
    MethodFormulation form;
    double initial_time, final_time, Reynolds, InnerVelocity;
    bool ReynoldsNearLapl;
    double dt;
    unsigned int n_global_refines, pressure_degree;
    unsigned int vel_max_iterations, vel_Krylov_size, vel_off_diagonals,
        vel_update_prec;
    double vel_eps, vel_diag_strength;
    bool verbose;
    unsigned int output_interval;
    std::string mesh_file = "grid.1.out.ucd"; // [0,pi]x[0,pi]

    // periodicity
    int b1;
    int b2;
    int dir;
    bool cylinder = false;

  protected:
    ParameterHandler prm;
};

// In the constructor of this class we declare all the parameters. The
// details of how this works have been discussed elsewhere, for example in
// step-19 and step-29.
Data_Storage::Data_Storage() {
    prm.declare_entry(
        "Method_Form",
        "rotational",
        Patterns::Selection("rotational|standard"),
        " Used to select the type of method that we are going "
        "to use. ");
    prm.enter_subsection("Physical data");
    {
        prm.declare_entry(
            "initial_time",
            "0.",
            Patterns::Double(0.),
            " The initial time of the simulation. ");
        prm.declare_entry(
            "final_time",
            "1.",
            Patterns::Double(0.),
            " The final time of the simulation. ");
        prm.declare_entry(
            "Reynolds", "1.", Patterns::Double(0.), " The Reynolds number. ");
        prm.declare_entry(
            "ReynoldsNearLapl", "true", Patterns::Bool(), " Reynolds near lapl or adv?");
        prm.declare_entry(
            "InnerVelocity", "1.", Patterns::Double(0.), " Inner cylinder velocity");
    }
    prm.leave_subsection();

    prm.enter_subsection("Time step data");
    {
        prm.declare_entry(
            "dt", "5e-4", Patterns::Double(0.), " The time step size. ");
    }
    prm.leave_subsection();

    prm.enter_subsection("Space discretization");
    {
        prm.declare_entry(
            "n_of_refines",
            "0",
            Patterns::Integer(0, 15),
            " The number of global refines we do on the mesh. ");
        prm.declare_entry(
            "pressure_fe_degree",
            "1",
            Patterns::Integer(1, 5),
            " The polynomial degree for the pressure space. ");
    }
    prm.leave_subsection();

    prm.enter_subsection("Data solve velocity");
    {
        prm.declare_entry(
            "max_iterations",
            "1000",
            Patterns::Integer(1, 1000),
            " The maximal number of iterations GMRES must make. ");
        prm.declare_entry(
            "eps", "1e-12", Patterns::Double(0.), " The stopping criterion. ");
        prm.declare_entry(
            "Krylov_size",
            "30",
            Patterns::Integer(1),
            " The size of the Krylov subspace to be used. ");
        prm.declare_entry(
            "off_diagonals",
            "60",
            Patterns::Integer(0),
            " The number of off-diagonal elements ILU must "
            "compute. ");
        prm.declare_entry(
            "diag_strength",
            "0.01",
            Patterns::Double(0.),
            " Diagonal strengthening coefficient. ");
        prm.declare_entry(
            "update_prec",
            "15",
            Patterns::Integer(1),
            " This number indicates how often we need to "
            "update the preconditioner");
    }
    prm.leave_subsection();

    prm.declare_entry(
        "verbose",
        "true",
        Patterns::Bool(),
        " This indicates whether the output of the solution "
        "process should be verbose. ");

    prm.declare_entry(
        "output_interval",
        "1",
        Patterns::Integer(1),
        " This indicates between how many time steps we print "
        "the solution. ");
}

Data_Storage::~Data_Storage() {}

void Data_Storage::read_data(const char* filename) {
    std::ifstream file(filename);
    AssertThrow(file, ExcFileNotOpen(filename));

    prm.parse_input(file);

    if (prm.get("Method_Form") == std::string("rotational"))
        form = METHOD_ROTATIONAL;
    else
        form = METHOD_STANDARD;

    prm.enter_subsection("Physical data");
    {
        initial_time = prm.get_double("initial_time");
        final_time = prm.get_double("final_time");
        Reynolds = prm.get_double("Reynolds");
        ReynoldsNearLapl = prm.get_bool("ReynoldsNearLapl");
        InnerVelocity = prm.get_double("InnerVelocity");
    }
    prm.leave_subsection();

    prm.enter_subsection("Time step data");
    { dt = prm.get_double("dt"); }
    prm.leave_subsection();

    prm.enter_subsection("Space discretization");
    {
        n_global_refines = prm.get_integer("n_of_refines");
        pressure_degree = prm.get_integer("pressure_fe_degree");
    }
    prm.leave_subsection();

    prm.enter_subsection("Data solve velocity");
    {
        vel_max_iterations = prm.get_integer("max_iterations");
        vel_eps = prm.get_double("eps");
        vel_Krylov_size = prm.get_integer("Krylov_size");
        vel_off_diagonals = prm.get_integer("off_diagonals");
        vel_diag_strength = prm.get_double("diag_strength");
        vel_update_prec = prm.get_integer("update_prec");
    }
    prm.leave_subsection();

    verbose = prm.get_bool("verbose");

    output_interval = prm.get_integer("output_interval");
}
} // namespace RunTimeParameters

// @sect3{Equation data}

// In the next namespace, we declare the initial and boundary conditions:
namespace EquationData {
// As we have chosen a completely decoupled formulation, we will not take
// advantage of deal.II's capabilities to handle vector valued
// problems. We do, however, want to use an interface for the equation
// data that is somehow dimension independent. To be able to do that, our
// functions should be able to know on which spatial component we are
// currently working, and we should be able to have a common interface to
// do that. The following class is an attempt in that direction.
template<int dim> class MultiComponentFunction : public Function<dim> {
  public:
    MultiComponentFunction(const double initial_time = 0.);
    void set_component(const unsigned int d);

  protected:
    unsigned int comp;
};

template<int dim>
MultiComponentFunction<dim>::MultiComponentFunction(const double initial_time)
    : Function<dim>(1, initial_time), comp(0) {}

template<int dim>
void MultiComponentFunction<dim>::set_component(const unsigned int d) {
    Assert(d < dim, ExcIndexRange(d, 0, dim));
    comp = d;
}

template<int dim> class ZeroMultiComponent : public MultiComponentFunction<dim> {
  public:
    ZeroMultiComponent(const double initial_time = 0.0);

    virtual double
    value(const Point<dim>& p, const unsigned int component = 0) const;

    virtual void value_list(
        const std::vector<Point<dim>>& points,
        std::vector<double>& values,
        const unsigned int component = 0) const;
};

template<int dim>
ZeroMultiComponent<dim>::ZeroMultiComponent(const double initial_time)
    : MultiComponentFunction<dim>(initial_time) {}

template<int dim>
void ZeroMultiComponent<dim>::value_list(
    const std::vector<Point<dim>>& points,
    std::vector<double>& values,
    const unsigned int) const {
    const unsigned int n_points = points.size();
    Assert(
        values.size() == n_points,
        ExcDimensionMismatch(values.size(), n_points));
    for (unsigned int i = 0; i < n_points; ++i)
        values[i] = ZeroMultiComponent<dim>::value(points[i]);
}

template<int dim>
double ZeroMultiComponent<dim>::value(const Point<dim>& , const unsigned int) const {
    return 0.0;
}

// With this class defined, we declare classes that describe the boundary
// conditions for velocity and pressure:
template<int dim> class Velocity : public MultiComponentFunction<dim> {
  public:
    Velocity(const double initial_time = 0.0);

    virtual double
    value(const Point<dim>& p, const unsigned int component = 0) const;

    virtual void value_list(
        const std::vector<Point<dim>>& points,
        std::vector<double>& values,
        const unsigned int component = 0) const;
};

template<int dim>
Velocity<dim>::Velocity(const double initial_time)
    : MultiComponentFunction<dim>(initial_time) {}

template<int dim>
void Velocity<dim>::value_list(
    const std::vector<Point<dim>>& points,
    std::vector<double>& values,
    const unsigned int) const {
    const unsigned int n_points = points.size();
    Assert(
        values.size() == n_points,
        ExcDimensionMismatch(values.size(), n_points));
    for (unsigned int i = 0; i < n_points; ++i)
        values[i] = Velocity<dim>::value(points[i]);
}

template<int dim>
double Velocity<dim>::value(const Point<dim>& p, const unsigned int) const {
    // TODO: div must be zero
    auto x = p(0); auto y = p(1); auto t = this->get_time();
    if (dim == 2) {
        if (this->comp == 0) {
            return -cos(x) * sin(y) * exp(-2.0 * t);
        } else if (this->comp == 1) {
            return sin(x) * cos(y) * exp(-2.0 * t);
        }
    } else if (dim == 3) {
        auto z = p(2);
        if (this->comp == 0) {
            return (sin(z)+cos(y))*exp(-t);
        } else if (this->comp == 1) {
            return (sin(x)+cos(z))*exp(-t);
        } else if (this->comp == 2) {
            return (sin(y)+cos(x))*exp(-t);
        }
    }
    assert(false);
    return -1.0;
}

template<int dim> class Pressure : public Function<dim> {
    const double Re;
  public:
    Pressure(const double Re, const double initial_time = 0.0);

    virtual double
    value(const Point<dim>& p, const unsigned int component = 0) const;

    virtual void value_list(
        const std::vector<Point<dim>>& points,
        std::vector<double>& values,
        const unsigned int component = 0) const;
};

template<int dim>
Pressure<dim>::Pressure(const double Re, const double initial_time)
    : Function<dim>(1, initial_time)
    , Re(Re)
{ }

template<int dim>
double Pressure<dim>::value(const Point<dim>& p, const unsigned int) const {
    auto x = p(0); auto y = p(1); auto t = this->get_time();
    if (dim == 2) {
        return -Re * 1.0/4.0 * (cos(2*x)+cos(2*y))*exp(-4.0*t);
    }
    assert(dim == 3);
    auto z= p(2);
    return -Re*(cos(x)*sin(y)+sin(x)+cos(z)+cos(y)*sin(z))*exp(-2.0*t);
}

template<int dim>
void Pressure<dim>::value_list(
    const std::vector<Point<dim>>& points,
    std::vector<double>& values,
    const unsigned int) const {
    const unsigned int n_points = points.size();
    Assert(
        values.size() == n_points,
        ExcDimensionMismatch(values.size(), n_points));
    for (unsigned int i = 0; i < n_points; ++i)
        values[i] = Pressure<dim>::value(points[i]);
}

/************************************************************************************/

template<int dim> class CouettePressure : public Function<dim> {
    const double U1;
    const double R1;
    const double R2;
    const QGauss<1> quad;

  public:
    CouettePressure(
        const double U1,
        const double R1 = M_PI/2.,
        const double R2 = 3.*M_PI/2.);

    virtual double
    value(const Point<dim>& p, const unsigned int component = 0) const;

    virtual void value_list(
        const std::vector<Point<dim>>& points,
        std::vector<double>& values,
        const unsigned int component = 0) const;
};

template<int dim>
CouettePressure<dim>::CouettePressure(const double U1, const double R1, const double R2)
    : Function<dim>(1, 0)
    , U1(U1)
    , R1(R1)
    , R2(R2)
    , quad(10)
{ }

template<int dim>
double CouettePressure<dim>::value(const Point<dim>& p, const unsigned int) const {
    auto x = p(0); auto y = p(1); // z unused
    // int v^2/r, r=r0..r

    auto points = quad.get_points();
    auto weights = quad.get_weights();

    double R = sqrt(x*x+y*y);

    double sum = 0.0;

    // интегрируем от R1 до R
    for (unsigned i = 0; i < points.size(); ++i) {
        double r = R1 + (R-R1)*points[i](0);
        double v = (U1 * R1 / (R2*R2 - R1*R1)) * (R2*R2 / r - r); // L-L 18.3 p.86
        sum += v*v/r*weights[i];
    }

    return sum;
}

template<int dim>
void CouettePressure<dim>::value_list(
    const std::vector<Point<dim>>& points,
    std::vector<double>& values,
    const unsigned int) const {
    const unsigned int n_points = points.size();
    Assert(
        values.size() == n_points,
        ExcDimensionMismatch(values.size(), n_points));
    for (unsigned int i = 0; i < n_points; ++i)
        values[i] = CouettePressure<dim>::value(points[i]);
}

/************************************************************************************/

template<int dim> class InnerCircleVelocity : public MultiComponentFunction<dim> {
    const double U;
  public:
    InnerCircleVelocity(const double U, const double initial_time = 0.0);

    virtual double
    value(const Point<dim>& p, const unsigned int component = 0) const;

    virtual void value_list(
        const std::vector<Point<dim>>& points,
        std::vector<double>& values,
        const unsigned int component = 0) const;
};

template<int dim>
InnerCircleVelocity<dim>::InnerCircleVelocity(const double U, const double initial_time)
    : MultiComponentFunction<dim>(initial_time), U(U) {}

template<int dim>
void InnerCircleVelocity<dim>::value_list(
    const std::vector<Point<dim>>& points,
    std::vector<double>& values,
    const unsigned int) const {
    const unsigned int n_points = points.size();
    Assert(
        values.size() == n_points,
        ExcDimensionMismatch(values.size(), n_points));
    for (unsigned int i = 0; i < n_points; ++i)
        values[i] = InnerCircleVelocity<dim>::value(points[i]);
}

template<int dim>
double InnerCircleVelocity<dim>::value(const Point<dim>& p, const unsigned int) const {
    auto x = p(0); auto y = p(1);
    if (dim == 2) {
        assert(false);
    }

    double ux = y;
    double uy = -x;
    auto k = U / sqrt(ux*ux + uy*uy);
    ux *= k;
    uy *= k;

    if (this->comp == 0) {
        return ux;
    } else if (this->comp == 1) {
        return uy;
    } else if (this->comp == 2) {
        return 0;
    }

    return 0;
}

template<int dim> class CouetteVelocity : public MultiComponentFunction<dim> {
    const double U1;
    const double R1;
    const double R2;
  public:
    CouetteVelocity(
        const double U1,
        const double R1 = M_PI/2.,
        const double R2 = 3.*M_PI/2.,
        const double initial_time = 0.0);

    virtual double
    value(const Point<dim>& p, const unsigned int component = 0) const;

    virtual void value_list(
        const std::vector<Point<dim>>& points,
        std::vector<double>& values,
        const unsigned int component = 0) const;
};

template<int dim>
CouetteVelocity<dim>::CouetteVelocity(
    const double U1,
    const double R1,
    const double R2,
    const double initial_time)
    : MultiComponentFunction<dim>(initial_time)
    , U1(U1)
    , R1(R1)
    , R2(R2)
{ }

template<int dim>
void CouetteVelocity<dim>::value_list(
    const std::vector<Point<dim>>& points,
    std::vector<double>& values,
    const unsigned int) const {
    const unsigned int n_points = points.size();
    Assert(
        values.size() == n_points,
        ExcDimensionMismatch(values.size(), n_points));
    for (unsigned int i = 0; i < n_points; ++i)
        values[i] = CouetteVelocity<dim>::value(points[i]);
}

template<int dim>
double CouetteVelocity<dim>::value(const Point<dim>& p, const unsigned int) const {
    const double x = p(0), y = p(1);
    const double r = sqrt(x*x+y*y);
    const double phi = [&](){
        if (fabs(x) < 1e-12) {
            if (y > 0) {
                return M_PI/2.;
            } else {
                return 3.*M_PI/2.;
            }
        } else if (x > 0) {
            if (y < 0) {
                return atan(y/x) + 2.*M_PI;
            } else /*if (y >= 0)*/ {
                return atan(y/x);
            }
        } else /*if (x < 0)*/ {
            return atan(y/x) + M_PI;
        }
        assert(false);
        return -1.;
    } ();

    const double Uphi = (U1 * R1 / (R2*R2 - R1*R1)) * (R2*R2 / r - r);

    const double ux =  Uphi*sin(phi);
    const double uy = -Uphi*cos(phi);
    const double uz = 0;

    if (this->comp == 0) {
        return ux;
    } else if (this->comp == 1) {
        return uy;
    } else if (this->comp == 2) {
        return uz;
    }
    assert(false);
    return -1.;
}

} // namespace EquationData

// @sect3{The <code>NavierStokesSpectral</code> class}

// Now for the main class of the program. It implements the various versions
// of the projection method for Navier-Stokes equations.  The names for all
// the methods and member variables should be self-explanatory, taking into
// account the implementation details given in the introduction.
template<int dim> class NavierStokesSpectral {
  public:
    NavierStokesSpectral(const RunTimeParameters::Data_Storage& data);

    void spectral(const bool verbose = false);

  protected:
    const RunTimeParameters::Data_Storage& data_storage;
    RunTimeParameters::MethodFormulation type;

    const unsigned int deg;
    const double dt;
    const double t_0, T, Re, InnerVelocity;
    bool LaplRe;

    Functions::ZeroFunction<dim> zero_func1;
    EquationData::ZeroMultiComponent<dim> zero_func;
    EquationData::CouetteVelocity<dim> cuette_func;
    EquationData::CouettePressure<dim> cuette_pres_func;
    std::map<types::global_dof_index, double> boundary_values;
    std::vector<types::boundary_id> boundary_ids;

    Triangulation<dim> triangulation;

    FE_Q<dim> fe_velocity;
    FE_Q<dim> fe_pressure;
    FESystem<dim> fe_velocity_joint;

    DoFHandler<dim> dof_handler_velocity;
    DoFHandler<dim> dof_handler_pressure;
    DoFHandler<dim> dof_handler_velocity_joint;
    MappingQ<dim> mapping;

    QGauss<dim> quadrature_pressure;
    QGauss<dim> quadrature_velocity;

    SparsityPattern sparsity_pattern_velocity;
    SparsityPattern sparsity_pattern_velocity_joint;
    SparsityPattern sparsity_pattern_pressure;
    SparsityPattern sparsity_pattern_pres_vel;

    SparseMatrix<double> vel_Laplace_plus_Mass;
    SparseMatrix<double> vel_it_matrix[dim];
    SparseMatrix<double> vel_Mass;
    SparseMatrix<double> vel_Laplace;
    SparseMatrix<double> vel_Advection;
    SparseMatrix<double> system_matrix;
    SparseMatrix<double> pres_Laplace;
    AffineConstraints<double> pres_constraints;
    AffineConstraints<double> vel_constraints;
    AffineConstraints<double> vel_constraints_joint;
    SparseMatrix<double> pres_Mass;
    SparseMatrix<double> pres_Diff[dim];
    SparseMatrix<double> vel_Diff[dim];
    SparseMatrix<double> pres_iterative;

    std::vector<
        GridTools::PeriodicFacePair<typename DoFHandler<dim>::cell_iterator>>
        pres_periodicity_vector;
    std::vector<
        GridTools::PeriodicFacePair<typename DoFHandler<dim>::cell_iterator>>
        vel_periodicity_vector;
    std::vector<
        GridTools::PeriodicFacePair<typename DoFHandler<dim>::cell_iterator>>
        vel_periodicity_vector_joint;

    Vector<double> pres_n;
    Vector<double> pres_n_minus_1;
    Vector<double> phi_n;
    Vector<double> phi_n_minus_1;
    Vector<double> u_n[dim];
    Vector<double> u_n_minus_1[dim];
    Vector<double> u_star[dim];
    Vector<double> force[dim];
    Vector<double> v_tmp;
    Vector<double> pres_tmp;
    Vector<double> rot_u;
    Vector<double> u_starj;

    SparseILU<double> prec_velocity[dim];
    SparseILU<double> prec_pres_Laplace;
    SparseDirectUMFPACK prec_mass;
    SparseDirectUMFPACK prec_vel_mass;

    DeclException2(
        ExcInvalidTimeStep,
        double,
        double,
        << " The time step " << arg1 << " is out of range." << std::endl
        << " The permitted range is (0," << arg2 << "]");

    void create_triangulation_and_dofs(const unsigned int n_refines);

    void initialize();

    void interpolate_velocity();

    void diffusion_step(const bool reinit_prec);

    void projection_step(const bool reinit_prec);

    void update_pressure(const bool reinit_prec);

  private:
    unsigned int vel_max_its;
    unsigned int vel_Krylov_size;
    unsigned int vel_off_diagonals;
    unsigned int vel_update_prec;
    double vel_eps;
    double vel_diag_strength;

    int periodic_b1;
    int periodic_b2;
    int periodic_dir;
    bool cylinder;

    void initialize_velocity_matrices();

    void initialize_pressure_matrices();

    // The next few structures and functions are for doing various things in
    // parallel. They follow the scheme laid out in @ref threads, using the
    // WorkStream class. As explained there, this requires us to declare two
    // structures for each of the assemblers, a per-task data and a scratch
    // data structure. These are then handed over to functions that assemble
    // local contributions and that copy these local contributions to the
    // global objects.
    //
    // One of the things that are specific to this program is that we don't
    // just have a single DoFHandler object that represents both the
    // velocities and the pressure, but we use individual DoFHandler objects
    // for these two kinds of variables. We pay for this optimization when we
    // want to assemble terms that involve both variables, such as the
    // divergence of the velocity and the gradient of the pressure, times the
    // respective test functions. When doing so, we can't just anymore use a
    // single FEValues object, but rather we need two, and they need to be
    // initialized with cell iterators that point to the same cell in the
    // triangulation but different DoFHandlers.
    //
    // To do this in practice, we declare a "synchronous" iterator -- an
    // object that internally consists of several (in our case two) iterators,
    // and each time the synchronous iteration is moved up one step, each of
    // the iterators stored internally is moved up one step as well, thereby
    // always staying in sync. As it so happens, there is a deal.II class that
    // facilitates this sort of thing.
    typedef std::tuple<
        typename DoFHandler<dim>::active_cell_iterator,
        typename DoFHandler<dim>::active_cell_iterator>
        IteratorTuple;

    typedef SynchronousIterators<IteratorTuple> IteratorPair;

    void initialize_gradient_operator();

    struct InitGradPerTaskData {
        unsigned int d;
        unsigned int vel_dpc;
        unsigned int pres_dpc;
        FullMatrix<double> local_grad;
        std::vector<types::global_dof_index> vel_local_dof_indices;
        std::vector<types::global_dof_index> pres_local_dof_indices;

        InitGradPerTaskData(
            const unsigned int dd,
            const unsigned int vdpc,
            const unsigned int pdpc)
            : d(dd), vel_dpc(vdpc), pres_dpc(pdpc), local_grad(vdpc, pdpc),
              vel_local_dof_indices(vdpc), pres_local_dof_indices(pdpc) {}
    };

    struct InitGradScratchData {
        unsigned int nqp;
        FEValues<dim> fe_val_vel;
        FEValues<dim> fe_val_pres;
        const MappingQ<dim>& mapping;
        InitGradScratchData(
            const MappingQ<dim>& mapping,
            const FE_Q<dim>& fe_v,
            const FE_Q<dim>& fe_p,
            const QGauss<dim>& quad,
            const UpdateFlags flags_v,
            const UpdateFlags flags_p)
            : nqp(quad.size()),
              fe_val_vel(mapping, fe_v, quad, flags_v),
              fe_val_pres(mapping, fe_p, quad, flags_p),
              mapping(mapping) {}
        InitGradScratchData(const InitGradScratchData& data)
            : nqp(data.nqp),
              fe_val_vel(
                  data.mapping,
                  data.fe_val_vel.get_fe(),
                  data.fe_val_vel.get_quadrature(),
                  data.fe_val_vel.get_update_flags()),
              fe_val_pres(
                  data.mapping,
                  data.fe_val_pres.get_fe(),
                  data.fe_val_pres.get_quadrature(),
                  data.fe_val_pres.get_update_flags()),
              mapping(data.mapping) {}
    };

    void assemble_one_cell_of_gradient(
        const IteratorPair& SI,
        InitGradScratchData& scratch,
        InitGradPerTaskData& data);

    void copy_gradient_local_to_global(const InitGradPerTaskData& data);
    void copy_gradient_local_to_global2(const InitGradPerTaskData& data);

    // The same general layout also applies to the following classes and
    // functions implementing the assembly of the advection term:
    void assemble_advection_term();

    void assemble_system_matrix();

    struct AdvectionPerTaskData {
        FullMatrix<double> local_advection;
        std::vector<types::global_dof_index> local_dof_indices;
        AdvectionPerTaskData(const unsigned int dpc)
            : local_advection(dpc, dpc), local_dof_indices(dpc) {}
    };

    struct AdvectionScratchData {
        unsigned int nqp;
        unsigned int dpc;
        std::vector<Point<dim>> u_star_local;
        std::vector<Tensor<1, dim>> grad_u_star;
        std::vector<double> u_star_tmp;
        FEValues<dim> fe_val;
        const MappingQ<dim>& mapping;
        AdvectionScratchData(
            const MappingQ<dim>& mapping,
            const FE_Q<dim>& fe,
            const QGauss<dim>& quad,
            const UpdateFlags flags)
            : nqp(quad.size()), dpc(fe.dofs_per_cell), u_star_local(nqp),
              grad_u_star(nqp), u_star_tmp(nqp), fe_val(mapping, fe, quad, flags),
              mapping(mapping) {}

        AdvectionScratchData(const AdvectionScratchData& data)
            : nqp(data.nqp), dpc(data.dpc), u_star_local(nqp), grad_u_star(nqp),
              u_star_tmp(nqp), fe_val(data.mapping,
                                   data.fe_val.get_fe(),
                                   data.fe_val.get_quadrature(),
                                   data.fe_val.get_update_flags()),
              mapping(data.mapping) {}
    };

    void assemble_one_cell_of_advection(
        const typename DoFHandler<dim>::active_cell_iterator& cell,
        AdvectionScratchData& scratch,
        AdvectionPerTaskData& data);

    void copy_advection_local_to_global(const AdvectionPerTaskData& data);

    // The final few functions implement the diffusion solve as well as
    // postprocessing the output, including computing the curl of the
    // velocity:
    void diffusion_component_solve(const unsigned int d);

    void output_results(const unsigned int step, const Vector<double>* u_n, const std::string& prefix = "solution-");

    void assemble_vorticity(const bool reinit_prec);

    void compute_pressure_gradient(Vector<double>& output, const Vector<double>& input, const unsigned int d);

    void join_velocity(Vector<double>& joint, const Vector<double>* u);

    void disjoin_velocity(Vector<double>* u, const Vector<double>& joint);
};

// @sect4{ <code>NavierStokesSpectral::NavierStokesSpectral</code> }

// In the constructor, we just read all the data from the
// <code>Data_Storage</code> object that is passed as an argument, verify
// that the data we read is reasonable and, finally, create the
// triangulation and load the initial data.
template<int dim>
NavierStokesSpectral<dim>::NavierStokesSpectral(
    const RunTimeParameters::Data_Storage& data)
    : data_storage(data), type(data.form), deg(data.pressure_degree), dt(data.dt),
      t_0(data.initial_time), T(data.final_time), Re(data.Reynolds),
      InnerVelocity(data.InnerVelocity), LaplRe(data.ReynoldsNearLapl),
      cuette_func(InnerVelocity), cuette_pres_func(InnerVelocity),
      fe_velocity(deg + 1), fe_pressure(deg),
      fe_velocity_joint(fe_velocity, dim),
      dof_handler_velocity(triangulation), dof_handler_pressure(triangulation),
      dof_handler_velocity_joint(triangulation),
      mapping(deg + 1),
      quadrature_pressure(deg + 1), quadrature_velocity(deg + 2),
      vel_max_its(data.vel_max_iterations),
      vel_Krylov_size(data.vel_Krylov_size),
      vel_off_diagonals(data.vel_off_diagonals),
      vel_update_prec(data.vel_update_prec), vel_eps(data.vel_eps),
      vel_diag_strength(data.vel_diag_strength),
      periodic_b1(data.b1), periodic_b2(data.b2), periodic_dir(data.dir),
      cylinder(data.cylinder) {
    if (deg < 1)
        std::cout << " WARNING: The chosen pair of finite element spaces is "
                     "not stable."
                  << std::endl
                  << " The obtained results will be nonsense" << std::endl;

    AssertThrow(!((dt <= 0.) || (dt > .5 * T)), ExcInvalidTimeStep(dt, .5 * T));

    create_triangulation_and_dofs(data.n_global_refines);
    initialize();
}

// @sect4{ <code>NavierStokesSpectral::create_triangulation_and_dofs</code> }

// The method that creates the triangulation and refines it the needed
// number of times.  After creating the triangulation, it creates the mesh
// dependent data, i.e. it distributes degrees of freedom and renumbers
// them, and initializes the matrices and vectors that we will use.
template<int dim>
void NavierStokesSpectral<dim>::create_triangulation_and_dofs(
    const unsigned int n_refines) {

    GridIn<dim> grid_in;
    grid_in.attach_triangulation(triangulation);

    {
        std::string filename = data_storage.mesh_file;
        std::ifstream file(filename.c_str());
        Assert(file, ExcFileNotOpen(filename.c_str()));
        grid_in.read_ucd(file);
    }

    if (cylinder) {
        CylindricalManifold<dim> cyl_manifold(2);
        // inner
        //triangulation.set_all_manifold_ids_on_boundary(4, 0);
        // outer
        //triangulation.set_all_manifold_ids_on_boundary(5, 0);
        triangulation.set_all_manifold_ids(0);
        triangulation.set_manifold(0, cyl_manifold);
    }

    std::cout << "Number of refines = " << n_refines << std::endl;
    triangulation.refine_global(n_refines);
    std::cout << "Number of active cells: " << triangulation.n_active_cells()
              << std::endl;

    boundary_ids = triangulation.get_boundary_ids();

    dof_handler_velocity.distribute_dofs(fe_velocity);
    DoFRenumbering::boost::Cuthill_McKee(dof_handler_velocity);
    dof_handler_pressure.distribute_dofs(fe_pressure);
    DoFRenumbering::boost::Cuthill_McKee(dof_handler_pressure);

    dof_handler_velocity_joint.distribute_dofs(fe_velocity_joint);

    initialize_velocity_matrices();
    initialize_pressure_matrices();
    initialize_gradient_operator();

    u_starj.reinit(dof_handler_velocity_joint.n_dofs());

    pres_n.reinit(dof_handler_pressure.n_dofs());
    pres_n_minus_1.reinit(dof_handler_pressure.n_dofs());
    phi_n.reinit(dof_handler_pressure.n_dofs());
    phi_n_minus_1.reinit(dof_handler_pressure.n_dofs());
    pres_tmp.reinit(dof_handler_pressure.n_dofs());
    for (unsigned int d = 0; d < dim; ++d) {
        u_n[d].reinit(dof_handler_velocity.n_dofs());
        u_n_minus_1[d].reinit(dof_handler_velocity.n_dofs());
        u_star[d].reinit(dof_handler_velocity.n_dofs());
        force[d].reinit(dof_handler_velocity.n_dofs());
    }
    v_tmp.reinit(dof_handler_velocity.n_dofs());
    rot_u.reinit(dof_handler_velocity.n_dofs());

    std::cout << "dim (X_h) = " << (dof_handler_velocity.n_dofs() * dim)
              << std::endl
              << "dim (M_h) = " << dof_handler_pressure.n_dofs() << std::endl
              << "Re        = " << Re << std::endl
              << "LaplRe    = " << LaplRe << std::endl
              << "InnerVelocity = " << InnerVelocity << std::endl
              << std::endl;
}

// @sect4{ <code>NavierStokesSpectral::initialize</code> }

// This method creates the constant matrices and loads the initial data
template<int dim> void NavierStokesSpectral<dim>::initialize() {
    vel_Laplace_plus_Mass = 0.;
    vel_Laplace_plus_Mass.add(LaplRe ?  1. / Re : 1., vel_Laplace);
    vel_Laplace_plus_Mass.add(1.5 / dt, vel_Mass);

    VectorTools::interpolate(mapping, dof_handler_pressure, cuette_pres_func, pres_n_minus_1);
    cuette_pres_func.advance_time(dt);
    VectorTools::interpolate(mapping, dof_handler_pressure, cuette_pres_func, pres_n);
    phi_n = 0.;
    phi_n_minus_1 = 0.;

    for (unsigned int d = 0; d < dim; ++d) {
        cuette_func.set_time(t_0);
        cuette_func.set_component(d);
        VectorTools::interpolate(
            mapping, dof_handler_velocity, cuette_func, u_n_minus_1[d]);
        cuette_func.advance_time(dt);
        VectorTools::interpolate(
            mapping, dof_handler_velocity, cuette_func, u_n[d]);
    }
}

// @sect4{ The <code>NavierStokesSpectral::initialize_*_matrices</code>
// methods }

// In this set of methods we initialize the sparsity patterns, the
// constraints (if any) and assemble the matrices that do not depend on the
// timestep <code>dt</code>. Note that for the Laplace and mass matrices, we
// can use functions in the library that do this. Because the expensive
// operations of this function -- creating the two matrices -- are entirely
// independent, we could in principle mark them as tasks that can be worked
// on in %parallel using the Threads::new_task functions. We won't do that
// here since these functions internally already are parallelized, and in
// particular because the current function is only called once per program
// run and so does not incur a cost in each time step. The necessary
// modifications would be quite straightforward, however.
template<int dim>
void NavierStokesSpectral<dim>::initialize_velocity_matrices() {
    GridTools::collect_periodic_faces(
        dof_handler_velocity,
        periodic_b1,
        periodic_b2,
        periodic_dir,
        vel_periodicity_vector);

    DoFTools::make_periodicity_constraints<DoFHandler<dim>>(
        vel_periodicity_vector,
        vel_constraints);

    vel_constraints.close();

    {
        DynamicSparsityPattern dsp(
            dof_handler_velocity.n_dofs(), dof_handler_velocity.n_dofs());
        DoFTools::make_sparsity_pattern(dof_handler_velocity, dsp);
        vel_constraints.condense (dsp);
        sparsity_pattern_velocity.copy_from(dsp);
    }
    vel_Laplace_plus_Mass.reinit(sparsity_pattern_velocity);
    for (unsigned int d = 0; d < dim; ++d)
        vel_it_matrix[d].reinit(sparsity_pattern_velocity);
    vel_Mass.reinit(sparsity_pattern_velocity);
    vel_Laplace.reinit(sparsity_pattern_velocity);
    vel_Advection.reinit(sparsity_pattern_velocity);

    MatrixCreator::create_mass_matrix(
        mapping, dof_handler_velocity, quadrature_velocity, vel_Mass);
    MatrixCreator::create_laplace_matrix(
        mapping, dof_handler_velocity, quadrature_velocity, vel_Laplace);


    GridTools::collect_periodic_faces(
        dof_handler_velocity_joint,
        periodic_b1,
        periodic_b2,
        periodic_dir,
        vel_periodicity_vector_joint);

    DoFTools::make_periodicity_constraints<DoFHandler<dim>>(
        vel_periodicity_vector_joint,
        vel_constraints_joint);

    // cylinder
    // outer
    VectorTools::interpolate_boundary_values(
        mapping,
        dof_handler_velocity_joint,
        5,
        ZeroFunction<dim>(),
        vel_constraints_joint);
    // inner
    VectorTools::interpolate_boundary_values(
        mapping,
        dof_handler_velocity_joint,
        4,
        ZeroFunction<dim>(),
        vel_constraints_joint);

    vel_constraints_joint.close();

    {
        DynamicSparsityPattern dsp(
            dof_handler_velocity_joint.n_dofs(), dof_handler_velocity_joint.n_dofs());
        DoFTools::make_sparsity_pattern(dof_handler_velocity_joint, dsp);
        vel_constraints_joint.condense (dsp);
        sparsity_pattern_velocity_joint.copy_from(dsp);
    }

    system_matrix.reinit(sparsity_pattern_velocity_joint);
}

// The initialization of the matrices that act on the pressure space is
// similar to the ones that act on the velocity space.
template<int dim>
void NavierStokesSpectral<dim>::initialize_pressure_matrices() {
    GridTools::collect_periodic_faces(
        dof_handler_pressure,
        periodic_b1,
        periodic_b2,
        periodic_dir,
        pres_periodicity_vector);


    auto mean_value_boundary_ids = std::set<types::boundary_id>();
    for (const auto id : boundary_ids) {
        if ((int)id != periodic_b1 && (int)id != periodic_b2) {
            std::cerr << "add " << id << std::endl;
            mean_value_boundary_ids.insert(id);
        }
    }

    std::vector<bool> boundary_dofs (dof_handler_pressure.n_dofs(), false);
    DoFTools::extract_boundary_dofs (
        dof_handler_pressure,
        ComponentMask(),
        boundary_dofs,
        mean_value_boundary_ids);

    const unsigned int first_boundary_dof
        = std::distance (
            boundary_dofs.begin(),
            std::find (
                boundary_dofs.begin(),
                boundary_dofs.end(),
                true));

    /*
    pres_constraints.add_line (0);
    for (unsigned int i=1; i<dof_handler_pressure.n_dofs(); ++i) {
        pres_constraints.add_entry (
                0,
                i, -1);
    }
    */

    pres_constraints.clear();

    DoFTools::make_periodicity_constraints<DoFHandler<dim>>(
        pres_periodicity_vector,
        pres_constraints);
    std::set<unsigned int> constrained;
    for (const auto& line : pres_constraints.get_lines()) {
        for (const auto& entry : line.entries) {
            constrained.insert(entry.first);
        }
    }

    unsigned int lineId = (unsigned)-1;

    for (unsigned int i=first_boundary_dof; i<dof_handler_pressure.n_dofs(); ++i) {
        if (boundary_dofs[i] == true) {
            if (lineId == (unsigned)-1 && constrained.find(i) == constrained.end()) {
                pres_constraints.add_line(i);
                lineId = i;
                continue;
            }
            if (lineId != (unsigned)-1) {
                pres_constraints.add_entry (
                    lineId,
                    i, -1);
            }
        }
    }

    pres_constraints.close();


    {
        DynamicSparsityPattern dsp(
            dof_handler_pressure.n_dofs(), dof_handler_pressure.n_dofs());
        DoFTools::make_sparsity_pattern(dof_handler_pressure, dsp);
        pres_constraints.condense (dsp);
        sparsity_pattern_pressure.copy_from(dsp);
    }

    pres_Laplace.reinit(sparsity_pattern_pressure);
    pres_iterative.reinit(sparsity_pattern_pressure);
    pres_Mass.reinit(sparsity_pattern_pressure); // used in rotational method

    MatrixCreator::create_laplace_matrix(
        mapping, dof_handler_pressure, quadrature_pressure, pres_Laplace);
    MatrixCreator::create_mass_matrix(
        mapping, dof_handler_pressure, quadrature_pressure, pres_Mass);
}

// For the gradient operator, we start by initializing the sparsity pattern
// and compressing it.  It is important to notice here that the gradient
// operator acts from the pressure space into the velocity space, so we have
// to deal with two different finite element spaces. To keep the loops
// synchronized, we use the <code>typedef</code>'s that we have defined
// before, namely <code>PairedIterators</code> and
// <code>IteratorPair</code>.
template<int dim>
void NavierStokesSpectral<dim>::initialize_gradient_operator() {
    {
        DynamicSparsityPattern dsp(
            dof_handler_velocity.n_dofs(), dof_handler_pressure.n_dofs());
        DoFTools::make_sparsity_pattern(
            dof_handler_velocity, dof_handler_pressure, dsp);
        sparsity_pattern_pres_vel.copy_from(dsp);
    }

    InitGradPerTaskData per_task_data(
        0, fe_velocity.dofs_per_cell, fe_pressure.dofs_per_cell);
    InitGradScratchData scratch_data(
        mapping,
        fe_velocity,
        fe_pressure,
        quadrature_velocity,
        update_gradients | update_JxW_values,
        update_values);

    for (unsigned int d = 0; d < dim; ++d) {
        pres_Diff[d].reinit(sparsity_pattern_pres_vel);
        per_task_data.d = d;
        WorkStream::run(
            IteratorPair(IteratorTuple(
                dof_handler_velocity.begin_active(),
                dof_handler_pressure.begin_active())),
            IteratorPair(IteratorTuple(
                dof_handler_velocity.end(), dof_handler_pressure.end())),
            *this,
            &NavierStokesSpectral<dim>::assemble_one_cell_of_gradient,
            &NavierStokesSpectral<dim>::copy_gradient_local_to_global,
            scratch_data,
            per_task_data);
    }

    InitGradPerTaskData per_task_data2(
        0, fe_velocity.dofs_per_cell, fe_velocity.dofs_per_cell);

    InitGradScratchData scratch_data2(
        mapping,
        fe_velocity,
        fe_velocity,
        quadrature_velocity,
        update_gradients | update_JxW_values,
        update_values);

    for (unsigned int d = 0; d < dim; ++d) {
        vel_Diff[d].reinit(sparsity_pattern_velocity);
        per_task_data2.d = d;
        WorkStream::run(
            IteratorPair(IteratorTuple(
                dof_handler_velocity.begin_active(),
                dof_handler_velocity.begin_active())),
            IteratorPair(IteratorTuple(
                dof_handler_velocity.end(), dof_handler_velocity.end())),
            *this,
            &NavierStokesSpectral<dim>::assemble_one_cell_of_gradient,
            &NavierStokesSpectral<dim>::copy_gradient_local_to_global2,
            scratch_data2,
            per_task_data2);
    }
}

template<int dim>
void NavierStokesSpectral<dim>::compute_pressure_gradient(
    Vector<double>& output,
    const Vector<double>& input,
    const unsigned int d)
{
    auto vel_cell = dof_handler_velocity.begin_active();
    auto pres_cell = dof_handler_pressure.begin_active();

    auto& quadrature = quadrature_velocity;
    const unsigned int dpc = fe_velocity.dofs_per_cell;
    const unsigned int nqp = quadrature.size();
    std::vector<types::global_dof_index> ldi(dpc);

    std::vector<Tensor<1, dim>> grad(nqp);
    Vector<double> loc_grad(dpc);

    FEValues<dim> fe_val_vel(mapping, fe_velocity, quadrature, update_gradients | update_JxW_values | update_values);
    FEValues<dim> fe_val_pres(mapping, fe_pressure, quadrature, update_gradients | update_JxW_values | update_values);

    while (vel_cell != dof_handler_velocity.end()) {
        fe_val_vel.reinit(vel_cell);
        fe_val_pres.reinit(pres_cell);

        fe_val_pres.get_function_gradients(input, grad);

        vel_cell->get_dof_indices(ldi);

        loc_grad = 0.;
        for (unsigned int q = 0; q < nqp; ++q) {
            for (unsigned int i = 0; i < dpc; ++i) {
                loc_grad(i) += fe_val_vel.shape_value(i, q) * grad[q][d] * fe_val_vel.JxW(q);
            }
        }

        for (unsigned int i = 0; i < dpc; ++i) {
            output(ldi[i]) += loc_grad(i);
        }

        ++vel_cell;
        ++pres_cell;
    }

    // don't invert mass matrix

    assert(vel_cell == dof_handler_velocity.end());
    assert(pres_cell == dof_handler_pressure.end());
}

template<int dim>
void NavierStokesSpectral<dim>::assemble_one_cell_of_gradient(
    const IteratorPair& SI,
    InitGradScratchData& scratch,
    InitGradPerTaskData& data) {
    scratch.fe_val_vel.reinit(std::get<0>(*SI));
    scratch.fe_val_pres.reinit(std::get<1>(*SI));

    std::get<0>(*SI)
        ->get_dof_indices(data.vel_local_dof_indices);
    std::get<1>(*SI)
        ->get_dof_indices(data.pres_local_dof_indices);

    data.local_grad = 0.;
    for (unsigned int q = 0; q < scratch.nqp; ++q) {
        for (unsigned int i = 0; i < data.vel_dpc; ++i)
            for (unsigned int j = 0; j < data.pres_dpc; ++j)
                data.local_grad(i, j) +=
                    -scratch.fe_val_vel.JxW(q) *
                    scratch.fe_val_vel.shape_grad(i, q)[data.d] *
                    scratch.fe_val_pres.shape_value(j, q);
    }
}

template<int dim>
void NavierStokesSpectral<dim>::copy_gradient_local_to_global(
    const InitGradPerTaskData& data) {
    for (unsigned int i = 0; i < data.vel_dpc; ++i)
        for (unsigned int j = 0; j < data.pres_dpc; ++j)
            pres_Diff[data.d].add(
                data.vel_local_dof_indices[i],
                data.pres_local_dof_indices[j],
                data.local_grad(i, j));
}

template<int dim>
void NavierStokesSpectral<dim>::copy_gradient_local_to_global2(
    const InitGradPerTaskData& data) {
    for (unsigned int i = 0; i < data.vel_dpc; ++i)
        for (unsigned int j = 0; j < data.vel_dpc; ++j)
            vel_Diff[data.d].add(
                data.vel_local_dof_indices[i],
                data.pres_local_dof_indices[j],
                data.local_grad(i, j));
}

template<int dim>
void NavierStokesSpectral<dim>::spectral(const bool verbose __attribute__((unused)))
{
    std::vector<Vector<double> >        eigenfunctions;
    std::vector<std::complex<double>>   eigenvalues;

    int n_eigenvalues = 10;

    eigenvalues.resize(n_eigenvalues);
    eigenfunctions.resize(2*n_eigenvalues);

    for (auto& eigenfunction : eigenfunctions) {
        eigenfunction.reinit(3*dof_handler_velocity.n_dofs());
    }

    auto n_dofs = dof_handler_velocity.n_dofs();

    SolverControl solver_control (dof_handler_velocity.n_dofs(), 1e-4);
    auto num_arnoldi_vectors = 2*eigenvalues.size() + 2;

    // stable trajectory
    for (unsigned int d = 0; d < dim; ++d) {
        cuette_func.set_component(d);
        VectorTools::interpolate(
            mapping, dof_handler_velocity, cuette_func, u_star[d]);
    }

    join_velocity(u_starj, u_star);
    //


    // constraints
    AffineConstraints<double> constraints[dim];
    Step35::EquationData::InnerCircleVelocity<dim> innerCircle(InnerVelocity);
    for (unsigned int d = 0; d < dim; ++d) {
        innerCircle.set_component(d);
        constraints[d].clear();
        constraints[d].merge(vel_constraints);
        // cylinder
        // outer
        VectorTools::interpolate_boundary_values(
            mapping,
            dof_handler_velocity,
            5,
            ZeroFunction<dim>(),
            constraints[d]);
        // inner
        VectorTools::interpolate_boundary_values(
            mapping,
            dof_handler_velocity,
            4,
            ZeroFunction<dim>(),
            constraints[d]);
        constraints[d].close();
    }

    prec_vel_mass.initialize(vel_Mass);
    assemble_advection_term();
    std::cerr << "assemble system matrix\n";

    // TODO: constraints
    assemble_system_matrix();
    std::cerr << "assemble system matrix done\n";

    std::cerr << "assemble umfpack matrix\n";
    SparseDirectUMFPACK inverse;
    inverse.initialize (system_matrix);
    std::cerr << "assemble umfpack matrix done\n";

    SparseMatrix<double> matrix[dim];

    for (unsigned int d = 0; d < dim; ++d) {
        matrix[d].reinit(sparsity_pattern_velocity);
        matrix[d] = 0.;
        matrix[d].add(LaplRe ?  1. / Re : 1., vel_Laplace);
        matrix[d].add(LaplRe ? 1. : Re, vel_Advection);
    }

    bool reinit_prec = true;
    // const double rho = 0.00001;

    if (reinit_prec) {
        prec_mass.initialize(pres_Mass);
    }

    // joint velocities
    auto v_n_minus_1 = u_starj;
    auto v_n = u_starj;
    auto force_joint = u_starj;

    auto solve_system = [&](Vector<double>& dst, const Vector<double>& src) {
        int j = 0;

        // 1 find pressure
        j = 0;
        pres_tmp = 0.;

        for (unsigned d = 0; d < dim; ++d) {
            for (auto& v : u_n[d]) { v = src[j++]; }
            prec_vel_mass.solve(u_n[d]);
            pres_Diff[d].Tvmult_add(pres_tmp, u_n[d]);
        }
        // pres_tmp <- - div u
        pres_iterative.copy_from(pres_Laplace);
        pres_constraints.condense (pres_iterative, pres_tmp);
        if (reinit_prec) {
            prec_pres_Laplace.initialize(
                pres_iterative,
                SparseILU<double>::AdditionalData(
                    vel_diag_strength, vel_off_diagonals));
        }
        SolverControl solvercontrol(vel_max_its, vel_eps * pres_tmp.l2_norm());
        SolverGMRES<> cg(solvercontrol);
        cg.solve(pres_iterative, pres_n, pres_tmp, prec_pres_Laplace);
        pres_constraints.distribute(pres_n); // - pres_n

        // right part
        for (unsigned d = 0; d < dim; ++d) {
            force[d] = 0.;
        }

        for (unsigned d = 0; d < dim; ++d) {
            pres_Diff[d].vmult_add(force[d], pres_n); // f = f - grad pres
        }

        force_joint = 0.;
        join_velocity(force_joint, force);
        // vel_constraints_joint.condense(force_joint);

        inverse.vmult(v_n, force_joint);
        // vel_constraints_joint.distribute(v_n);
        dst = v_n;

#if 0
        /*
        for (unsigned d = 0; d < dim; ++d) {
            u_n_minus_1[d] = 0.0;
        }
        */

        pres_n_minus_1 = 0;
        double eps = 1;
        while (eps > 1e-5) {
            // right part
            for (unsigned d = 0; d < dim; ++d) {
                force[d] = 0.;
            }

            for (unsigned d = 0; d < dim; ++d) {
                pres_Diff[d].vmult_add(force[d], pres_n_minus_1);
                force[d] *= -1;
            }

            j = 0;
            for (unsigned d = 0; d < dim; ++d) {
                for (auto& v : force[d]) { v += src[j++]; }
            }

            force_joint = 0.;
            join_velocity(force_joint, force);
            vel_constraints_joint.condense(force_joint);

            inverse.vmult(v_n, force_joint);
            vel_constraints_joint.distribute(v_n);
            disjoin_velocity(u_n, v_n);

            // pressure
            pres_tmp = 0.;
            for (unsigned d = 0; d < dim; ++d) {
                pres_Diff[d].Tvmult_add(pres_tmp, u_n[d]); // -div
            }
            prec_mass.solve(pres_tmp);
            pres_n = pres_tmp;
            pres_n.sadd(rho, pres_n_minus_1); // pres^n = - rho div + pres^{n-1}

            pres_tmp = pres_n;
            pres_tmp -= pres_n_minus_1;
            eps = pres_tmp.l2_norm();

            pres_n_minus_1 = pres_n;

            std::cerr << "eps = " << eps << std::endl;
        }

        dst = v_n;
#endif
#if 0
            // right part
            j = 0;
            for (unsigned d = 0; d < dim; ++d) {
                force[d] = 0;
                for (auto& v : force[d]) { v = src[j++]; }
            }

            j = 0;
            for (unsigned d = 0; d < dim; ++d) {
                pres_Diff[d].vmult_add(force[d], pres_n_minus_1);
                force[d] *= -1; // <- wrong

                // (u \dot \grad) u^*
                for (unsigned dd = 0; dd < dim; ++dd) {
                    v_tmp = 0.;
                    vel_Diff[dd].vmult(v_tmp, u_star[d]);
                    v_tmp.scale(u_n_minus_1[dd]);
                    force[d] -= v_tmp;
                }

                // 1/2 (div u) u^*
                v_tmp = 0.;
                for (unsigned dd = 0; dd < dim; ++dd) {
                    vel_Diff[dd].Tvmult_add(v_tmp, u_n_minus_1[dd]);
                }
                v_tmp.scale(u_star[d]);
                force[d].sadd(0.5, v_tmp);
                vel_it_matrix[d].copy_from(matrix[d]);
                constraints[d].condense(vel_it_matrix[d], force[d]);
            }

            Threads::TaskGroup<void> tasks;
            for (unsigned int d = 0; d < dim; ++d) {
                if (reinit_prec)
                    prec_velocity[d].initialize(
                        vel_it_matrix[d],
                        SparseILU<double>::AdditionalData(
                            vel_diag_strength, vel_off_diagonals));
                tasks += Threads::new_task(
                    &NavierStokesSpectral<dim>::diffusion_component_solve, *this, d);
            }
            tasks.join_all();

            std::cerr << "tasks done\n";

            // velosity
            j = 0;
            for (unsigned int d = 0; d < dim; ++d) {
                constraints[d].distribute(u_n[d]);
            }

            // pressure
            pres_tmp = 0.;
            for (unsigned d = 0; d < dim; ++d) {
                pres_Diff[d].Tvmult_add(pres_tmp, u_n[d]); // -div
            }
            prec_mass.solve(pres_tmp);
            pres_n = pres_tmp;
            pres_n.sadd(rho, pres_n_minus_1);

            eps = 0.;
            for (unsigned d = 0; d < dim; ++d) {
                v_tmp = u_n[d];
                v_tmp -= u_n_minus_1[d];
                eps += v_tmp.l2_norm();

                u_n_minus_1[d] = u_n[d];
            }
            pres_n_minus_1 = pres_n;

            std::cerr << "eps = " << eps << std::endl;
        }

        // answer
        j = 0;
        for (unsigned int d = 0; d < dim; ++d) {
            for (const auto& v : u_n[d]) { dst[j++] = v; }
        }
#endif

#if 0
        // 1 find pressure
        j = 0;
        pres_tmp = 0.;

        for (unsigned d = 0; d < dim; ++d) {
            for (auto& v : u_n[d]) { v = src[j++]; }
            prec_vel_mass.solve(u_n[d]);
            pres_Diff[d].Tvmult_add(pres_tmp, u_n[d]);
        }
        // pres_tmp <- div u
        pres_iterative.copy_from(pres_Laplace);
        pres_constraints.condense (pres_iterative, pres_tmp);
        if (reinit_prec) {
            prec_pres_Laplace.initialize(
                pres_iterative,
                SparseILU<double>::AdditionalData(
                    vel_diag_strength, vel_off_diagonals));
        }
        SolverControl solvercontrol(vel_max_its, vel_eps * pres_tmp.l2_norm());
        SolverGMRES<> cg(solvercontrol);
        cg.solve(pres_iterative, phi_n, pres_tmp, prec_pres_Laplace);
        pres_constraints.distribute(phi_n);

        // phi_n <- pres

        j = 0;
        phi_n *= -1.0;
        for (unsigned d = 0; d < dim; ++d) {
            for (auto& v : force[d]) { v = src[j++]; }
            pres_Diff[d].vmult_add(force[d], phi_n); // CHECK

            vel_it_matrix[d].copy_from(matrix[d]);
            constraints[d].condense(vel_it_matrix[d], force[d]);
        }

        Threads::TaskGroup<void> tasks;
        for (unsigned int d = 0; d < dim; ++d) {
            if (reinit_prec)
                prec_velocity[d].initialize(
                    vel_it_matrix[d],
                    SparseILU<double>::AdditionalData(
                        vel_diag_strength, vel_off_diagonals));
            tasks += Threads::new_task(
                &NavierStokesSpectral<dim>::diffusion_component_solve, *this, d);
        }
        tasks.join_all();

        // velosity
        j = 0;
        for (unsigned int d = 0; d < dim; ++d) {
            constraints[d].distribute(u_n[d]);
            for (const auto& v : u_n[d]) { dst[j++] = v; }
        }
        // pressure
        // for (const auto& v : phi_n) { dst[j++] = v; }
#endif
        reinit_prec = false;
    };
    //

    auto mul_mass = [&](Vector<double>& dst, const Vector<double>& src) {
        int j = 0;
        for (unsigned i = 0; i < dim; ++i) {
            Vector<double> s(src.begin() + i*n_dofs, src.begin() + (i+1)*n_dofs);
            Vector<double> d(n_dofs);
            vel_Mass.vmult(d, s);
            for (const auto& val : d) {
                dst[j++] = val;
            }
        }
    };

    struct MatrixWrapper {
        const std::function<void(Vector<double>& dst, const Vector<double>& src)> mul;
        MatrixWrapper(const std::function<void(Vector<double>& dst, const Vector<double>& src)>& mul)
            : mul(mul)
        { }

        void vmult(Vector<double>& dst, const Vector<double>& src) const {
            mul(dst, src);
        }
    };

    //ArpackSolver::AdditionalData additional_data(num_arnoldi_vectors, ArpackSolver::largest_real_part, /*symmetric = */ false);
    ArpackSolver::AdditionalData additional_data(num_arnoldi_vectors, ArpackSolver::smallest_real_part, /*symmetric = */ false);
    ArpackSolver eigensolver (solver_control, additional_data);

    eigensolver.solve (
        MatrixWrapper(mul_mass)  /* unused for shift-invert mode */,
        MatrixWrapper(mul_mass), /* mass matrix */
        MatrixWrapper(solve_system), /* inverted A matrix */
        eigenvalues,
        eigenfunctions,
        eigenvalues.size());

    for (const auto& value : eigenvalues) {
        std::cerr << "value " << value << std::endl;
    }
}

template<int dim> void NavierStokesSpectral<dim>::interpolate_velocity() {
    for (unsigned int d = 0; d < dim; ++d) {
        u_star[d].equ(2., u_n[d]);
        u_star[d] -= u_n_minus_1[d];
    }
}

// @sect4{<code>NavierStokesSpectral::diffusion_step</code>}

// The implementation of a diffusion step. Note that the expensive operation
// is the diffusion solve at the end of the function, which we have to do
// once for each velocity component. To accelerate things a bit, we allow
// to do this in %parallel, using the Threads::new_task function which makes
// sure that the <code>dim</code> solves are all taken care of and are
// scheduled to available processors: if your machine has more than one
// processor core and no other parts of this program are using resources
// currently, then the diffusion solves will run in %parallel. On the other
// hand, if your system has only one processor core then running things in
// %parallel would be inefficient (since it leads, for example, to cache
// congestion) and things will be executed sequentially.
template<int dim>
void NavierStokesSpectral<dim>::diffusion_step(const bool reinit_prec) {
    pres_tmp.equ(-1., pres_n);
    pres_tmp.add(-4. / 3., phi_n, 1. / 3., phi_n_minus_1);

    assemble_advection_term();

    AffineConstraints<double> constraints[dim];

    Step35::EquationData::InnerCircleVelocity<dim> innerCircle(InnerVelocity);

    for (unsigned int d = 0; d < dim; ++d) {
        force[d] = 0.;
        v_tmp.equ(2. / dt, u_n[d]);
        v_tmp.add(-.5 / dt, u_n_minus_1[d]);
        vel_Mass.vmult_add(force[d], v_tmp);
        pres_Diff[d].vmult_add(force[d], pres_tmp);
        u_n_minus_1[d] = u_n[d];

        vel_it_matrix[d].copy_from(vel_Laplace_plus_Mass);
        vel_it_matrix[d].add(LaplRe ? 1. : Re, vel_Advection);

        innerCircle.set_component(d);
        constraints[d].clear();
        constraints[d].merge(vel_constraints);

        // cylinder
        // outer
        VectorTools::interpolate_boundary_values(
            mapping,
            dof_handler_velocity,
            5,
            ZeroFunction<dim>(),
            constraints[d]);

        // inner
        VectorTools::interpolate_boundary_values(
            mapping,
            dof_handler_velocity,
            4,
            innerCircle,
            constraints[d]);

        constraints[d].close();

        constraints[d].condense(vel_it_matrix[d], force[d]);
    }

    Threads::TaskGroup<void> tasks;
    for (unsigned int d = 0; d < dim; ++d) {
        if (reinit_prec)
            prec_velocity[d].initialize(
                vel_it_matrix[d],
                SparseILU<double>::AdditionalData(
                    vel_diag_strength, vel_off_diagonals));
        tasks += Threads::new_task(
            &NavierStokesSpectral<dim>::diffusion_component_solve, *this, d);
    }
    tasks.join_all();

    for (unsigned int d = 0; d < dim; ++d) {
        constraints[d].distribute(u_n[d]);
    }
}

template<int dim>
void NavierStokesSpectral<dim>::diffusion_component_solve(
    const unsigned int d) {
    SolverControl solver_control(vel_max_its, vel_eps * force[d].l2_norm());
    SolverGMRES<> gmres(
        solver_control, SolverGMRES<>::AdditionalData(vel_Krylov_size));
    gmres.solve(vel_it_matrix[d], u_n[d], force[d], prec_velocity[d]);
}

// @sect4{ The <code>NavierStokesSpectral::assemble_advection_term</code>
// method and related}

// The following few functions deal with assembling the advection terms,
// which is the part of the system matrix for the diffusion step that
// changes at every time step. As mentioned above, we will run the assembly
// loop over all cells in %parallel, using the WorkStream class and other
// facilities as described in the documentation module on @ref threads.
template<int dim> void NavierStokesSpectral<dim>::assemble_advection_term() {
    vel_Advection = 0.;
    AdvectionPerTaskData data(fe_velocity.dofs_per_cell);
    AdvectionScratchData scratch(
        mapping,
        fe_velocity,
        quadrature_velocity,
        update_values | update_JxW_values | update_gradients);
    WorkStream::run(
        dof_handler_velocity.begin_active(),
        dof_handler_velocity.end(),
        *this,
        &NavierStokesSpectral<dim>::assemble_one_cell_of_advection,
        &NavierStokesSpectral<dim>::copy_advection_local_to_global,
        scratch,
        data);
}

template<int dim>
void NavierStokesSpectral<dim>::assemble_one_cell_of_advection(
    const typename DoFHandler<dim>::active_cell_iterator& cell,
    AdvectionScratchData& scratch,
    AdvectionPerTaskData& data) {
    scratch.fe_val.reinit(cell);
    cell->get_dof_indices(data.local_dof_indices);
    for (unsigned int d = 0; d < dim; ++d) {
        scratch.fe_val.get_function_values(u_star[d], scratch.u_star_tmp);
        for (unsigned int q = 0; q < scratch.nqp; ++q)
            scratch.u_star_local[q](d) = scratch.u_star_tmp[q];
    }

    for (unsigned int d = 0; d < dim; ++d) {
        scratch.fe_val.get_function_gradients(u_star[d], scratch.grad_u_star);
        for (unsigned int q = 0; q < scratch.nqp; ++q) {
            if (d == 0)
                scratch.u_star_tmp[q] = 0.;
            scratch.u_star_tmp[q] += scratch.grad_u_star[q][d];
        }
    }

    data.local_advection = 0.;
    for (unsigned int q = 0; q < scratch.nqp; ++q)
        for (unsigned int i = 0; i < scratch.dpc; ++i)
            for (unsigned int j = 0; j < scratch.dpc; ++j)
                data.local_advection(i, j) +=
                    (scratch.u_star_local[q] * scratch.fe_val.shape_grad(j, q) *
                         scratch.fe_val.shape_value(i, q) +
                     0.5 * scratch.u_star_tmp[q] *
                         scratch.fe_val.shape_value(i, q) *
                         scratch.fe_val.shape_value(j, q)) *
                    scratch.fe_val.JxW(q);
}

template<int dim>
void NavierStokesSpectral<dim>::copy_advection_local_to_global(
    const AdvectionPerTaskData& data) {
    for (unsigned int i = 0; i < fe_velocity.dofs_per_cell; ++i)
        for (unsigned int j = 0; j < fe_velocity.dofs_per_cell; ++j)
            vel_Advection.add(
                data.local_dof_indices[i],
                data.local_dof_indices[j],
                data.local_advection(i, j));
}

template<int dim>
void NavierStokesSpectral<dim>::assemble_system_matrix()
{
    FEValues<dim> fe_values(
        fe_velocity_joint,
        quadrature_velocity,
        update_values | update_quadrature_points |
        update_JxW_values | update_gradients);

    const unsigned int dofs_per_cell = fe_velocity_joint.dofs_per_cell;
    const unsigned int n_q_points    = quadrature_velocity.size();

    FullMatrix<double> local_matrix(dofs_per_cell, dofs_per_cell);

    std::vector<types::global_dof_index> ldi(dofs_per_cell);

    std::vector<Tensor<1, dim>> u_star_values(n_q_points);
    std::vector<Tensor<2, dim>> u_star_gradients(n_q_points);

    std::vector<double>         div_phi_u(dofs_per_cell);
    std::vector<Tensor<1, dim>> phi_u(dofs_per_cell);
    std::vector<Tensor<2, dim>> grad_phi_u(dofs_per_cell);

    const FEValuesExtractors::Vector velocities(0);

    for (const auto &cell : dof_handler_velocity_joint.active_cell_iterators())
    {
        fe_values.reinit(cell);

        local_matrix = 0;

        fe_values[velocities].get_function_values(
            u_starj, u_star_values);
        fe_values[velocities].get_function_gradients(
            u_starj, u_star_gradients);

        for (unsigned int q = 0; q < n_q_points; ++q)
        {
            for (unsigned int k = 0; k < dofs_per_cell; ++k)
            {
                div_phi_u[k]  = fe_values[velocities].divergence(k, q);
                grad_phi_u[k] = fe_values[velocities].gradient(k, q);
                phi_u[k]      = fe_values[velocities].value(k, q);
            }

            for (unsigned int i = 0; i < dofs_per_cell; ++i)
            {
                for (unsigned int j = 0; j < dofs_per_cell; ++j)
                {
                    local_matrix(i, j) +=
                        (
                            1./Re * scalar_product(grad_phi_u[j], grad_phi_u[i]) +
                            u_star_gradients[q] * phi_u[j] * phi_u[i] +
                            grad_phi_u[j] * u_star_values[q] * phi_u[i]
                        )
                        * fe_values.JxW(q);
                }
            }
        }

        cell->get_dof_indices(ldi);
        vel_constraints_joint.distribute_local_to_global(local_matrix, ldi, system_matrix);
    }
}

// @sect4{<code>NavierStokesSpectral::projection_step</code>}

// This implements the projection step:
template<int dim>
void NavierStokesSpectral<dim>::projection_step(const bool reinit_prec) {
    pres_iterative.copy_from(pres_Laplace);

    pres_tmp = 0.;
    for (unsigned d = 0; d < dim; ++d) {
        auto tmp = u_n[d];
        //tmp *= 2./3. * dt;
        pres_Diff[d].Tvmult_add(pres_tmp, tmp);
    }

    phi_n_minus_1 = phi_n;

    // pres_tmp *= 2./3. * dt;

    pres_constraints.condense (pres_iterative, pres_tmp);

/*
    static std::map<types::global_dof_index, double> bval;
    if (reinit_prec) {
        for (auto id : boundary_ids) {
            VectorTools::interpolate_boundary_values(
                dof_handler_pressure, id, ZeroFunction<dim>(), bval);
        }
    }

    MatrixTools::apply_boundary_values(bval, pres_iterative, phi_n, pres_tmp);
*/

    if (reinit_prec)
        prec_pres_Laplace.initialize(
            pres_iterative,
            SparseILU<double>::AdditionalData(
                vel_diag_strength, vel_off_diagonals));

    SolverControl solvercontrol(vel_max_its, vel_eps * pres_tmp.l2_norm());
    //SolverCG<> cg(solvercontrol);
    SolverGMRES<> cg(solvercontrol);
    cg.solve(pres_iterative, phi_n, pres_tmp, prec_pres_Laplace);

    pres_constraints.distribute(phi_n);

    phi_n *= 1.5 / dt;
}

// @sect4{ <code>NavierStokesSpectral::update_pressure</code> }

// This is the pressure update step of the projection method. It implements
// the standard formulation of the method, that is @f[ p^{n+1} = p^n +
// \phi^{n+1}, @f] or the rotational form, which is @f[ p^{n+1} = p^n +
// \phi^{n+1} - \frac{1}{Re} \nabla\cdot u^{n+1}.  @f]
template<int dim>
void NavierStokesSpectral<dim>::update_pressure(const bool reinit_prec) {
    pres_n_minus_1 = pres_n;
    switch (type) {
        case RunTimeParameters::METHOD_STANDARD:
            pres_n += phi_n;
            break;
        case RunTimeParameters::METHOD_ROTATIONAL:
            if (reinit_prec) {
                // pres_constraints.condense (pres_Mass); //
                prec_mass.initialize(pres_Mass);
            }
            pres_tmp = 0.;
            for (unsigned d = 0; d < dim; ++d) {
                pres_Diff[d].Tvmult_add(pres_tmp, u_n[d]);
            }

            pres_n = pres_tmp;
            /// pres_constraints.condense (pres_n);  //
            prec_mass.solve(pres_n);
            // pres_constraints.distribute(pres_n); //
            pres_n.sadd(LaplRe ? 1. / Re : 1., 1., pres_n_minus_1);
            pres_n += phi_n;
            break;
        default:
            Assert(false, ExcNotImplemented());
    };

    //auto mean = pres_n.mean_value();;
    //for (auto& nn : pres_n) {
    //    nn -= mean;
    //}

    //
#if 0
    for (int d = 0; d < dim; ++d) {
        Vector<double> tmp = u_n[d];
        tmp = 0.0;
        if (0) {
            pres_Diff[d].vmult_add(tmp, phi_n);
        } else {
            compute_pressure_gradient(tmp, phi_n, d);
        }
        prec_vel_mass.solve(tmp);
        tmp *= 2.*dt/3.;
        u_n[d] -= tmp;
    }
#endif
}

template<int dim>
void NavierStokesSpectral<dim>::join_velocity(Vector<double>& joint, const Vector<double>* u)
{
   typename DoFHandler<dim>::active_cell_iterator
        joint_cell = dof_handler_velocity_joint.begin_active(),
        joint_endc = dof_handler_velocity_joint.end(),
        vel_cell = dof_handler_velocity.begin_active();

    std::vector<types::global_dof_index> loc_joint_dof_indices(
        fe_velocity_joint.dofs_per_cell),
        loc_vel_dof_indices(fe_velocity.dofs_per_cell);

    for (; joint_cell != joint_endc; ++joint_cell, ++vel_cell) {
        joint_cell->get_dof_indices(loc_joint_dof_indices);
        vel_cell->get_dof_indices(loc_vel_dof_indices);
        for (unsigned int i = 0; i < fe_velocity_joint.dofs_per_cell; ++i) {
            joint(loc_joint_dof_indices[i]) =
                u[fe_velocity_joint.system_to_base_index(i).first.second](
                    loc_vel_dof_indices[fe_velocity_joint.system_to_base_index(i).second]);
        }
    }
}

template<int dim>
void NavierStokesSpectral<dim>::disjoin_velocity(Vector<double>* u, const Vector<double>& joint)
{
   typename DoFHandler<dim>::active_cell_iterator
        joint_cell = dof_handler_velocity_joint.begin_active(),
        joint_endc = dof_handler_velocity_joint.end(),
        vel_cell = dof_handler_velocity.begin_active();

    std::vector<types::global_dof_index> loc_joint_dof_indices(
        fe_velocity_joint.dofs_per_cell),
        loc_vel_dof_indices(fe_velocity.dofs_per_cell);

    for (; joint_cell != joint_endc; ++joint_cell, ++vel_cell) {
        joint_cell->get_dof_indices(loc_joint_dof_indices);
        vel_cell->get_dof_indices(loc_vel_dof_indices);
        for (unsigned int i = 0; i < fe_velocity_joint.dofs_per_cell; ++i) {
            u[fe_velocity_joint.system_to_base_index(i).first.second](
                loc_vel_dof_indices[fe_velocity_joint.system_to_base_index(i).second])
                = joint(loc_joint_dof_indices[i]);
        }
    }
}

// @sect4{ <code>NavierStokesSpectral::output_results</code> }

// This method plots the current solution. The main difficulty is that we
// want to create a single output file that contains the data for all
// velocity components, the pressure, and also the vorticity of the flow. On
// the other hand, velocities and the pressure live on separate DoFHandler
// objects, and so can't be written to the same file using a single DataOut
// object. As a consequence, we have to work a bit harder to get the various
// pieces of data into a single DoFHandler object, and then use that to
// drive graphical output.
//
// We will not elaborate on this process here, but rather refer to step-32,
// where a similar procedure is used (and is documented) to
// create a joint DoFHandler object for all variables.
//
// Let us also note that we here compute the vorticity as a scalar quantity
// in a separate function, using the $L^2$ projection of the quantity
// $\text{curl} u$ onto the finite element space used for the components of
// the velocity. In principle, however, we could also have computed as a
// pointwise quantity from the velocity, and do so through the
// DataPostprocessor mechanism discussed in step-29 and step-33.
template<int dim>
void NavierStokesSpectral<dim>::output_results(const unsigned int step, const Vector<double>* u_n, const std::string& prefix) {
    assemble_vorticity((step == 1));
    const FESystem<dim> joint_fe(
        fe_velocity, dim, fe_pressure, 1, fe_velocity, 1);
    DoFHandler<dim> joint_dof_handler(triangulation);
    joint_dof_handler.distribute_dofs(joint_fe);
    Assert(
        joint_dof_handler.n_dofs() ==
            ((dim + 1) * dof_handler_velocity.n_dofs() +
             dof_handler_pressure.n_dofs()),
        ExcInternalError());
    static Vector<double> joint_solution(joint_dof_handler.n_dofs());
    std::vector<types::global_dof_index> loc_joint_dof_indices(
        joint_fe.dofs_per_cell),
        loc_vel_dof_indices(fe_velocity.dofs_per_cell),
        loc_pres_dof_indices(fe_pressure.dofs_per_cell);
    typename DoFHandler<dim>::active_cell_iterator
        joint_cell = joint_dof_handler.begin_active(),
        joint_endc = joint_dof_handler.end(),
        vel_cell = dof_handler_velocity.begin_active(),
        pres_cell = dof_handler_pressure.begin_active();
    for (; joint_cell != joint_endc; ++joint_cell, ++vel_cell, ++pres_cell) {
        joint_cell->get_dof_indices(loc_joint_dof_indices);
        vel_cell->get_dof_indices(loc_vel_dof_indices),
            pres_cell->get_dof_indices(loc_pres_dof_indices);
        for (unsigned int i = 0; i < joint_fe.dofs_per_cell; ++i)
            switch (joint_fe.system_to_base_index(i).first.first) {
                case 0:
                    Assert(
                        joint_fe.system_to_base_index(i).first.second < dim,
                        ExcInternalError());
                    joint_solution(loc_joint_dof_indices[i]) =
                        u_n[joint_fe.system_to_base_index(i).first.second](
                            loc_vel_dof_indices[joint_fe.system_to_base_index(i)
                                                    .second]);
                    break;
                case 1:
                    Assert(
                        joint_fe.system_to_base_index(i).first.second == 0,
                        ExcInternalError());
                    joint_solution(loc_joint_dof_indices[i]) = pres_n(
                        loc_pres_dof_indices[joint_fe.system_to_base_index(i)
                                                 .second]);
                    break;
                case 2:
                    Assert(
                        joint_fe.system_to_base_index(i).first.second == 0,
                        ExcInternalError());
                    joint_solution(loc_joint_dof_indices[i]) = rot_u(
                        loc_vel_dof_indices[joint_fe.system_to_base_index(i)
                                                .second]);
                    break;
                default:
                    Assert(false, ExcInternalError());
            }
    }
    std::vector<std::string> joint_solution_names(dim, "v");
    joint_solution_names.push_back("p");
    joint_solution_names.push_back("rot_u");
    DataOut<dim> data_out;
    data_out.attach_dof_handler(joint_dof_handler);
    std::vector<DataComponentInterpretation::DataComponentInterpretation>
        component_interpretation(
            dim + 2, DataComponentInterpretation::component_is_part_of_vector);
    component_interpretation[dim] =
        DataComponentInterpretation::component_is_scalar;
    component_interpretation[dim + 1] =
        DataComponentInterpretation::component_is_scalar;
    data_out.add_data_vector(
        joint_solution,
        joint_solution_names,
        DataOut<dim>::type_dof_data,
        component_interpretation);
    data_out.build_patches(mapping, deg + 1);
    std::ofstream output(
        (prefix + Utilities::int_to_string(step, 5) + ".vtu").c_str());
    data_out.write_vtu(output);
}

// Following is the helper function that computes the vorticity by
// projecting the term $\text{curl} u$ onto the finite element space used
// for the components of the velocity. The function is only called whenever
// we generate graphical output, so not very often, and as a consequence we
// didn't bother parallelizing it using the WorkStream concept as we do for
// the other assembly functions. That should not be overly complicated,
// however, if needed. Moreover, the implementation that we have here only
// works for 2d, so we bail if that is not the case.
template<int dim>
void NavierStokesSpectral<dim>::assemble_vorticity(const bool reinit_prec) {
    if (reinit_prec)
        prec_vel_mass.initialize(vel_Mass);

    FEValues<dim> fe_val_vel(
        mapping,
        fe_velocity,
        quadrature_velocity,
        update_gradients | update_JxW_values | update_values);
    const unsigned int dpc = fe_velocity.dofs_per_cell,
                       nqp = quadrature_velocity.size();
    std::vector<types::global_dof_index> ldi(dpc);
    Vector<double> loc_rot(dpc);

    std::vector<Tensor<1, dim>> grad_u1(nqp), grad_u2(nqp);
    rot_u = 0.;

    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler_velocity.begin_active(),
        end = dof_handler_velocity.end();
    for (; cell != end; ++cell) {
        fe_val_vel.reinit(cell);
        cell->get_dof_indices(ldi);
        fe_val_vel.get_function_gradients(u_n[0], grad_u1);
        fe_val_vel.get_function_gradients(u_n[1], grad_u2);
        loc_rot = 0.;
        for (unsigned int q = 0; q < nqp; ++q)
            for (unsigned int i = 0; i < dpc; ++i)
                loc_rot(i) += (grad_u2[q][0] - grad_u1[q][1]) *
                              fe_val_vel.shape_value(i, q) * fe_val_vel.JxW(q);

        for (unsigned int i = 0; i < dpc; ++i)
            rot_u(ldi[i]) += loc_rot(i);
    }

    prec_vel_mass.solve(rot_u);
}
} // namespace Step35

// @sect3{ The main function }

// The main function looks very much like in all the other tutorial programs,
// so there is little to comment on here:
int main(int argc, char** argv) {
    try {
        using namespace dealii;
        using namespace Step35;

        RunTimeParameters::Data_Storage data;
        data.read_data("parameter-file.prm");

        for (int i = 1; i < argc; ++i) {
            if (!strcmp(argv[i], "--mesh") && i < argc - 1) {
                data.mesh_file = argv[++i];
            }
        }

        deallog.depth_console(data.verbose ? 2 : 0);

        // bnd
        // 2 -- top
        // 3 -- bottom
        // 4 -- inner
        // 5 -- outer
        data.b1 = 3;
        data.b2 = 2;
        data.dir = 2;
        data.cylinder = true;

        NavierStokesSpectral<3> test(data);
        // test.run(data.verbose, data.output_interval);
        test.spectral();

    } catch (std::exception& exc) {
        std::cerr << std::endl
                  << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        std::cerr << "Exception on processing: " << std::endl
                  << exc.what() << std::endl
                  << "Aborting!" << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        return 1;
    } catch (...) {
        std::cerr << std::endl
                  << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        std::cerr << "Unknown exception!" << std::endl
                  << "Aborting!" << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        return 1;
    }
    std::cout << "----------------------------------------------------"
              << std::endl
              << "Apparently everything went fine!" << std::endl
              << "Don't forget to brush your teeth :-)" << std::endl
              << std::endl;
    return 0;
}
