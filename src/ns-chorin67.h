#pragma once
//
// Created by aozeritsky on 17.06.19.
//

#include "model_base.h"
#include "lapl.h"
#include "div.h"
#include "grad.h"

#include "func_tools.h"

#include <deal.II/base/work_stream.h>

template <int dim>
class NsChorin67 {
public:

    explicit NsChorin67(
        const ModelBase<dim>& velSpace,
        const ModelBase<dim>& presSpace,
        const double Re = 32,
        const double dt = 1e-4)
        : velSpace(velSpace)
        , presSpace(presSpace)
        , velDiv(velSpace)
        , velLapl(velSpace)
        , presLapl(presSpace)
        , velGrad(velSpace)
        , presGrad(presSpace)
        , Re(Re)
        , dt(dt)
    { }

    void initialize()
    {
        velDiv.initialize();
        velLapl.initialize();
        presLapl.initialize();
        velGrad.initialize();
        presGrad.initialize();
    }

    void calc(
        Vector<double>& output,
        const Vector<double>& input,
        const Vector<double>& E /*forcing*/,
        const Function<dim>& function /* boundary cond */)
    {
        // The numerical solutions of the navier-Stokes equations for an incompressible fluid
        // Chorin, 1967
        // Use iteration (4)
        // 1. Calc u^{aux}
        // 2. Use (4) to find next p and next u <- projection step
        // u^{aux} = u + dt F u,
        // F = - R u du + Lu + E (E <- forcing)

        // input -> {u_x, u_y, u_z, p}
        // output -> {u_x, u_u, u_z, p}

        auto& velDof = velSpace.dof();
        int n_velDofs = velDof.n_dofs();

        auto& presDof = presSpace.dof();
        int n_presDofs = presDof.n_dofs();

        assert(n_presDofs == n_velDofs); // TODO:

        Vector<double> p(input.begin()+dim*n_velDofs, input.end());

        Vector<double> nextU(3*n_velDofs);
        Vector<double> divU(n_velDofs);

        int i = 0;
        for (;i < dim*n_velDofs; i++) {
            nextU[i] = input[i];
        }
        velDiv.div(divU, nextU);
        fprintf(stderr, "divU %le\n", divU.l2_norm());

        Vector<double> uAux(n_velDofs*dim);
        Vector<double> u(input.begin(), input.begin() + n_velDofs*dim);

        auto rpE = E;
        //auto gradP1 = nextU;
        //presGrad.grad(gradP1, p);
        //rpE -= gradP1;

        solveUaux(uAux, u, rpE, function);

        fprintf(stderr, "uAux norm %le\n", uAux.l2_norm());

        //Vector<double> divUaux(n_velDofs);
        //velDiv.div(divUaux, uAux);

        //fprintf(stderr, "uAux norm %le\n", divUaux.l2_norm());

        //divUaux *= lambda;
        //divUaux *= 1.0 / dt;


        assert((int)p.size() == n_presDofs);
        Vector<double> pn(n_presDofs);
        pn = 1.0;

        //presLapl.solve(pn, divUaux);

        /*
        p += pn;
        double mean = 0.0;
        for (auto v : p) {
            mean += v;
        }
        mean /= p.size();
        for (auto& v : p) {
            v -= mean;
        }
        */

        /*
        fprintf(stderr, "p/pn %le/%le\n", p.l2_norm(), pn.l2_norm());
        pn.swap(p);

        */

        i =0;
//        fprintf(stderr, "uaux found\n");

        Vector<double> un(n_velDofs*dim);
        Vector<double> rp(n_presDofs);
        Vector<double> gradP;

        double curEps;
#if 0
        while ((curEps = presSpace.norm_1(pn, p)) > tol && i++ < 50) {
            presGrad.grad(gradP, p);
            gradP *= dt;
            un = uAux;
            un -= gradP;

            velDiv.div(divUaux, un);
            divUaux *= lambda;
            pn = p;
            pn -= divUaux;

//            rp = p;
//            fprintf(stderr, "1->%le\n", p.l2_norm());
//            fprintf(stderr, "2->%le\n", divUaux.l2_norm());
//            rp -= divUaux;
//            fprintf(stderr, "3->%le\n", rp.l2_norm());
//            presLapl.solve(pn, rp, 1.0, -lambda*dt);
//            fprintf(stderr, "4->%le\n", pn.l2_norm());
            p.swap(pn);
            fprintf(stderr, "5->%le\n", curEps);
        }
//        exit(1);
#endif

        while (true) {
            presGrad.grad(gradP, p);
            // gradP *= dt;
            un = uAux;
            un -= gradP;

            Vector<double> phi(n_presDofs);
            velDiv.div(phi, un);
            phi *= 0.001; //lambda;
            // phi *= 1.0/dt;
            pn = p;
            pn -= phi;

            if ((curEps = phi.linfty_norm()) < 1e-3/*tol*/) {
                break;
            }
            // fprintf(stderr, "tol=%le\n", curEps);
            p = pn;
        }

       // fprintf(stderr, "tol=%le\n", curEps);

        fprintf(stderr, "new p found\n");

//        Vector<double> gradP;
//        presGrad.grad(gradP, pn);

//        fprintf(stderr, "gradP found %le\n", gradP.l2_norm());

//        assert(gradP.size() == n_velDofs*dim);
        i = 0;
        for (;i < n_velDofs*dim; i++) {
            //nextU[i] = output[i] = uAux[i] - dt * gradP[i];
            nextU[i] = output[i] = un[i];
        }
        velDiv.div(divU, nextU);
        fprintf(stderr, "divU %le\n", divU.l2_norm());
        int j = 0;
        for (; j < (int)pn.size(); j++) {
            output[i++] = pn[j];
        }
    }

    void solveUaux(
        Vector<double>& output,
        const Vector<double>& input /*u_x, u_y, u_z*/,
        const Vector<double>& E, /*E_x, E_y, E_z*/
        const Function<dim>& function /* boundary cond */)
    {
        auto& velDof = velSpace.dof();
        int n_velDofs = velDof.n_dofs();

        Vector<double> prev(input.begin(), input.end());
        Vector<double> next(input.begin(), input.end());
        Vector<double> scaledE(E.begin(), E.end());
        Vector<double> tmp(n_velDofs);

        scaledE *= dt;

        for (int d = 0; d < dim; ++d) {
            for (int i = 0; i < n_velDofs; ++i) { tmp[i] = prev[i+d*n_velDofs]; } // tmp = u_d
            if (d == dim - 1) {
                prev += scaledE;
            }
            solvePartU(next, prev, tmp, d, function); // prev = u^*
            next.swap(prev);
        }
        output.swap(prev);
    }

    void solvePartU(
        Vector<double>& output,
        const Vector<double>& input /*u_x, u_y, u_z*/,
        const Vector<double>& u,
        int uDirection,
        const Function<dim>& function /* boundary cond */)
    {
        const int base_max_its = 1000;
        const double solve_eps = 1e-14;

        auto& velDof = velSpace.dof();
        int n_velDofs = velDof.n_dofs();

        buildAdvMatix(u, uDirection);

        Adv *= dt * Re;
        Adv.add(dt, velLapl.matrix());
        Adv.add(1.0, velSpace.mass());

        prec_Adv.initialize(
            Adv,
            SparseILU<double>::AdditionalData(0.1, 70));

        for (int i = 0; i < dim; ++i) {
            Vector<double> tmp(input.begin()+i*n_velDofs, input.begin()+(i+1)*n_velDofs);
            Vector<double> in(n_velDofs);
            Vector<double> out(n_velDofs);

            velSpace.mass().vmult(in, tmp);

            std::map<types::global_dof_index, double> bval;
            auto boundary_ids = velSpace.triangulation().get_boundary_ids();

            for (auto id : boundary_ids) {
                VectorTools::interpolate_boundary_values(
                    velSpace.dof(), id, funcProj(function, i), bval);
            }

            SparseMatrix<double> system_matrix;
            system_matrix.reinit(velSpace.s_pattern());
            system_matrix.copy_from(Adv);

            MatrixTools::apply_boundary_values(
                bval, system_matrix, out, in);

            SolverControl solvercontrol(base_max_its, solve_eps);
            // SolverCG<> cg(solvercontrol);
            SolverGMRES<> gmres(solvercontrol);

            gmres.solve(
                system_matrix,
                out,
                in,
                prec_Adv);

            for (int j = 0; j < n_velDofs; ++j) {
                output[i*n_velDofs + j] = out[j];
            }
        }
    }

private:
    const ModelBase<dim>& velSpace;
    const ModelBase<dim>& presSpace;

    Div<dim> velDiv;
    Lapl<dim> velLapl;
    Lapl<dim> presLapl;
    Grad<dim> velGrad;
    Grad<dim> presGrad;

    const double Re;
    const double dt;
    const double lambda = 1.0; // Chorin parameter
    const double tol = 1e-8; // Iteration process (4) tol

    SparseMatrix<double> Adv;
    SparseILU<double> prec_Adv;

    void buildAdvMatix(const Vector<double>& u, int direction) {
        auto& base = velSpace;
        const unsigned int dpc = base.fe_q().dofs_per_cell,
            nqp = base.q().size();
        std::vector<types::global_dof_index> ldi(dpc);
        std::vector<double> val(nqp);
        std::vector<Tensor<1, dim>> grad(nqp);

        FullMatrix<double> cell_matrix(dpc, dpc);

        Adv.reinit(base.s_pattern());

        auto cell = base.dof().begin_active();
        auto end = base.dof().end();

        FEValues<dim> fe_val(
            base.fe_q(),
            base.q(),
            update_gradients | update_JxW_values | update_values);

        for (; cell != end; ++cell) {
            fe_val.reinit(cell);
            cell->get_dof_indices(ldi);
            // global val -> cell val
            fe_val.get_function_values(u, val);
            fe_val.get_function_gradients(u, grad);

            cell_matrix = 0.0;

            for (unsigned int q = 0; q < nqp; ++q) { // quadrature point
                for (unsigned int i = 0; i < dpc; ++i) { // dofs per cell
                    for (unsigned int j = 0; j < dpc; ++j) {
                        cell_matrix(i, j) -= fe_val.shape_value(j, q) * (
                            val[q] * fe_val.shape_grad(i, q)[direction] + grad[q][direction] * fe_val.shape_value(i, q)
                        ) * fe_val.JxW(q);
                    }
                }
            }

            for (unsigned int i = 0; i < dpc; ++i) {
                for (unsigned int j = 0; j < dpc; ++j) {
//                    fprintf(stderr, "%lf ", cell_matrix(i, j));
                    Adv.add(ldi[i], ldi[j], cell_matrix(i, j));
                }
//                fprintf(stderr, "\n");
            }
//            fprintf(stderr, "\n");
        }
    }
};
