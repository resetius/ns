/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include <stdio.h>

#include "arpack_solver_mode2.h"
#include "initial-values.h"
#include "navier-stokes-lin.h"

namespace Task {

using namespace dealii;
typedef unsigned int uint;

NavierStokesLin::NavierStokesLin(const Parameters& data)
    : NavierStokes2D(data), lin_joint_fe(fe_velocity, dim, fe_pressure, 1),
      lin_joint_dof_handler(triangulation) {
    initialize();
}

void NavierStokesLin::initialize() {
    uint vdofs = dof_handler_velocity.n_dofs();

    uv1.reinit(2 * dof_handler_velocity.n_dofs());
    uv2.reinit(2 * dof_handler_velocity.n_dofs());
    for (uint i = 0; i < dim; ++i) {
        zu[i].reinit(dof_handler_velocity.n_dofs());
        hu[i].reinit(dof_handler_velocity.n_dofs());
    }

    pu.reinit(dof_handler_pressure.n_dofs());

    // test
    if (0) {
        for (uint i = 0; i < vdofs; ++i) {
            u_n[0][i] = 1;
            u_n[1][i] = 2;
        }
        u_n[1][vdofs - 1] = 3;
        output_results(1, "test");
        read_data("test-00001.h5");
        _exit(0);
    }

    if (1) {
        // init z0
        read_data(data.spectral_z0_path);
        for (uint i = 0; i < dim; ++i) {
            zu[i] = u_n[i];
        }
        zp = pres_n;
    }

    {
        DynamicSparsityPattern dsp(
            dim * dof_handler_velocity.n_dofs(),
            dim * dof_handler_velocity.n_dofs());

        auto it = sparsity_pattern_velocity.begin();
        auto end = sparsity_pattern_velocity.end();
        for (; it != end; ++it) {
            for (uint i = 0; i < dim; ++i) {
                for (uint j = 0; j < dim; ++j) {
                    dsp.add(it->row() * dim + i, it->column() * dim + j);
                }
            }
        }
        sparsity_pattern_lin_adv.copy_from(dsp);
        sparsity_pattern_lin_adv.compress();
    }

    vel_lin_Advection.reinit(sparsity_pattern_lin_adv);

    assemble_advection_term();

    //  test_adv();

    //  vel_lin_Advection[0].print_formatted(std::cout);
    //  exit(1);

#if 0
  lin_joint_dof_handler.distribute_dofs(lin_joint_fe);
  Assert(lin_joint_dof_handler.n_dofs() ==
         dim * dof_handler_velocity.n_dofs() +
         dof_handler_pressure.n_dofs(),
         ExcInternalError());
  
  {
    DynamicSparsityPattern dsp(
      lin_joint_dof_handler.n_dofs(),
      lin_joint_dof_handler.n_dofs()
    );
    DoFTools::make_sparsity_pattern (lin_joint_dof_handler, dsp);
    sparsity_pattern_lin.copy_from (dsp);
  }

  lin.reinit(sparsity_pattern_lin);

  FEValues<dim> fe_values (lin_joint_fe,
                           quadrature_velocity,
                           update_values |
                           update_gradients |
                           update_JxW_values);

  //uint i = 0;
  typename DoFHandler<dim>::active_cell_iterator elem, endc;
#endif

/*
  elem = dof_handler_velocity.begin_active();
  endc = dof_handler_velocity.end();
  for (; elem != endc; ++elem) {
    i ++;
  }
  std::cout << i << "\n";
*/
#if 0
  elem = lin_joint_dof_handler.begin_active();
  endc = lin_joint_dof_handler.end();

  uint vel_dofs_per_elem = fe_velocity.dofs_per_cell;
  uint pre_dofs_per_elem = fe_pressure.dofs_per_cell;
  uint dofs_per_elem = lin_joint_fe.dofs_per_cell;
  uint num_quad_pts = quadrature_velocity.size();

  std::vector<uint> local_dof_indices (dofs_per_elem);
  
  std::cout << vel_dofs_per_elem << " "
            << pre_dofs_per_elem << " " << dofs_per_elem << "\n";

//  i = 0;
  for (; elem != endc; ++elem) {
    fe_values.reinit(elem);
    // get [u, v, p]
    fe_values.get_function_values();
    // get grad
    fe_values.get_function_gradients();
    elem->get_dof_indices (local_dof_indices);
    for (uint q = 0; q < num_quad_pts; ++q) {
      for (uint d = 0; d < dim; ++d) {
        for (uint i = 0; i < vel_dofs_per_elem; ++i) {
          for (uint j = 0; j < vel_dofs_per_elem; ++j) {
            
          }
        }
      }
    }
//    i ++;
  }
//  std::cout << i << "\n";
  exit(1);
#endif
}

void NavierStokesLin::test_adv() {
    uint cells = triangulation.n_active_cells();
    uint vdofs = dof_handler_velocity.n_dofs();

    Vector<double> norm;
    norm.reinit(cells);
    for (uint i = 0; i < dim; ++i) {
        zu[i] = 0;
        u_n[i] = 0;
        VectorTools::interpolate(dof_handler_velocity, Z0Test<dim>(i), zu[i]);
        VectorTools::interpolate(dof_handler_velocity, U0Test<dim>(i), u_n[i]);

        u_star[i] = zu[i];
    }

    //  NavierStokes::assemble_advection_term();
    assemble_advection_term();

    //  vel_lin_Advection.print_formatted(std::cout);
    if (1) {
        // apply advection
        uint vdofs = dof_handler_velocity.n_dofs();
        uv1 = 0;
        for (uint d = 0; d < dim; ++d) {
            for (uint i = 0; i < vdofs; ++i) {
                uv1[i * dim + d] = u_n[d][i];
            }
        }
        uv2 = 0;
        vel_lin_Advection.vmult_add(uv2, uv1);
        for (uint i = 0; i < vdofs; ++i) {
            for (uint d = 0; d < dim; ++d) {
                //        std::cout << uv2[i * dim + d] << " ";
                hu[d][i] = uv2[i * dim + d];
            }
            //      std::cout << "\n";
        }
    }

    if (0) {
        for (uint d = 0; d < dim; ++d) {
            hu[d] = 0;
            vel_Advection.vmult_add(hu[d], u_n[d]);
        }
    }

    prec_mass.initialize(vel_Mass);

    for (uint i = 0; i < dim; ++i) {
        prec_mass.solve(hu[i]);
        double tot = 0.0;
        hu[i] *= 2;
        //    VectorTools::integrate_difference(
        //      dof_handler_velocity,
        //      hu[i], AdvP1Test(i), norm, quadrature_velocity,
        //      VectorTools::L2_norm);
        VectorTools::integrate_difference(
            dof_handler_velocity,
            hu[i],
            AdvP2Test<dim>(i),
            norm,
            quadrature_velocity,
            VectorTools::L2_norm);

        v_tmp = 0;
        //    VectorTools::interpolate (dof_handler_velocity, AdvP1Test(i),
        //    v_tmp);
        VectorTools::interpolate(
            dof_handler_velocity, AdvP2Test<dim>(i), v_tmp);

        for (uint j = 0; j < vdofs; ++j) {
            std::cout << zu[i][j] << " " << v_tmp[j] << " -> " << hu[i][j]
                      << "\n";
        }

        for (uint j = 0; j < cells; ++j) {
            //      std::cout << norm[j] << "\n";
            tot += norm[j] * norm[j];
        }
        tot = sqrt(tot);
        fprintf(stdout, "%.16le\n", tot);
        break;
    }

    _exit(0);
}

void NavierStokesLin::assemble_advection_term() {
    std::cout << "assemble lin adv\n";

    AdvectionPerTaskData data(fe_velocity.dofs_per_cell, dim);
    AdvectionScratchData scratch(
        fe_velocity,
        quadrature_velocity,
        update_values | update_JxW_values | update_gradients);
    WorkStream::run(
        dof_handler_velocity.begin_active(),
        dof_handler_velocity.end(),
        *this,
        &NavierStokesLin::assemble_one_cell_of_advection,
        &NavierStokesLin::copy_advection_local_to_global,
        scratch,
        data);
}

void NavierStokesLin::copy_advection_local_to_global(
    const AdvectionPerTaskData& data)
{
//    uint vdofs = dof_handler_velocity.n_dofs();
    for (unsigned int i = 0; i < fe_velocity.dofs_per_cell; ++i)
        for (unsigned int j = 0; j < fe_velocity.dofs_per_cell; ++j)
            for (uint d1 = 0; d1 < dim; ++d1) {
                for (uint d2 = 0; d2 < dim; ++d2) {
                    vel_lin_Advection.add(
                        data.local_dof_indices[i] * dim + d1,
                        data.local_dof_indices[j] * dim + d2,
                        data.local_advection(i * dim + d1, j * dim + d2));
                }
            }
}

void NavierStokesLin::assemble_one_cell_of_advection(
    const typename DoFHandler<dim>::active_cell_iterator& cell,
    AdvectionScratchData& scratch,
    AdvectionPerTaskData& data)
{
    scratch.fe_val.reinit(cell);
    cell->get_dof_indices(data.local_dof_indices);
    for (unsigned int d = 0; d < dim; ++d) {
        scratch.fe_val.get_function_values(zu[d], scratch.u_star_tmp);
        for (unsigned int q = 0; q < scratch.nqp; ++q)
            scratch.z_local[q](d) = scratch.u_star_tmp[q];
    }

    for (unsigned int d = 0; d < dim; ++d) {
        scratch.fe_val.get_function_gradients(zu[d], scratch.grad_u_star);
        for (unsigned int q = 0; q < scratch.nqp; ++q) {
            for (uint dd = 0; dd < dim; ++dd) {
                scratch.z_grad[d][q](dd) = scratch.grad_u_star[q][dd];
            }
        }
    }

    data.local_advection = 0.;
    uint dpc = scratch.dpc;
    double v = 0;
    for (unsigned int q = 0; q < scratch.nqp; ++q) {
        for (unsigned int i = 0; i < dpc; ++i) {
            for (unsigned int j = 0; j < dpc; ++j) {
                for (uint d = 0; d < dim; ++d) {
                    for (uint dd = 0; dd < dim; ++dd) {
                        v =
                            (scratch.z_local[q](dd) *
                             scratch.fe_val.shape_grad(j, q)[dd] *
                             scratch.fe_val.shape_value(i, q)
                             //                 +
                             //                 0.5 *
                             //                 scratch.z_grad[dd][q](dd) *
                             //                 scratch.fe_val.shape_value (i,
                             //                 q) * scratch.fe_val.shape_value
                             //                 (j, q)
                            );
                        data.local_advection(i * dim + d, j * dim + d) +=
                            v * scratch.fe_val.JxW(q);
                    }

                    for (uint dd = 0; dd < dim; ++dd) {
                        v =
                            (scratch.z_grad[d][q](dd) *
                             scratch.fe_val.shape_value(j, q) *
                             scratch.fe_val.shape_value(i, q)
                             //                 +
                             //                 0.5 *
                             //                 scratch.z_local[q](dd) *
                             //                 scratch.fe_val.shape_value (i,
                             //                 q) * scratch.fe_val.shape_grad
                             //                 (j, q)[dd]
                            );
                        // TODO: check this
                        data.local_advection(i * dim + d, j * dim + dd) +=
                            v * scratch.fe_val.JxW(q);
                    }
                }
            }
        }
    }
}

void NavierStokesLin::mass_solve() {
    static int initialized = 0;
    if (!initialized) {
        prec_mass.initialize(vel_Mass);
        initialized = 1;
    }

    for (uint d = 0; d < dim; ++d) {
        prec_mass.solve(u_n[d]);
    }
    //  pres_n = 0;
}

void NavierStokesLin::mass() {
    static int initialized = 0;
    if (!initialized) {
        prec_mass.initialize(vel_Mass);
        initialized = 1;
    }
#if 0
//  std::cout << u_n[0][10] << "\n";

  for (uint d = 0; d < dim; ++d) {
    prec_mass.solve(u_n[d]);
  }
  pres_n = 0;
#endif
        //  std::cout << u_n[0][10] << "\n";

#if 1
    for (uint d = 0; d < dim; ++d) {
        v_tmp = 0;
        vel_Mass.vmult_add(v_tmp, u_n[d]);
        u_n[d] = v_tmp;
    }
    pres_tmp = 0;
    //  pres_Mass.vmult_add(pres_tmp, pres_n);
    pres_n = pres_tmp;
#endif
}

void NavierStokesLin::vmult_mass(
    Vector<double>& dst, const Vector<double>& src) const {
    ((NavierStokes*)this)->set_up(src);
    ((NavierStokesLin*)this)->mass();

    //  dst = src;

    get_up(dst, u_n[0], u_n[1], pres_n);
}

void NavierStokesLin::run(
    const bool /*verbose*/, const unsigned int /*output_interval*/)
{
    // lin
    for (uint d = 0; d < dim; ++d) {
        hu[d] = 0;
    }
    pu = 0.;
    pres_tmp.equ(-1., pres_n);

    {
        // apply advection
        uint vdofs = dof_handler_velocity.n_dofs();
        for (uint d = 0; d < dim; ++d) {
            for (uint i = 0; i < vdofs; ++i) {
                uv1[i * dim + d] = u_n[d][i];
            }
        }
        uv2 = 0;
        vel_lin_Advection.vmult_add(uv2, uv1);
        for (uint d = 0; d < dim; ++d) {
            for (uint i = 0; i < vdofs; ++i) {
                hu[d][i] = uv2[i * dim + d];
            }
        }
    }

    for (uint d = 0; d < dim; ++d) {
        v_tmp.equ(-1. / Re, u_n[d]);
        vel_Laplace.vmult_add(hu[d], v_tmp);

        //    v_tmp.equ(-1, u_n[d]);
        //    vel_lin_Advection.vmult_add(hu[d], v_tmp);
        //    std::cout << zu[d][10] << " " << hu[d][10] << "\n";

        pres_Diff[d].vmult_add(hu[d], pres_tmp);

        v_tmp.equ(-1, u_n[d]);
        pres_Diff[d].Tvmult_add(pu, v_tmp);
    }

    for (uint d = 0; d < dim; ++d) {
        u_n[d] = hu[d];
    }

    //  pres_n = pu;
    pres_n = 0;

    mass_solve();

    //  std::cout << u_n[0][10] << "\n";
}

void NavierStokesLin::spectral() {
    uint vdofs = dof_handler_velocity.n_dofs();
    // uint pdofs = dof_handler_pressure.n_dofs();
    uint n = 2 * vdofs; // + pdofs;

    uint solve = data.spectral_vectors;
    std::vector<std::complex<double>> eigenvalues(solve);
    std::vector<Vector<double>> eigenvectors(solve, Vector<double>(n));
    SolverControl control(1000, 1e-9);

    struct MassMult {
        const NavierStokes* p;
        MassMult(const NavierStokes* p) : p(p) {}
        void vmult(Vector<double>& dst, Vector<double>& src) const {
            p->vmult_mass(dst, src);
        }
    };

    ArpackSolverMode2 solver(
        control,
        ArpackSolverMode2::AdditionalData(
            2 * solve + 2, ArpackSolverMode2::largest_real_part, 1));
    solver.solve(*this, MassMult(this), *this, eigenvalues, eigenvectors, 0);

    xdmf_entries.clear();

    u_n[0] = zu[0];
    u_n[1] = zu[1];
    //  pres_n = zp;

    output_results(1, "spectral");

    for (uint i = 0; i < solve; ++i) {
        std::cout << std::abs(eigenvalues[i]) << " " << eigenvalues[i] << "\n";
        set_up(eigenvectors[i]);

        if (eigenvalues[i].real() > 0) {
            output_results(i + 2, "spectral");
        }
    }
}

} // namespace Task
