# -*- Mode: cmake; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
# vim: set ts=2 sw=2 et:

#
# Are all dependencies fullfilled?
#
IF(NOT DEAL_II_WITH_UMFPACK)
  MESSAGE(FATAL_ERROR "
Error! The deal.II library found at ${DEAL_II_PATH} was not configured with
    DEAL_II_WITH_UMFPACK = ON
One or all of these are OFF in your installation but are required for this tutorial step."
    )
ENDIF()

#IF(NOT DEAL_II_WITH_ARPACK)
#  MESSAGE(FATAL_ERROR "
#Error! The deal.II library found at ${DEAL_II_PATH} was not configured with
#    DEAL_II_WITH_ARPACK = ON
#One or all of these are OFF in your installation but are required for this tutorial step."
#      )
#ENDIF()

DEAL_II_INITIALIZE_CACHED_VARIABLES()

project(navier-stokes)
add_library(ns
  navier-stokes.h
  navier-stokes-impl.h
  navier-stokes-poisson.h
  navier-stokes-poisson-impl.h
  output-solution.h
  initial-values.h
  initial-values-impl.h
  navier-stokes-lin.cc
  parameters.cc

  model_base.cpp model_base.h
  lapl.cpp lapl.h
  div.cpp div.h
  ns-chorin67.cpp ns-chorin67.h
)

add_executable(ns-ex35-main ns-ex35-main.cc)
add_executable(ns-ex353-main ns-ex353-main.cc)
if (DEAL_II_WITH_ARPACK)
add_executable(ns-ex35-spectral-main ns-ex35-spectral-main.cc)
add_executable(ns-lin-spectral-main ns-lin-spectral-main.cc)
endif ()
add_executable(ns-poisson-main ns-poisson-main.cc)
add_executable(ns-poisson3-main ns-poisson3-main.cc)
add_executable(step-11 step-11.cc)
add_executable(step-35 step-35.cc)
add_executable(step-35-modified step-35-modified.cpp)
add_executable(step-35-periodic step-35-periodic.cpp)
add_executable(step-35-spectral step-35-spectral.cpp)

add_executable(grid grid.cpp)
add_executable(grid_cylinder grid_cylinder.cpp)

add_executable(main_lapl main_lapl.cpp)
add_executable(main_div main_div.cpp)

add_executable(ns-chorin67-main ns-chorin67-main.cpp)

DEAL_II_SETUP_TARGET(ns)
DEAL_II_SETUP_TARGET(ns-ex35-main)
DEAL_II_SETUP_TARGET(ns-ex353-main)
if (DEAL_II_WITH_ARPACK)
DEAL_II_SETUP_TARGET(ns-ex35-spectral-main)
DEAL_II_SETUP_TARGET(ns-lin-spectral-main)
endif ()
DEAL_II_SETUP_TARGET(ns-poisson-main)
DEAL_II_SETUP_TARGET(ns-poisson3-main)
DEAL_II_SETUP_TARGET(step-11)
DEAL_II_SETUP_TARGET(step-35)
DEAL_II_SETUP_TARGET(step-35-modified)
DEAL_II_SETUP_TARGET(step-35-periodic)
DEAL_II_SETUP_TARGET(step-35-spectral)
DEAL_II_SETUP_TARGET(grid)
DEAL_II_SETUP_TARGET(grid_cylinder)
DEAL_II_SETUP_TARGET(main_lapl)
DEAL_II_SETUP_TARGET(main_div)
DEAL_II_SETUP_TARGET(ns-chorin67-main)

target_link_libraries(ns-ex35-main ns)
target_link_libraries(ns-ex353-main ns)
if (DEAL_II_WITH_ARPACK)
target_link_libraries(ns-ex35-spectral-main ns)
target_link_libraries(ns-lin-spectral-main ns)
endif ()
target_link_libraries(ns-poisson-main ns)
target_link_libraries(ns-poisson3-main ns)

target_link_libraries(main_lapl ns)
target_link_libraries(main_div ns)
target_link_libraries(ns-chorin67-main ns)
target_link_libraries(step-35-modified ns)

