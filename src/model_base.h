#pragma once

#include <deal.II/base/quadrature_lib.h>

#include <deal.II/lac/sparsity_pattern.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/sparse_direct.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_in.h>

#include <deal.II/fe/fe_q.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/dofs/dof_renumbering.h>

#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/vector_tools.h>

#include <deal.II/grid/grid_tools.h>

#include <string>
#include <fstream>
#include <iostream>

using namespace dealii;

template <int dim>
class Grid {
    ::Triangulation<dim> Triangulation;

public:
    void load(const std::string& fname) {
        std::ifstream input_file(fname);
        GridIn<dim> gridIn;

        gridIn.attach_triangulation(Triangulation);
        gridIn.read_ucd(input_file);
    }

    ::Triangulation<dim>& triangulation() {
        return Triangulation;
    }
};

template <int dim>
class ModelBase {
    Grid<dim>& grid;

    // pressure degree
    int deg;

    FE_Q<dim> fe;

    DoFHandler<dim> dof_handler;

    QGauss<dim> quadrature;

    SparsityPattern sparsity_pattern;

    SparseMatrix<double> Mass;

    SparseDirectUMFPACK prec_mass;

    std::vector<
        GridTools::PeriodicFacePair<typename DoFHandler<dim>::cell_iterator>>
        periodicity_vector;

    AffineConstraints<double> peridicity_constraints;

    void initialize_matrices()
    {
        {
            DynamicSparsityPattern dsp(
                dof_handler.n_dofs(), dof_handler.n_dofs());
            DoFTools::make_sparsity_pattern(dof_handler, dsp);
            sparsity_pattern.copy_from(dsp);
        }

        Mass.reinit(sparsity_pattern);
        MatrixCreator::create_mass_matrix(
            dof_handler, quadrature, Mass);

        prec_mass.initialize(Mass);
    }

    void initialize_matrices_periodic()
    {
        DoFTools::make_periodicity_constraints<DoFHandler<dim>>(
            periodicity_vector,
            peridicity_constraints);

        peridicity_constraints.close();

        {
            DynamicSparsityPattern dsp(
                dof_handler.n_dofs(), dof_handler.n_dofs());
            DoFTools::make_sparsity_pattern(dof_handler, dsp);
            peridicity_constraints.condense(dsp);
            sparsity_pattern.copy_from(dsp);
        }

        Mass.reinit(sparsity_pattern);
        MatrixCreator::create_mass_matrix(
            dof_handler, quadrature, Mass);

        // peridicity_constraints.condense(Mass);

        prec_mass.initialize(Mass);
    }

public:
    ModelBase(Grid<dim>& grid, int deg)
        : grid(grid)
        , deg(deg)
        , fe(deg)
        , quadrature(deg + 1)
    { }

    void initialize_periodic(
        const types::boundary_id b1,
        const types::boundary_id b2,
        const int direction)
    {
        dof_handler.initialize(grid.triangulation(), fe);

        DoFRenumbering::boost::Cuthill_McKee(dof_handler);

        GridTools::collect_periodic_faces(
            dof_handler,
            b1,
            b2,
            direction,
            periodicity_vector);

        initialize_matrices_periodic();
    }

    void initialize()
    {
        dof_handler.initialize(grid.triangulation(), fe);

        DoFRenumbering::boost::Cuthill_McKee(dof_handler);

        initialize_matrices();
    }

    const AffineConstraints<double>& constraints() const {
        return peridicity_constraints;
    }

    const Triangulation<dim>& triangulation() const {
        return grid.triangulation();
    }

    const FE_Q<dim>& fe_q() const
    {
        return fe;
    }

    const DoFHandler<dim>& dof() const
    {
        return dof_handler;
    }

    const Quadrature<dim>& q() const
    {
        return quadrature;
    }

    const SparsityPattern& s_pattern() const
    {
        return sparsity_pattern;
    }

    const SparseMatrix<double>& mass() const
    {
        return Mass;
    }

    void mass_solve(Vector<double>& rhs_and_solutions) const
    {
        prec_mass.solve(rhs_and_solutions);
    }

    void output(Vector<double>& what, const std::string& name, const std::string& fname) const {
        DataOut<dim> dataOut;
        dataOut.attach_triangulation(grid.triangulation());
        dataOut.attach_dof_handler(dof_handler);
        dataOut.add_data_vector(what, name);

        dataOut.build_patches();

        std::ofstream output_file(fname);
        // dataOut.write_eps(output_file);
        // dataOut.write_svg(output_file);
        dataOut.write_gnuplot(output_file);
    }

    double norm_1(const Vector<double>& func) const {
        double norm;
        Vector<double> difference;

        VectorTools::integrate_difference(
            dof(),
            func,
            Functions::ZeroFunction<dim>(),
            difference,
            q(),
            VectorTools::L2_norm);

        norm = VectorTools::compute_global_error(
            grid.triangulation(),
            difference,
            VectorTools::H1_seminorm);

        return norm;
    }

    double norm_1(const Vector<double>& a, const Vector<double>& b) const {
        Vector<double> tmp(a);
        tmp -= b;
        return norm_1(tmp);
    }
};
