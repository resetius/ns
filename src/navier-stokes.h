#pragma once
/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/function.h>
#include <deal.II/base/multithread_info.h>
#include <deal.II/base/parallel.h>
#include <deal.II/base/point.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/thread_management.h>
#include <deal.II/base/utilities.h>
#include <deal.II/base/work_stream.h>

#include <deal.II/lac/affine_constraints.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_gmres.h>
#include <deal.II/lac/sparse_direct.h>
#include <deal.II/lac/sparse_ilu.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/grid/tria.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>

#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_tools.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/vector_tools.h>

#include <cmath>
#include <fstream>
#include <iostream>

#include "parameters.h"

namespace Task {

using namespace dealii;
typedef unsigned int uint;

template<int Dim> class NavierStokes {
  public:
    const static int dim = Dim;

    Triangulation<dim> triangulation;
    const Parameters& data;

    const uint deg;
    const double dt;
    const double t_0, T, Re;

    std::map<types::global_dof_index, double> boundary_values;
    std::vector<types::boundary_id> boundary_ids;

    FE_Q<dim> fe_velocity;
    FE_Q<dim> fe_pressure;

    DoFHandler<dim> dof_handler_velocity;
    DoFHandler<dim> dof_handler_pressure;

    QGauss<dim> quadrature_pressure;
    QGauss<dim> quadrature_velocity;

    unsigned int vel_max_its;
    unsigned int vel_Krylov_size;
    unsigned int vel_off_diagonals;
    unsigned int vel_update_prec;
    double vel_eps;
    double vel_diag_strength;

    SparsityPattern sparsity_pattern_velocity;
    SparsityPattern sparsity_pattern_pressure;
    SparsityPattern sparsity_pattern_pres_vel;

    SparseMatrix<double> vel_it_matrix[dim];
    SparseMatrix<double> vel_Mass;
    SparseMatrix<double> vel_Laplace;
    SparseMatrix<double> vel_Advection;
    SparseMatrix<double> pres_Laplace;
    SparseMatrix<double> pres_Mass;
    SparseMatrix<double> pres_Diff[dim];
    SparseMatrix<double> pres_iterative;
    SparseMatrix<double> vel_Laplace_plus_Mass;

    SparseILU<double> prec_velocity[dim];
    SparseILU<double> prec_pres_Laplace;
    SparseDirectUMFPACK prec_mass;

    Vector<double> F[dim];
    mutable Vector<double> pres_n;
    Vector<double> pres_n_minus_1;
    Vector<double> phi_n;
    Vector<double> phi_n_minus_1;
    mutable Vector<double> u_n[dim];
    Vector<double> u_n_minus_1[dim];
    Vector<double> force[dim];
    Vector<double> v_tmp;
    Vector<double> pres_tmp;
    Vector<double> u_star[dim];

    /*for output only*/
    Vector<double> rot_u;
    Vector<double> psi_u;
    Vector<double> psi_u_tmp;
    SparseMatrix<double> psi_iterative;
    SparseILU<double> prec_psi_iterative;
    /*output*/

    SparseDirectUMFPACK prec_vel_mass;

    explicit NavierStokes(const Parameters& data);
    virtual ~NavierStokes() = default;

    std::vector<XDMFEntry> xdmf_entries;

    void read_data(const std::string& fn);

    void output_results(int step, const std::string& prefix = "solution");

    void output_scalar(
        const std::string& fname, DoFHandler<dim>& dof, Vector<double>& scalar);
    void output_vector(
        const std::string& fname, DoFHandler<dim>& dof, Vector<double>* vector);

    void create_triangulation_and_dofs();

    virtual void initialize();
    void initialize_velocity_matrices();
    void initialize_pressure_matrices();
    void initialize_gradient_operator();

    typedef std::tuple<
        typename DoFHandler<dim>::active_cell_iterator,
        typename DoFHandler<dim>::active_cell_iterator>
        IteratorTuple;

    typedef SynchronousIterators<IteratorTuple> IteratorPair;

    struct InitGradPerTaskData {
        unsigned int d;
        unsigned int vel_dpc;
        unsigned int pres_dpc;
        FullMatrix<double> local_grad;
        //    FullMatrix<double>        local_mass;
        std::vector<types::global_dof_index> vel_local_dof_indices;
        std::vector<types::global_dof_index> pres_local_dof_indices;

        InitGradPerTaskData(
            const unsigned int dd,
            const unsigned int vdpc,
            const unsigned int pdpc)
            : d(dd), vel_dpc(vdpc), pres_dpc(pdpc), local_grad(vdpc, pdpc),
              //      local_mass (vdpc, pdpc),
              vel_local_dof_indices(vdpc), pres_local_dof_indices(pdpc) {}
    };

    struct InitGradScratchData {
        unsigned int nqp;
        FEValues<dim> fe_val_vel;
        FEValues<dim> fe_val_pres;
        InitGradScratchData(
            const FE_Q<dim>& fe_v,
            const FE_Q<dim>& fe_p,
            const QGauss<dim>& quad,
            const UpdateFlags flags_v,
            const UpdateFlags flags_p)
            : nqp(quad.size()), fe_val_vel(fe_v, quad, flags_v),
              fe_val_pres(fe_p, quad, flags_p) {}
        InitGradScratchData(const InitGradScratchData& data)
            : nqp(data.nqp), fe_val_vel(
                                 data.fe_val_vel.get_fe(),
                                 data.fe_val_vel.get_quadrature(),
                                 data.fe_val_vel.get_update_flags()),
              fe_val_pres(
                  data.fe_val_pres.get_fe(),
                  data.fe_val_pres.get_quadrature(),
                  data.fe_val_pres.get_update_flags()) {}
    };

    virtual void assemble_one_cell_of_gradient(
        const IteratorPair& SI,
        InitGradScratchData& scratch,
        InitGradPerTaskData& data);

    virtual void copy_gradient_local_to_global(const InitGradPerTaskData& data);

    virtual void assemble_advection_term();

    struct AdvectionPerTaskData {
        FullMatrix<double> local_advection;
        std::vector<types::global_dof_index> local_dof_indices;
        uint dim; /* used in lin */
        AdvectionPerTaskData(const unsigned int dpc, uint dim = 1)
            : local_advection(dim * dpc, dim * dpc), local_dof_indices(dpc),
              dim(dim) {}
    };

    struct AdvectionScratchData {
        unsigned int nqp;
        unsigned int dpc;
        std::vector<Point<dim>> u_star_local;
        std::vector<Tensor<1, dim>> grad_u_star;
        std::vector<double> u_star_tmp;
        std::vector<Point<dim>> z_local;
        std::vector<Point<dim>> z_grad[dim];
        FEValues<dim> fe_val;
        AdvectionScratchData(
            const FE_Q<dim>& fe,
            const QGauss<dim>& quad,
            const UpdateFlags flags)
            : nqp(quad.size()), dpc(fe.dofs_per_cell), u_star_local(nqp),
              grad_u_star(nqp), u_star_tmp(nqp), z_local(nqp),
              fe_val(fe, quad, flags) {
            for (uint d = 0; d < dim; ++d) {
                z_grad[d].resize(nqp);
            }
        }

        AdvectionScratchData(const AdvectionScratchData& data)
            : nqp(data.nqp), dpc(data.dpc), u_star_local(nqp), grad_u_star(nqp),
              u_star_tmp(nqp), z_local(nqp),
              fe_val(
                  data.fe_val.get_fe(),
                  data.fe_val.get_quadrature(),
                  data.fe_val.get_update_flags()) {
            for (uint d = 0; d < dim; ++d) {
                z_grad[d].resize(nqp);
            }
        }
    };

    virtual void assemble_one_cell_of_advection(
        const typename DoFHandler<dim>::active_cell_iterator& cell,
        AdvectionScratchData& scratch,
        AdvectionPerTaskData& data);

    virtual void copy_advection_local_to_global(const AdvectionPerTaskData& data);

    void diffusion_component_solve(const unsigned int d);

    void assemble_vorticity(const bool reinit_prec);

    void set_u(const double* u, const double* v);
    void set_p(const double* p);
    void set_up(const Vector<double>& uvp);
    void get_up(
        Vector<double>& uvp,
        const Vector<double>& u,
        const Vector<double>& v,
        const Vector<double>& p) const;
    virtual void vmult(Vector<double>& dst, const Vector<double>& src) const;
    virtual void
    vmult_mass(Vector<double>& dst, const Vector<double>& src) const;
#ifdef DEAL_II_WITH_ARPACK
    virtual void spectral();
#endif
    virtual void
    run(const bool /*verbose*/, const unsigned int /*output_interval*/){};
};

typedef NavierStokes<2> NavierStokes2D;
typedef NavierStokes<3> NavierStokes3D;

} // namespace Task

#include "navier-stokes-impl.h"
#include "output-solution.h"
