/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/function.h>
#include <deal.II/base/multithread_info.h>
#include <deal.II/base/parallel.h>
#include <deal.II/base/point.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/thread_management.h>
#include <deal.II/base/utilities.h>
#include <deal.II/base/work_stream.h>

#include <deal.II/lac/affine_constraints.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_gmres.h>
#include <deal.II/lac/sparse_direct.h>
#include <deal.II/lac/sparse_ilu.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/manifold_lib.h>
#include <deal.II/grid/tria.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>

#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_tools.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/vector_tools.h>

#include <cmath>
#include <fstream>
#include <iostream>

#include "arpack_solver_mode2.h"
#include "initial-values.h"
#include "navier-stokes.h"
#include "parameters.h"

namespace Task {

using namespace dealii;
typedef unsigned int uint;

template<int dim>
NavierStokes<dim>::NavierStokes(const Parameters& data)
    : triangulation(Triangulation<dim>::maximum_smoothing, true),

      data(data), deg(data.pressure_degree),

      dt(data.dt),

      t_0(data.initial_time), T(data.final_time), Re(data.Reynolds),

      fe_velocity(deg + 1), fe_pressure(deg),

      dof_handler_velocity(triangulation), dof_handler_pressure(triangulation),

      quadrature_pressure(deg + 1), quadrature_velocity(deg + 2),

      vel_max_its(data.vel_max_iterations),
      vel_Krylov_size(data.vel_Krylov_size),
      vel_off_diagonals(data.vel_off_diagonals),
      vel_update_prec(data.vel_update_prec), vel_eps(data.vel_eps),
      vel_diag_strength(data.vel_diag_strength)
{
    create_triangulation_and_dofs();
    initialize();
}

template<int dim> void NavierStokes<dim>::create_triangulation_and_dofs() {
    const Point<dim> center;

    if (dim == 2 || data.use_sphere_in_3d) {
        GridGenerator::hyper_shell(
            triangulation,
            center,
            data.inner_radius, /* inner */
            data.outer_radius, /* outer */
            data.mesh_initial_cells,
            true /* colorize boundaries */
        );
        //use CylindricalManifold
        //static const HyperShellBoundary<dim> boundary;
        //triangulation.set_manifold(0, boundary);
        //triangulation.set_manifold(1, boundary);

        //typename Triangulation<dim>::active_cell_iterator
        //    cell = triangulation.begin_active(),
        //    endc = triangulation.end();

        //use SphericalManifold
        //static const SphericalManifold<dim> boundary_description(center);
        //triangulation.set_manifold(0, boundary_description);
        //for (; cell != endc; ++cell) {
        //    cell->set_all_manifold_ids(0);
        //}
    } else if (dim == 3) {
        GridGenerator::cylinder_shell(
            triangulation,
            data.z_length,
            data.inner_radius,
            data.outer_radius,
            0,
            4);

        //    static const CylinderBoundary<dim> boundary1(data.inner_radius,
        //    2); static const CylinderBoundary<dim>
        //    boundary2(data.outer_radius, 2); for (uint i = 0; i <= 5; ++i) {
        // flat boundary
        //      triangulation.set_boundary(i, boundary2);
        //    }

        //    triangulation.set_boundary(6, boundary1);

        typename Triangulation<dim>::active_cell_iterator
            cell = triangulation.begin_active(),
            endc = triangulation.end();

        static const CylindricalManifold<dim> boundary_description(
            2); // 2 - z axis
        triangulation.set_manifold(0, boundary_description);
        for (; cell != endc; ++cell) {
            cell->set_all_manifold_ids(0);
            for (uint f = 0; f < GeometryInfo<dim>::faces_per_cell; ++f) {
                auto face = cell->face(f);
                if (face->boundary_id() == 0) {
                    Point<dim> center(face->center());
                    double x = center(0);
                    double y = center(1);
                    double z = center(2);
                    //          double r = sqrt(x*x+y*y+z*z);

                    double r = sqrt(x*x+y*y);

                    if (fabs(z - 0) < 1e-10) {
                        // bottom
                        face->set_boundary_id(3);
                        //            std::cout << "bottom\n";
                    } else if (fabs(z - data.z_length) < 1e-10) {
                        // top
                        face->set_boundary_id(2);
                        //            std::cout << "top\n";
                    } else {
                        // circles
                        // face->set_boundary_id(1);

                        if (fabs(r - data.inner_radius) < 1e-1) {
                            face->set_boundary_id(4);
                        } else {
                            face->set_boundary_id(5);
                        }
                        //            std::cout << "circle\n";
                    }
                    //std::cerr << "p: " << data.inner_radius << " " << r << " " << x << " " << y << "\n";
                    //          << " " << z << "\n";
                } else {
                    // std::cout <<(int) face->boundary_id() << "\n";
                }
            }
        }
    } else {
        Assert(false, ExcNotImplemented());
    }
    //  _exit(0);

    triangulation.refine_global(data.mesh_refines);
//  triangulation.execute_coarsening_and_refinement();
#if 0
  cell = triangulation.begin_active();
  endc = triangulation.end();
  for (; cell!=endc; ++cell)
  {
    for (unsigned int v=0;
         v < GeometryInfo<dim>::vertices_per_cell;
         ++v)
    {
      const double distance_from_center
        = center.distance (cell->vertex(v));
      if (distance_from_center <
                    (data.inner_radius +
                     0.95 * (data.outer_radius - data.inner_radius)))
      {
        cell->set_refine_flag ();
        //cell->flag_for_face_refinement(0);
        //cell->flag_for_face_refinement(1);
        break;
      }
    }
  }
  triangulation.execute_coarsening_and_refinement ();
#endif

    /*
      for (unsigned int step=0; step<5; ++step)
      {
        Triangulation<2>::active_cell_iterator
          cell = triangulation.begin_active(),
          endc = triangulation.end();
        for (; cell!=endc; ++cell)
        {
          for (unsigned int v=0;
               v < GeometryInfo<2>::vertices_per_cell;
               ++v)
          {
            const double distance_from_center
              = center.distance (cell->vertex(v));
            if (std::fabs(distance_from_center - data.inner_radius) < 1e-10)
            {
              cell->set_refine_flag ();
              break;
            }
          }
        }
        triangulation.execute_coarsening_and_refinement ();
      }
    */

    //  GridTools::distort_random(0.3, triangulation);

    boundary_ids = triangulation.get_boundary_ids();

    dof_handler_velocity.distribute_dofs(fe_velocity);
    DoFRenumbering::boost::Cuthill_McKee(dof_handler_velocity);
    dof_handler_pressure.distribute_dofs(fe_pressure);
    DoFRenumbering::boost::Cuthill_McKee(dof_handler_pressure);

    initialize_velocity_matrices();
    initialize_pressure_matrices();
    initialize_gradient_operator();

    pres_n.reinit(dof_handler_pressure.n_dofs());
    pres_n_minus_1.reinit(dof_handler_pressure.n_dofs());
    phi_n.reinit(dof_handler_pressure.n_dofs());
    phi_n_minus_1.reinit(dof_handler_pressure.n_dofs());
    pres_tmp.reinit(dof_handler_pressure.n_dofs());

    for (uint d = 0; d < dim; ++d) {
        u_star[d].reinit(dof_handler_velocity.n_dofs());
        u_n[d].reinit(dof_handler_velocity.n_dofs());
        u_n_minus_1[d].reinit(dof_handler_velocity.n_dofs());
        force[d].reinit(dof_handler_velocity.n_dofs());
        F[d].reinit(dof_handler_velocity.n_dofs());
    }
    v_tmp.reinit(dof_handler_velocity.n_dofs());
    rot_u.reinit(dof_handler_velocity.n_dofs());
    psi_u.reinit(dof_handler_velocity.n_dofs());
    psi_u_tmp.reinit(dof_handler_velocity.n_dofs());
}

template<int dim> void NavierStokes<dim>::initialize() {
    phi_n = 0.;
    phi_n_minus_1 = 0.;

    for (uint i = 0; i < dim; ++i) {
        u_n_minus_1[i] = 0;
        u_n[i] = 0;
        if (data.use_sphere_in_3d) {
            F[i] = 0.0;
        } else {
            //VectorTools::interpolate(
            //    dof_handler_velocity,
            //    Force<dim>(i, data.use_sphere_in_3d),
            //    F[i]);

            VectorTools::interpolate(
                dof_handler_velocity,
                ZeroFunction<dim>(),
                F[i]);
        }
    }
}

template<int dim> void NavierStokes<dim>::initialize_velocity_matrices() {
    {
        DynamicSparsityPattern dsp(
            dof_handler_velocity.n_dofs(), dof_handler_velocity.n_dofs());
        DoFTools::make_sparsity_pattern(dof_handler_velocity, dsp);
        sparsity_pattern_velocity.copy_from(dsp);
    }
    for (unsigned int d = 0; d < dim; ++d)
        vel_it_matrix[d].reinit(sparsity_pattern_velocity);
    vel_Mass.reinit(sparsity_pattern_velocity);
    vel_Laplace.reinit(sparsity_pattern_velocity);
    vel_Advection.reinit(sparsity_pattern_velocity);

    MatrixCreator::create_mass_matrix(
        dof_handler_velocity, quadrature_velocity, vel_Mass);
    MatrixCreator::create_laplace_matrix(
        dof_handler_velocity, quadrature_velocity, vel_Laplace);
}

// The initialization of the matrices that act on the pressure space is
// similar to the ones that act on the velocity space.
template<int dim> void NavierStokes<dim>::initialize_pressure_matrices() {
    {
        DynamicSparsityPattern dsp(
            dof_handler_pressure.n_dofs(), dof_handler_pressure.n_dofs());
        DoFTools::make_sparsity_pattern(dof_handler_pressure, dsp);
        sparsity_pattern_pressure.copy_from(dsp);
    }

    pres_Laplace.reinit(sparsity_pattern_pressure);
    pres_iterative.reinit(sparsity_pattern_pressure);
    pres_Mass.reinit(sparsity_pattern_pressure);

    MatrixCreator::create_laplace_matrix(
        dof_handler_pressure, quadrature_pressure, pres_Laplace);
    MatrixCreator::create_mass_matrix(
        dof_handler_pressure, quadrature_pressure, pres_Mass);
}

template<int dim> void NavierStokes<dim>::initialize_gradient_operator() {
    {
        DynamicSparsityPattern dsp(
            dof_handler_velocity.n_dofs(), dof_handler_pressure.n_dofs());
        DoFTools::make_sparsity_pattern(
            dof_handler_velocity, dof_handler_pressure, dsp);
        sparsity_pattern_pres_vel.copy_from(dsp);
    }

    InitGradPerTaskData per_task_data(
        0, fe_velocity.dofs_per_cell, fe_pressure.dofs_per_cell);
    InitGradScratchData scratch_data(
        fe_velocity,
        fe_pressure,
        quadrature_velocity,
        update_values | update_gradients | update_JxW_values,
        update_values);

    MatrixCreator::create_mass_matrix(
        dof_handler_velocity, quadrature_velocity, vel_Mass);
    //  vel_pres_Mass.reinit(sparsity_pattern_pres_vel);

    for (unsigned int d = 0; d < dim; ++d) {
        pres_Diff[d].reinit(sparsity_pattern_pres_vel);
        per_task_data.d = d;
        WorkStream::run(
            IteratorPair(IteratorTuple(
                dof_handler_velocity.begin_active(),
                dof_handler_pressure.begin_active())),
            IteratorPair(IteratorTuple(
                dof_handler_velocity.end(), dof_handler_pressure.end())),
            *this,
            &NavierStokes<dim>::assemble_one_cell_of_gradient,
            &NavierStokes<dim>::copy_gradient_local_to_global,
            scratch_data,
            per_task_data);
    }
}

template<int dim>
void NavierStokes<dim>::assemble_one_cell_of_gradient(
    const IteratorPair& SI,
    InitGradScratchData& scratch,
    InitGradPerTaskData& data) {
    scratch.fe_val_vel.reinit(std::get<0>(*SI));
    scratch.fe_val_pres.reinit(std::get<1>(*SI));

    std::get<0>(*SI)->get_dof_indices(data.vel_local_dof_indices);
    std::get<1>(*SI)->get_dof_indices(data.pres_local_dof_indices);

    data.local_grad = 0.;
    //  data.local_mass = 0.;
    for (unsigned int q = 0; q < scratch.nqp; ++q) {
        for (unsigned int i = 0; i < data.vel_dpc; ++i) {
            for (unsigned int j = 0; j < data.pres_dpc; ++j) {
                double v = -scratch.fe_val_vel.JxW(q) *
                           scratch.fe_val_vel.shape_grad(i, q)[data.d] *
                           scratch.fe_val_pres.shape_value(j, q);
                data.local_grad(i, j) += v;

                //        if (data.d == 0) {
                //          v = - scratch.fe_val_vel.JxW(q) *
                //            scratch.fe_val_vel.shape_value (i, q) *
                //            scratch.fe_val_pres.shape_value (j, q);
                //          data.local_mass (i, j) += v;
                //        }
            }
        }
    }
}

template<int dim>
void NavierStokes<dim>::copy_gradient_local_to_global(
    const InitGradPerTaskData& data) {
    for (unsigned int i = 0; i < data.vel_dpc; ++i) {
        for (unsigned int j = 0; j < data.pres_dpc; ++j) {
            pres_Diff[data.d].add(
                data.vel_local_dof_indices[i],
                data.pres_local_dof_indices[j],
                data.local_grad(i, j));

            //      if (data.d == 0) {
            //        vel_pres_Mass.add (data.vel_local_dof_indices[i],
            //        data.pres_local_dof_indices[j],
            //                           data.local_mass (i, j) );
            //      }
        }
    }
}

template<int dim> void NavierStokes<dim>::assemble_advection_term() {
    vel_Advection = 0.;
    AdvectionPerTaskData data(fe_velocity.dofs_per_cell);
    AdvectionScratchData scratch(
        fe_velocity,
        quadrature_velocity,
        update_values | update_JxW_values | update_gradients);
    WorkStream::run(
        dof_handler_velocity.begin_active(),
        dof_handler_velocity.end(),
        *this,
        &NavierStokes<dim>::assemble_one_cell_of_advection,
        &NavierStokes<dim>::copy_advection_local_to_global,
        scratch,
        data);
}

template<int dim>
void NavierStokes<dim>::assemble_one_cell_of_advection(
    const typename DoFHandler<dim>::active_cell_iterator& cell,
    AdvectionScratchData& scratch,
    AdvectionPerTaskData& data) {
    scratch.fe_val.reinit(cell);
    cell->get_dof_indices(data.local_dof_indices);
    for (unsigned int d = 0; d < dim; ++d) {
        scratch.fe_val.get_function_values(u_star[d], scratch.u_star_tmp);
        for (unsigned int q = 0; q < scratch.nqp; ++q)
            scratch.u_star_local[q](d) = scratch.u_star_tmp[q];
    }

    for (unsigned int d = 0; d < dim; ++d) {
        scratch.fe_val.get_function_gradients(u_star[d], scratch.grad_u_star);
        for (unsigned int q = 0; q < scratch.nqp; ++q) {
            if (d == 0)
                scratch.u_star_tmp[q] = 0.;
            scratch.u_star_tmp[q] += scratch.grad_u_star[q][d];
        }
    }

    data.local_advection = 0.;
    for (unsigned int q = 0; q < scratch.nqp; ++q)
        for (unsigned int i = 0; i < scratch.dpc; ++i)
            for (unsigned int j = 0; j < scratch.dpc; ++j)
                data.local_advection(i, j) +=
                    (scratch.u_star_local[q] * scratch.fe_val.shape_grad(j, q) *
                         scratch.fe_val.shape_value(i, q) +
                     0.5 * scratch.u_star_tmp[q] *
                         scratch.fe_val.shape_value(i, q) *
                         scratch.fe_val.shape_value(j, q)) *
                    scratch.fe_val.JxW(q);
}

template<int dim>
void NavierStokes<dim>::copy_advection_local_to_global(
    const AdvectionPerTaskData& data) {
    for (unsigned int i = 0; i < fe_velocity.dofs_per_cell; ++i)
        for (unsigned int j = 0; j < fe_velocity.dofs_per_cell; ++j)
            vel_Advection.add(
                data.local_dof_indices[i],
                data.local_dof_indices[j],
                data.local_advection(i, j));
}

template<int dim>
void NavierStokes<dim>::diffusion_component_solve(const unsigned int d) {
    SolverControl solver_control(vel_max_its, vel_eps * force[d].l2_norm());
    SolverGMRES<> gmres(
        solver_control, SolverGMRES<>::AdditionalData(vel_Krylov_size));
    gmres.solve(vel_it_matrix[d], u_n[d], force[d], prec_velocity[d]);
}

template<int dim>
void NavierStokes<dim>::set_u(const double* u, const double* v) {
    for (uint i = 0; i < dof_handler_velocity.n_dofs(); ++i) {
        u_star[0][i] = u_n_minus_1[0][i] = u_n[0][i] = u[i];
        u_star[1][i] = u_n_minus_1[1][i] = u_n[1][i] = v[i];
    }
}

template<int dim> void NavierStokes<dim>::set_p(const double* p) {
    for (uint i = 0; i < dof_handler_pressure.n_dofs(); ++i) {
        pres_n_minus_1[i] = pres_n[i] = p[i];
    }
}

template<int dim> void NavierStokes<dim>::set_up(const Vector<double>& uvp) {
    uint vdofs = dof_handler_velocity.n_dofs();
    uint pdofs = dof_handler_pressure.n_dofs();

    std::vector<double> u(vdofs);
    std::vector<double> v(vdofs);
    std::vector<double> p(pdofs);

    for (uint i = 0; i < vdofs; ++i) {
        u[i] = uvp[0 * vdofs + i];
        v[i] = uvp[1 * vdofs + i];
    }

    for (uint i = 0; i < pdofs; ++i) {
        p[i] = uvp[2 * vdofs + i];
    }

    set_u(&u[0], &v[0]);
    set_p(&p[0]);
}

template<int dim>
void NavierStokes<dim>::get_up(
    Vector<double>& dst,
    const Vector<double>& u,
    const Vector<double>& v,
    const Vector<double>& p) const {
    uint vdofs = dof_handler_velocity.n_dofs();
    uint pdofs = dof_handler_pressure.n_dofs();

    for (uint i = 0; i < vdofs; ++i) {
        dst[0 * vdofs + i] = u[i];
        dst[1 * vdofs + i] = v[i];
    }
    for (uint i = 0; i < pdofs; ++i) {
        dst[2 * vdofs + i] = p[i];
    }
}

template<int dim>
void NavierStokes<dim>::vmult(
    Vector<double>& dst, const Vector<double>& src) const {
    ((NavierStokes*)this)->set_up(src);
    const unsigned int n_steps = static_cast<unsigned int>((T - t_0) / dt);

    ((NavierStokes*)this)->run(false, 2 * n_steps);

    get_up(dst, u_n[0], u_n[1], pres_n);
}

template<int dim>
void NavierStokes<dim>::vmult_mass(
    Vector<double>& dst, const Vector<double>& src) const {
    dst = src;
}

#ifdef DEAL_II_WITH_ARPACK

template<int dim> void NavierStokes<dim>::spectral() {
    uint vdofs = dof_handler_velocity.n_dofs();
    uint pdofs = dof_handler_pressure.n_dofs();
    uint n = 2 * vdofs + pdofs;

    uint solve = 10;
    std::vector<std::complex<double>> eigenvalues(solve);
    std::vector<Vector<double>> eigenvectors(solve, Vector<double>(n));
    SolverControl control(1000, 1e-9);

    struct MassMult {
        const NavierStokes* p;
        MassMult(const NavierStokes* p) : p(p) {}
        void vmult(Vector<double>& dst, Vector<double>& src) const {
            p->vmult_mass(dst, src);
        }
    };

    ArpackSolverMode2 solver(
        control,
        ArpackSolverMode2::AdditionalData(
            2 * solve + 2, ArpackSolverMode2::largest_magnitude, 3));
    solver.solve(*this, MassMult(this), *this, eigenvalues, eigenvectors, 0);
    for (uint i = 0; i < solve; ++i) {
        std::cout << std::abs(eigenvalues[i]) << " " << eigenvalues[i] << "\n";
    }
}
#endif

} // namespace Task
