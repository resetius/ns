//
// Created by aozeritsky on 07.05.19.
//

#include <deal.II/base/point.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_generator.h>

#define _USE_MATH_DEFINES

#include <string>
#include <iostream>
#include <fstream>

#include <math.h>

using namespace dealii;
using namespace std;

template<int dim>
void doall(int mesh_refines, const string& outbase)
{
    Triangulation<dim> triangulation;

    Point<dim> p1;
    Point<dim> p2;

    fprintf(stderr, "build mesh of %d dim\n", dim);

    if (dim == 2) {
        p1 = Point<dim>(0, 0);
        //p2 = Point<dim>(M_PI, M_PI);
        p2 = Point<dim>(2*M_PI, 2*M_PI); // for periodicity
    } else {
        p1 = Point<dim>(0, 0, 0);
        //p2 = Point<dim>(M_PI, M_PI, M_PI);
        p2 = Point<dim>(2*M_PI, 2*M_PI, 2*M_PI); // for periodicity
    }

    bool colorize = true;

    // If the colorize flag is true, the boundary_ids of the boundary faces are assigned, such that the lower one in x-direction is 0, the upper one is 1.
    // The indicators for the surfaces in y-direction are 2 and 3, the ones for z are 4 and 5.
    // This corresponds to the numbers of faces of the unit square of cube as laid out in the documentation of the GeometryInfo class.
    // Importantly, however, in 3d colorization does not set boundary_ids of edges, but only of faces,
    // because each boundary edge is shared between two faces and it is not clear how the boundary id of an edge should be set in that case.

    GridGenerator::hyper_rectangle(
        triangulation, p1, p2, colorize
        );

    triangulation.refine_global(mesh_refines);

    GridOut out;

    {
        ofstream output_file(outbase + ".vtk");
        out.write_vtk(triangulation, output_file);
    }

    { // save with boundary ids
        ofstream output_file(outbase + ".ucd");
        out.set_flags (GridOutFlags::Ucd(true, true, true));
        out.write_ucd(triangulation, output_file);
    }

    if (0) {
        GridOutFlags::Svg flags;
        // flags.label_material_id = true;
        ofstream output_file(outbase + ".svg");
        out.set_flags(flags);
        out.write_svg(triangulation, output_file);
    }

    {
        ofstream output_file(outbase + ".eps");
        out.write_eps(triangulation, output_file);
    }
}

int main(int argc, char** argv) {
    int mesh_refines = 2;
    string outbase = "gridout";
    if (argc > 1) {
        mesh_refines = atoi(argv[1]);
    }
    if (argc > 2) {
        outbase = argv[2];
    }
    if (argc > 3) {
        if (!strcmp(argv[3], "3")) {
            doall<3>(mesh_refines, outbase);
        } else if (!strcmp(argv[3], "2")) {
            doall<2>(mesh_refines, outbase);
        } else {
            fprintf(stderr, "invalid dim %s", argv[3]);
            return -1;
        }
    } else {
        doall<2>(mesh_refines, outbase);
    }
    return 0;
}
