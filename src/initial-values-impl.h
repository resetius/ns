/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include "initial-values.h"
#include <iostream>

namespace Task {

template<int dim>
double Velocity<dim>::value(const Point<dim>&, const uint /*component*/) const {
    return 0;
}

template<int dim>
double Pressure<dim>::value(const Point<dim>&, const uint /*component*/) const {
    return 0;
}


template<int dim>
double RotationalCylinder<dim>::value(const Point<dim>& p, const uint /*component*/) const {
    double x = p(0) / p.norm();
    double y = p(1) / p.norm();

    // fprintf(stderr, "direction -> %d \n", direction);

    if (direction == 0 /*x?*/) {
        return y;
    } else if (direction == 1 /*y?*/) {
        return -x;
    } else if (direction == 2 /*z?*/) {
        return 0;
    }

    return 0;
}

template<int dim>
double Force<dim>::value(const Point<dim>& p, const uint /*component*/) const {
    double x = p(0);
    double y = p(1);
    double a = 1.0;

    double gR1 = 1;
    double gR2 = 0.35;

    double pi = M_PI;

    double F1, F2;

    if (dim == 2) {
        double Fphi = a * (-0.345) *
                      sin(2 * pi * (sqrt(x * x + y * y) - gR2) / (gR1 - gR2)) *
                      (sqrt(x * x + y * y) < gR1) * (sqrt(x * x + y * y) > gR2);

        F1 = -Fphi * sin(acos(fabs(x) / sqrt(x * x + y * y))) * (y >= 0.) *
                 (x >= 0.) -
             Fphi * sin(acos(fabs(x) / sqrt(x * x + y * y))) * (y >= 0.) *
                 (x < 0.) +
             Fphi * sin(acos(fabs(x) / sqrt(x * x + y * y))) * (y < 0.) *
                 (x < 0.) +
             Fphi * sin(acos(fabs(x) / sqrt(x * x + y * y))) * (y < 0.) *
                 (x >= 0.);

        F2 = +Fphi * cos(acos(fabs(x) / sqrt(x * x + y * y))) * (y >= 0.) *
                 (x >= 0.) -
             Fphi * cos(acos(fabs(x) / sqrt(x * x + y * y))) * (y >= 0.) *
                 (x < 0.) -
             Fphi * cos(acos(fabs(x) / sqrt(x * x + y * y))) * (y < 0.) *
                 (x < 0.) +
             Fphi * cos(acos(fabs(x) / sqrt(x * x + y * y))) * (y < 0.) *
                 (x >= 0.);
    } else {
        // double z = 0; // p(2);

        double r = sqrt(x * x + y * y);

        double Fphi = a * (-0.345) * sin(2 * pi * (r - gR2) / (gR1 - gR2)) *
                      (r < gR1) * (r > gR2);

        if (r > (gR1 + gR2) / 2) {
            Fphi *= 1.005;
        }

        if (sphere) {
            Fphi = 10 * r;
        }

        F1 = -Fphi * sin(acos(fabs(x) / r)) * (y >= 0.) * (x >= 0.) -
             Fphi * sin(acos(fabs(x) / r)) * (y >= 0.) * (x < 0.) +
             Fphi * sin(acos(fabs(x) / r)) * (y < 0.) * (x < 0.) +
             Fphi * sin(acos(fabs(x) / r)) * (y < 0.) * (x >= 0.);

        F2 = +Fphi * cos(acos(fabs(x) / r)) * (y >= 0.) * (x >= 0.) -
             Fphi * cos(acos(fabs(x) / r)) * (y >= 0.) * (x < 0.) -
             Fphi * cos(acos(fabs(x) / r)) * (y < 0.) * (x < 0.) +
             Fphi * cos(acos(fabs(x) / r)) * (y < 0.) * (x >= 0.);

        if (sphere && fabs(r) < 1e-8) {
            F1 = F2 = 0;
        }
    }
    switch (direction) {
        case 0: // x
            return F1;
        case 1: // y
            return F2;
        case 2: // z
            return 0;
        default:
            Assert(false, ExcNotImplemented());
    }
    Assert(false, ExcNotImplemented());
    return -1;
}

static Force<2> f2(0);
static Force<3> f3(0);

template<int dim>
double Z0Test<dim>::value(const Point<dim>& p, const uint /*component*/) const {
    double x = p(0);
    double y = p(1);
    double R = 1;
    double r = 0.35;

    double F1 = -(x * x + y * y - r) * (x * x + y * y - R);
    double F2 = (x * x + y * y - r) * (x * x + y * y - R);
    if (direction == 0) {
        return F1;
    } else if (direction == 1) {
        return F2;
    }
    Assert(false, ExcNotImplemented());
    return -1;
}

template<int dim>
double U0Test<dim>::value(const Point<dim>& p, const uint /*component*/) const {
    double x = p(0);
    double y = p(1);
    double R = 1;
    double r = 0.35;

    double F1 = -x * (x * x + y * y - r) * (x * x + y * y - R);
    double F2 = y * (x * x + y * y - r) * (x * x + y * y - R);
    if (direction == 0) {
        return F1;
    } else if (direction == 1) {
        return F2;
    }

    Assert(false, ExcNotImplemented());
    return -1;
}

template<int dim>
double
AdvP1Test<dim>::value(const Point<dim>& p, const uint /*component*/) const {
    double x = p(0);
    double y = p(1);
    double R = 1;
    double r = 0.35;

    double F1 = (-y * y - x * x + r) *
                    (-(y * y + x * x - r) * (-R + y * y + x * x) -
                     2 * x * x * (-R + y * y + x * x) -
                     2 * x * x * (y * y + x * x - r)) *
                    (-R + y * y + x * x) +
                (y * y + x * x - r) *
                    (-2 * x * y * (-R + y * y + x * x) -
                     2 * x * y * (y * y + x * x - r)) *
                    (-R + y * y + x * x);
    double F2 = (y * y + x * x - r) *
                    ((y * y + x * x - r) * (-R + y * y + x * x) +
                     2 * y * y * (-R + y * y + x * x) +
                     2 * y * y * (y * y + x * x - r)) *
                    (-R + y * y + x * x) +
                (-y * y - x * x + r) *
                    (2 * x * y * (-R + y * y + x * x) +
                     2 * x * y * (y * y + x * x - r)) *
                    (-R + y * y + x * x);

    //  double F1 = (-y*y-x*x+r)*
    //    (-(y*y+x*x-r)*(-R+y*y+x*x)-2*x*x*(-R+y*y+x*x)-2*x*x*(y*y+x*x-r))*
    //    (-R+y*y+x*x);
    //  double F2 = 0;

    if (direction == 0) {
        return F1;
    } else if (direction == 1) {
        return F2;
    }

    Assert(false, ExcNotImplemented());
    return -1;
}

template<int dim>
double
AdvP2Test<dim>::value(const Point<dim>& p, const uint /*component*/) const {
    double x = p(0);
    double y = p(1);
    double R = 1;
    double r = 0.35;

    double F1 =
        y * (y * y + x * x - r) *
            (2 * y * (-y * y - x * x + r) - 2 * y * (-R + y * y + x * x)) *
            (-R + y * y + x * x) -
        x * (y * y + x * x - r) *
            (2 * x * (-y * y - x * x + r) - 2 * x * (-R + y * y + x * x)) *
            (-R + y * y + x * x);
    double F2 =
        y * (y * y + x * x - r) *
            (2 * y * (-R + y * y + x * x) + 2 * y * (y * y + x * x - r)) *
            (-R + y * y + x * x) -
        x * (y * y + x * x - r) *
            (2 * x * (-R + y * y + x * x) + 2 * x * (y * y + x * x - r)) *
            (-R + y * y + x * x);
    if (direction == 0) {
        return F1;
    } else if (direction == 1) {
        return F2;
    }

    Assert(false, ExcNotImplemented());
    return -1;
}

} // namespace Task
