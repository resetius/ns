/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include "navier-stokes-lin.h"

int main(int argc, char** argv) {
    try {
        using namespace dealii;
        using namespace Task;
#ifdef DEAL_II_WITH_MPI
        MPI_Init(&argc, &argv);
#endif
        Parameters data;
        data.read_data("parameter-file.prm");

        deallog.depth_console(data.verbose ? 2 : 0);

        NavierStokesLin test(data);
        test.spectral();
    } catch (std::exception& exc) {
        std::cerr << std::endl
                  << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        std::cerr << "Exception on processing: " << std::endl
                  << exc.what() << std::endl
                  << "Aborting!" << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        return 1;
    } catch (...) {
        std::cerr << std::endl
                  << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        std::cerr << "Unknown exception!" << std::endl
                  << "Aborting!" << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        return 1;
    }

    return 0;
}
