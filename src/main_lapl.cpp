//
// Created by aozeritsky on 16.05.19.
//

#include "lapl.h"

#include <deal.II/numerics/vector_tools.h>

#include <math.h>

#include <iostream>
#include <vector>

using namespace std;

// - cos(x) sin(y) e^{-2t}
//   sin(x) cos(y) e^{-2t}
template<int dim>
class InitialValue : public Function<dim> {
    int example;
    double t;
    double mean;

public:

    explicit InitialValue(int example, double t = 0, double mean = 0)
        : example(example)
        , t(t)
        , mean(mean)
    { }

    virtual double value(const Point<dim>& p, const uint) const override
    {
        double x = p(0);
        double y = p(1);
        if (example == 0) {
            return -31.337*cos(x)*sin(y)*exp(t);
        } else if (example == 1) {
            return -cos(x)*cos(y)*exp(t) - mean;
        } else if (example == 2) {
            return -cos(2*x)*cos(2*y)*exp(t);
        } else {
            assert(0);
        }

        return -1.0;
    }
};

template<int dim>
class ExactSolution : public Function<dim> {
    int example;
    double t;

public:

    explicit ExactSolution(int example, double t = 0)
        : example(example)
        , t(t)
    { }

    virtual double value(const Point<dim>& p, const uint) const override
    {
        double x = p(0);
        double y = p(1);
        if (example == 0) {
            return  31.337*2*cos(x)*sin(y)*exp(t);
        } else if (example == 1) {
            return  2*cos(x)*cos(y)*exp(t);
        } else if (example == 2) {
            return  8*cos(2*x)*cos(2*y)*exp(t);
        } else {
            assert(0);
        }
        return -1;
    }
};

template<int dim>
void lapl_calc(Grid<dim>& grid, const int femDeg) {
    ModelBase<dim> base(grid, femDeg);
    base.initialize();

    Lapl<dim> lapl(base);
    lapl.initialize();

    auto& dof = base.dof();
    int n_dofs = dof.n_dofs();

    Vector<double> input(n_dofs);
    Vector<double> output(n_dofs);

    VectorTools::interpolate(
        dof,
        InitialValue<dim>(0),
        input);

/*
    VectorTools::create_right_hand_side(
        dof,
        base.q(),
        InitialValue<dim>(i),
        input
    );
*/
    lapl.lapl(output, input);

    //base.output(output, "output", "output.eps");
    //base.output(input, "input", "input.eps");

    Vector<double> difference;
    VectorTools::integrate_difference(
        dof,
        output,
        ExactSolution<dim>(0),
        difference,
        base.q(),
        VectorTools::L2_norm);
/*
    for (auto& v : difference) {
        cout << " > " << v << "\n";
    }
*/
    double norm = VectorTools::compute_global_error(
        grid.triangulation(),
        difference,
        VectorTools::H1_seminorm);

    printf("calc: %.16le\n", norm);
}

template<int dim>
void lapl_solve_bnd(Grid<dim>& grid, const int femDeg) {
    ModelBase<dim> base(grid, femDeg);
    base.initialize();

    Lapl<dim> lapl(base);
    lapl.initialize();

    auto& dof = base.dof();
    int n_dofs = dof.n_dofs();

    Vector<double> input(n_dofs);
    Vector<double> output(n_dofs);


    std::map<types::global_dof_index, double> bval;

    auto boundary_ids = grid.triangulation().get_boundary_ids();

    VectorTools::interpolate(
        dof,
        ExactSolution<dim>(0),
        input);

    bval.clear();

    for (auto id : boundary_ids) {
        VectorTools::interpolate_boundary_values(
            dof, id, InitialValue<dim>(0), bval);
    }

    lapl.solve(output, input, bval);

    //base.output(output, "output", "output.eps");
    //base.output(input, "input", "input.eps");

    Vector<double> difference;
    VectorTools::integrate_difference(
        dof,
        output,
        InitialValue<dim>(0),
        difference,
        base.q(),
        VectorTools::L2_norm);

    double norm = VectorTools::compute_global_error(
        grid.triangulation(),
        difference,
        VectorTools::H1_seminorm);

    printf("solve %.16le\n", norm);
}

template<int dim>
void lapl_solve_mean(Grid<dim>& grid, const int femDeg) {
    ModelBase<dim> base(grid, femDeg);
    base.initialize();

    Lapl<dim> lapl(base);
    lapl.initialize();

    auto& dof = base.dof();
    int n_dofs = dof.n_dofs();

    std::map<types::global_dof_index, double> bval;

    auto boundary_ids = grid.triangulation().get_boundary_ids();

    Vector<double> input(n_dofs);
    Vector<double> output(n_dofs);

    VectorTools::interpolate(
        dof,
        ExactSolution<dim>(1),
        input);

    lapl.solve(output, input);
    //lapl.solve(output, ExactSolution<dim>(1));

    double n1 = base.norm_1(output);
    printf("solution norm %.16le\n", n1);

    //base.output(output, "output", "output.eps");
    //base.output(input, "input", "input.eps");

    std::vector<bool> boundary_dofs (dof.n_dofs(), false);
    DoFTools::extract_boundary_dofs(
        dof,
        ComponentMask(),
        boundary_dofs);

    double mean = 0.0;
    double meansq = 0.0;
    double boundary_nodes_count = 0.0;
    for (int i = 0; i < (int)dof.n_dofs(); ++i) {
        if (boundary_dofs[i]) {
            mean += output[i];
            meansq += output[i]*output[i];
            boundary_nodes_count += 1;
        }
    }

    mean /= boundary_nodes_count;

    printf("> mean %.16le\n", mean);
    printf("> meansq %.16le\n", meansq);

    bval.clear();

    for (auto id : boundary_ids) {
        VectorTools::interpolate_boundary_values(
            dof, id, InitialValue<dim>(1), bval);
    }

    mean = 0.0;
    for (const auto& item : bval) {
        mean += item.second;
        meansq += item.second;
    }
    mean /= bval.size();

    printf(">> mean %.16le\n", mean);
    printf(">> meansq %.16le\n", meansq);

    Vector<double> difference;
    VectorTools::integrate_difference(
        dof,
        output,
        InitialValue<dim>(1, 0, mean),
        difference,
        base.q(),
        VectorTools::L2_norm);

    double norm = VectorTools::compute_global_error(
        grid.triangulation(),
        difference,
        VectorTools::H1_seminorm);

    base.output(input, "input", "input.gpl");

    VectorTools::interpolate(
        dof,
        InitialValue<dim>(1),
        input);

    double n2 = base.norm_1(input);
    printf("target norm %.16le\n", n2);

    base.output(output, "output", "output.gpl");
    base.output(input, "exact", "exact.gpl");

    input -= output;
    base.output(input, "diff", "diff.gpl");

    printf("norm diff %.16le\n", fabs(n1-n2));
    printf("solve %.16le\n", norm);
}

template<int dim>
void lapl_solve_bnd_constraints(Grid<dim>& grid, const int femDeg) {
    ModelBase<dim> base(grid, femDeg);
    base.initialize();

    Lapl<dim> lapl(base);
    lapl.initialize();

    auto& dof = base.dof();
    int n_dofs = dof.n_dofs();

    Vector<double> input(n_dofs);
    Vector<double> output(n_dofs);

    auto boundary_ids = grid.triangulation().get_boundary_ids();

    int exNo = 0;
    // int exNo = 1;

    VectorTools::interpolate(
        dof,
        ExactSolution<dim>(exNo),
        input);

    AffineConstraints<double> constraints;

    constraints.clear();

    for (auto id : boundary_ids) {
        VectorTools::interpolate_boundary_values(
            dof, id, InitialValue<dim>(exNo), constraints);
    }

    constraints.close();

    lapl.solve(output, input, constraints);

    Vector<double> difference;
    VectorTools::integrate_difference(
        dof,
        output,
        InitialValue<dim>(exNo),
        difference,
        base.q(),
        VectorTools::L2_norm);

    double norm = VectorTools::compute_global_error(
        grid.triangulation(),
        difference,
        VectorTools::H1_seminorm);

    printf("solve %.16le\n", norm);
}

template<int dim>
void lapl_solve_periodic(Grid<dim>& grid, const int femDeg) {
    std::cout << "init base\n";
    ModelBase<dim> base(grid, femDeg);
    base.initialize_periodic(2 /*left*/, 3 /*right*/, 1 /* y */);
    //base.initialize_periodic(0, 1, 0);
    //base.initialize();

    std::set< types::boundary_id > mean_value_boundary_ids;
    mean_value_boundary_ids.insert(0);
    mean_value_boundary_ids.insert(1);
    //mean_value_boundary_ids.insert(2);
    //mean_value_boundary_ids.insert(3);

    std::cout << "init lapl\n";
    Lapl<dim> lapl(base);
    lapl.initialize(mean_value_boundary_ids);
    //lapl.initialize();

    auto& dof = base.dof();
    int n_dofs = dof.n_dofs();

    Vector<double> input(n_dofs);
    Vector<double> output(n_dofs);


    auto boundary_ids = grid.triangulation().get_boundary_ids();

    int exNo = 2;

    std::cout << "interpolate rp\n";
    VectorTools::interpolate(
        dof,
        ExactSolution<dim>(exNo),
        input);

    AffineConstraints<double> constraints;
    constraints.clear();

    std::vector<
        GridTools::PeriodicFacePair<typename DoFHandler<dim>::cell_iterator>>
        periodicity_vector;

    GridTools::collect_periodic_faces(
        dof,
        2,
        3,
        1,
        periodicity_vector);
/*
    GridTools::collect_periodic_faces(
        dof,
        0,
        1,
        0,
        periodicity_vector);
*/
    DoFTools::make_periodicity_constraints<DoFHandler<dim>>(
        periodicity_vector,
        constraints);

    bool useMerge = false;
    if (useMerge) {
        constraints.clear();
    }

    std::cout << "init bnd constraints\n";
    for (auto id : boundary_ids) {
        // if (id == 2 || id == 3) {
        if (id == 0 || id == 1) {
            VectorTools::interpolate_boundary_values(
                dof, id, InitialValue<dim>(exNo), constraints);
        }

        //    VectorTools::interpolate_boundary_values(
        //        dof, id, InitialValue<dim>(exNo), constraints);
    }

    constraints.close();

    if (useMerge) {
        std::cout << "merge\n";
        // constraints.merge(base.constraints()); // periodic + bnd
        //constraints.merge(base.constraints(), AffineConstraints<double>::right_object_wins); // periodic + bnd
        constraints.merge(base.constraints(), AffineConstraints<double>::left_object_wins); // periodic + bnd
    }

    // constraints.print(std::cout);

    std::cout << "solve\n";
    // lapl.solve(output, input, bval);
    lapl.solve(output, input, constraints);

    //base.output(output, "output", "output.eps");
    //base.output(input, "input", "input.eps");

    Vector<double> difference;
    VectorTools::integrate_difference(
        dof,
        output,
        InitialValue<dim>(exNo),
        difference,
        base.q(),
        VectorTools::L2_norm);

    double norm = VectorTools::compute_global_error(
        grid.triangulation(),
        difference,
        VectorTools::H1_seminorm);

    printf("solve %.16le\n", norm);
}

template<int dim>
void lapl_solve_periodic_mean(Grid<dim>& grid, const int femDeg) {
    std::cout << "init base\n";
    ModelBase<dim> base(grid, femDeg);
    base.initialize_periodic(2 /*down*/, 3 /*up*/, 1 /* y */);
    //base.initialize_periodic(0, 1, 0);
    //base.initialize();

    std::set< types::boundary_id > mean_value_boundary_ids;
    mean_value_boundary_ids.insert(0);
    mean_value_boundary_ids.insert(1);
    //mean_value_boundary_ids.insert(2);
    //mean_value_boundary_ids.insert(3);

    std::cout << "init lapl\n";
    Lapl<dim> lapl(base);
    lapl.initialize(mean_value_boundary_ids);
    //lapl.initialize();

    auto& dof = base.dof();
    int n_dofs = dof.n_dofs();

    Vector<double> input(n_dofs);
    Vector<double> output(n_dofs);


    auto boundary_ids = grid.triangulation().get_boundary_ids();

    int exNo = 2;

    std::cout << "interpolate rp\n";
    VectorTools::interpolate(
        dof,
        ExactSolution<dim>(exNo),
        input);

    AffineConstraints<double> constraints;
    constraints.clear();

    std::vector<
        GridTools::PeriodicFacePair<typename DoFHandler<dim>::cell_iterator>>
        periodicity_vector;

    GridTools::collect_periodic_faces(
        dof,
        2,
        3,
        1,
        periodicity_vector);
/*
    GridTools::collect_periodic_faces(
        dof,
        0,
        1,
        0,
        periodicity_vector);
*/
    DoFTools::make_periodicity_constraints<DoFHandler<dim>>(
        periodicity_vector,
        constraints);


    constraints.close();

    std::cout << "periodicity constraints before merge: " << constraints.n_constraints() << "\n";
    constraints.print(std::cout);

    AffineConstraints<double> cc(lapl.constraints());
    std::cout << "mean constraints before merge: " << constraints.n_constraints() << "\n";
    cc.print(std::cout);

    cc.merge(constraints, AffineConstraints<double>::right_object_wins);
    std::cout << "constraints after merge: " << constraints.n_constraints() << "\n";
    cc.print(std::cout);

    //constraints.merge(base.constraints()); // periodic + bnd
    //constraints.merge(lapl.constraints(), AffineConstraints<double>::right_object_wins); // periodic + mean
    //constraints.merge(lapl.constraints(), AffineConstraints<double>::left_object_wins); // periodic + mean

    //std::cout << "constraints after merge: " << constraints.n_constraints() << "\n";
    //constraints.print(std::cout);

    // constraints.print(std::cout);

    std::cout << "solve\n";
    // lapl.solve(output, input, bval);
    lapl.solve(output, input, cc /*constraints*/);

    //base.output(output, "output", "output.eps");
    //base.output(input, "input", "input.eps");

    Vector<double> difference;
    VectorTools::integrate_difference(
        dof,
        output,
        InitialValue<dim>(exNo),
        difference,
        base.q(),
        VectorTools::L2_norm);

    double norm = VectorTools::compute_global_error(
        grid.triangulation(),
        difference,
        VectorTools::H1_seminorm);

    printf("solve %.16le\n", norm);
}

int main(int argc, char** argv)
{
    if (argc < 2) {
        cerr << "Usage: " << argv[0] << " filename\n";
        return -1;
    }
    string gridinput = argv[1];

    static const int femDeg = 1; // cycle in constraints for femDeg = 1
    static const int dim = 2;

    Grid<dim> grid;
    grid.load(gridinput);

    cout << "run lapl_calc\n";
    lapl_calc(grid, femDeg);
    cout << "\n";

    cout << "run lapl_solve_bnd\n";
    lapl_solve_bnd(grid, femDeg);
    cout << "\n";

    cout << "run lapl solve mean\n";
    lapl_solve_mean(grid, femDeg);
    cout << "\n";

    cout << "run lapl solve bnd constraints\n";
    lapl_solve_bnd_constraints(grid, femDeg);
    cout << "\n";

    cout << "run lapl solve bnd and periodic constraints\n";
    lapl_solve_periodic(grid, femDeg);
    cout << "\n";

    cout << "run lapl solve mean and periodic constraints\n";
    lapl_solve_periodic_mean(grid, femDeg);
    cout << "\n";

    return 0;
}
