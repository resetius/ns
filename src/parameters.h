#pragma once
/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include <deal.II/base/parameter_handler.h>

namespace Task {

enum MethodFormulation { METHOD_STANDARD, METHOD_ROTATIONAL };

class Parameters {
  public:
    Parameters();
    ~Parameters();

    void read_data(const char* fn);

    MethodFormulation form;

    double initial_time;
    double final_time;
    double Reynolds;
    double dt;

    double inner_radius;
    double outer_radius;
    int mesh_refines;
    int mesh_initial_cells;
    double z_length;
    unsigned int z_retentions;
    int pressure_degree;

    unsigned int vel_max_iterations, vel_Krylov_size, vel_off_diagonals,
        vel_update_prec;
    double vel_eps, vel_diag_strength;

    bool verbose;
    unsigned int output_interval;

    // spectral
    int spectral_vectors;
    std::string spectral_z0_path;

    // 3d
    bool use_sphere_in_3d;

  protected:
    dealii::ParameterHandler prm;
};

} // namespace Task
