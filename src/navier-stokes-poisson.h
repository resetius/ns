#pragma once
/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include "navier-stokes.h"

namespace Task {

using namespace dealii;
typedef unsigned int uint;

// http://www.math.tamu.edu/~guermond/PUBLICATIONS/guermond_quartapelle_Num_math_1998.pdf

template<int Dim> class NavierStokesPoisson : public NavierStokes<Dim> {
    typedef NavierStokes<Dim> base;

  public:
    NavierStokesPoisson(const Parameters& data);

    void initialize();

    void diffusion_step(const bool reinit_prec);
    void projection_step(const bool reinit_prec);
    void velocity_component_solve(const uint d);
    void run(const bool verbose, const unsigned int output_interval);
};
} // namespace Task

#include "navier-stokes-poisson-impl.h"
