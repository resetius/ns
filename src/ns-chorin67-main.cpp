//
// Created by aozeritsky on 17.06.19.
//

#include "ns-chorin67.h"

using namespace std;

template<int dim>
class ExactSolutionVel : public Function<dim> {
    int direction;
    double t;

public:

    explicit ExactSolutionVel(int direction, double t = 0)
        : direction(direction)
        , t(t)
    { }

    virtual double value(const Point<dim>& p, const uint) const override
    {
        double x = p(0);
        double y = p(1);
        if (direction == 0) {
            return -cos(x)*sin(y)*exp(-2.0*t);
        } else if (direction == 1) {
            return sin(x)*cos(y)*exp(-2.0*t);
        } else {
            Assert(false, "unknown direction");
        }
    }
};

template<int dim>
class ExactSolutionPres : public Function<dim> {
    double t;

public:

    explicit ExactSolutionPres(double t = 0)
        : t(t)
    { }

    virtual double value(const Point<dim>& p, const uint) const override
    {
        double x = p(0);
        // double y = p(1);
        return -1.0/4.0*(cos(2.0*x)+cos(2*x))*exp(-4.0*t);
    }
};

template<int dim>
class F1 : public Function<dim> {
    const std::function<double(const Point<dim>&p, const uint component)> f;

public:
    F1(const std::function<double(const Point<dim>&p, const uint component)>& f)
        : f(f)
    { }

    virtual double value(const Point<dim>& p, const uint component) const override {
        return f(p, component);
    }
};

template<int dim>
F1<dim> func1(const std::function<double(const Point<dim>&p, const uint component)>& f) {
    return F1<dim>(f);
}

template<int dim>
class F1t : public Function<dim> {
    const std::function<double(const Point<dim>&p, const uint component, double t)> f;

public:
    F1t(const std::function<double(const Point<dim>&p, const uint component, double t)>& f)
        : f(f)
    { }

    virtual double value(const Point<dim>& p, const uint component) const override {
        return f(p, component, this->get_time());
    }
};

template<int dim>
F1t<dim> func1t(const std::function<double(const Point<dim>&p, const uint component, double t)>& f) {
    return F1t<dim>(f);
}

int main(int argc, char ** argv)
{
    if (argc < 2) {
        cerr << "Usage: " << argv[0] << " filename\n";
        return -1;
    }
    string gridinput = argv[1];

    static const int velFemDeg = 3; // TODO: 2,1 ?
    static const int presFemDeg = 3; // TODO: Use different spaces for `vel` and `pres`

//    static const int velFemDeg = 1; // TODO: 2,1 ?
//    static const int presFemDeg = 1; // TODO: Use different spaces for `vel` and `pres`

    static const int dim = 2;

    static const double Re = 1; //32;
    static const double dt = 0.001;

    Grid<dim> grid;
    grid.load(gridinput);

    ModelBase<dim> velSpace(grid, velFemDeg);
    ModelBase<dim> presSpace(grid, presFemDeg);
    velSpace.initialize();
    presSpace.initialize();

    NsChorin67<dim> nsChorin67(velSpace, presSpace, Re, dt);
    nsChorin67.initialize();

    int n_velDofs = velSpace.dof().n_dofs();
    int n_presDofs = presSpace.dof().n_dofs();

    const auto fUx = [](const Point<dim>&p ,const uint, double t = 0) {
        auto x = p(0); auto y = p(1);
        return -cos(x)*sin(y)*exp(-2.0*t);
    };

    const auto fUy = [](const Point<dim>&p ,const uint, double t = 0) {
        auto x = p(0); auto y = p(1);
        return sin(x)*cos(y)*exp(-2.0*t);
    };

    const auto fU = [&](const Point<dim>&p ,const uint i, double t = 0) {
        if (i == 0) {
            return fUx(p, 0, t);
        } else if (i == 1) {
            return fUy(p, 0, t);
        }
        assert(false);
        return -1.0;
    };

    // 1. test solvePartU
    {
        auto fU1 = [](const Point<dim>& p,const uint) {
            auto x = p(0); auto y = p(1);
            return sin(x)*cos(y);
        };

        auto fRpX = [&](const Point<dim>&p ,const uint i) {
            auto x = p(0); auto y = p(1);
            return -cos(x)*sin(y) + dt * Re * fU1(p, i) * sin(x) * sin(y) - 2*dt*cos(x)*sin(y);
        };

        auto fRpY = [&](const Point<dim>&p ,const uint i) {
            auto x = p(0); auto y = p(1);
            return sin(x)*cos(y) + dt * Re * fU1(p, i) * cos(x) * cos(y) + 2*dt*sin(x)*cos(y);
        };

        Vector<double> u1(n_velDofs); // constant = 1
        VectorTools::interpolate(velSpace.dof(), func1<dim>(fU1), u1);

        Vector<double> input(n_presDofs*dim);
        Vector<double> output(n_presDofs*dim);
        Vector<double> tmp1(n_velDofs); int i = 0;
        VectorTools::interpolate(velSpace.dof(), func1<dim>(fRpX), tmp1); // u_x
        for (const auto v : tmp1) { input[i++] = v; }
        VectorTools::interpolate(velSpace.dof(), func1<dim>(fRpY), tmp1); // u_y
        for (const auto v : tmp1) { input[i++] = v; }
        nsChorin67.solvePartU(output, input, u1, 0 /* 0 : dx, 1 : dy */, func1<dim>(fU) /* boundary*/ );

        i = 0;
        Vector<double> u_x(n_velDofs); for (auto& v : u_x) { v = output[i++]; }
        Vector<double> u_y(n_velDofs); for (auto& v : u_y) { v = output[i++]; }

        Vector<double> difference; double norm;
        VectorTools::integrate_difference(
            velSpace.dof(),
            u_x,
            func1<dim>(fUx), // real solution, u_x
            difference,
            velSpace.q(),
            VectorTools::L2_norm);

        norm = VectorTools::compute_global_error(
            grid.triangulation(),
            difference,
            VectorTools::H1_seminorm);

        printf("norm u_x %.16le\n", norm);

        VectorTools::integrate_difference(
            velSpace.dof(),
            u_y,
            func1<dim>(fUy), // real solution, u_y
            difference,
            velSpace.q(),
            VectorTools::L2_norm);

        norm = VectorTools::compute_global_error(
            grid.triangulation(),
            difference,
            VectorTools::H1_seminorm);

        printf("norm u_y %.16le\n", norm);
    }

    // 2. test solveUaux
    {
        auto fRpX = [&](const Point<dim>&p ,const uint) {
            auto x = p(0); auto y = p(1);
            return Re * (-cos(x)*sin(y)*sin(x)*sin(y)-sin(x)*cos(y)*cos(x)*cos(y))-2.0*cos(x)*sin(y);
        };

        auto fRpY = [&](const Point<dim>&p ,const uint) {
            auto x = p(0); auto y = p(1);
            return Re * (-cos(x)*sin(y)*cos(x)*cos(y)-sin(x)*cos(y)*sin(x)*sin(y))+2.0*sin(x)*cos(y);
        };

        Vector<double> E(n_presDofs*dim);
        Vector<double> tmp1(n_velDofs); int i = 0;
        VectorTools::interpolate(velSpace.dof(), func1<dim>(fRpX), tmp1); // E_x
        for (const auto v : tmp1) { E[i++] = v; }
        VectorTools::interpolate(velSpace.dof(), func1<dim>(fRpY), tmp1); // E_y
        for (const auto v : tmp1) { E[i++] = v; }

        Vector<double> input(n_presDofs*dim);
        Vector<double> output(n_presDofs*dim);
        i = 0;
        VectorTools::interpolate(velSpace.dof(), func1<dim>(fUx), tmp1); // u_x
        for (const auto v : tmp1) { input[i++] = v; }
        VectorTools::interpolate(velSpace.dof(), func1<dim>(fUy), tmp1); // u_y
        for (const auto v : tmp1) { input[i++] = v; }

        const auto fRp = [&](const Point<dim>&p ,const uint i) {
            if (i == 0) {
                return fUx(p, 0) - dt*fRpX(p, 0);
            } else if (i == 1) {
                return fUy(p, 0) - dt*fRpY(p, 0);
            }
            assert(false);
            return -1.0;
        };

        nsChorin67.solveUaux(output, input, E, func1<dim>(fRp /*fU*/) /* boundary*/ );

        i = 0;
        Vector<double> u_x(n_velDofs); for (auto& v : u_x) { v = output[i++]; }
        Vector<double> u_y(n_velDofs); for (auto& v : u_y) { v = output[i++]; }

        Vector<double> difference; double norm;
        VectorTools::integrate_difference(
            velSpace.dof(),
            u_x,
            func1<dim>(fUx), // real solution, u_x
            difference,
            velSpace.q(),
            VectorTools::L2_norm);

        norm = VectorTools::compute_global_error(
            grid.triangulation(),
            difference,
            VectorTools::H1_seminorm);

        printf("norm u_x %.16le\n", norm);

        VectorTools::integrate_difference(
            velSpace.dof(),
            u_y,
            func1<dim>(fUy), // real solution, u_y
            difference,
            velSpace.q(),
            VectorTools::L2_norm);

        norm = VectorTools::compute_global_error(
            grid.triangulation(),
            difference,
            VectorTools::H1_seminorm);

        printf("norm u_y %.16le\n", norm);
    }

    if (1) {
        double t = 0;
        double T = 1.0;

        Vector<double> input(n_velDofs*dim+n_presDofs); // u_x, u_y, u_z, p
        Vector<double> output(input.size());

        Vector<double> E(n_velDofs*dim);

        Vector<double> tmp1(n_velDofs); int i = 0;
        i = 0;
        VectorTools::interpolate(velSpace.dof(), func1<dim>(fUx), tmp1); // u_x
        for (const auto v : tmp1) { input[i++] = v; }
        VectorTools::interpolate(velSpace.dof(), func1<dim>(fUy), tmp1); // u_y
        for (const auto v : tmp1) { input[i++] = v; }

        auto presF = [&](const Point<dim>&p, unsigned int, double t = 0.0) {
            auto x = p(0); auto y = p(1);
            return -Re/4.0 * (cos(2.0*x)+cos(2.0*y))*exp(-4.0*t);
        };
        VectorTools::interpolate(velSpace.dof(), func1<dim>(presF), tmp1); // p
        for (const auto v : tmp1) { input[i++] = v; }

        fprintf(stderr, "%le t=%lf/%lf\n", input.l2_norm(), t, T);

        auto ff = func1t<dim>(fU);
        auto pp = func1t<dim>(presF);
        while (t < T) {
            nsChorin67.calc(output, input, E, ff);
            input.swap(output);

            ff.advance_time(dt);
            pp.advance_time(dt);

            Vector<double> tmp(n_presDofs); int i = 0;
            for (auto& v : tmp) { v = input[i++]; }
            Vector<double> tmp1(n_presDofs);

            VectorTools::interpolate(velSpace.dof(), funcProj(ff, 0), tmp1);
            tmp -= tmp1;
            auto normInf1 = tmp.linfty_norm();

            for (auto& v : tmp) { v = input[i++]; }
            VectorTools::interpolate(velSpace.dof(), funcProj(ff, 1), tmp1);
            tmp -= tmp1;
            auto normInf2 = tmp.linfty_norm();

            for (auto& v : tmp) { v = input[i++]; }
            VectorTools::interpolate(presSpace.dof(), pp, tmp1);
            tmp -= tmp1;
            auto normInf3 = tmp.linfty_norm();

            t += dt;

            fprintf(stderr, "%le, %le/%le/%le ", t+dt, normInf1, normInf2, normInf3);
            fprintf(stderr, ", %le t=%lf/%lf\n", input.l2_norm(), t, T);
        }
    }

    return 0;
}
