#include "navier-stokes-poisson.h"

int main(int argc, char** argv) {
    try {
        using namespace dealii;
        using namespace Task;
#ifdef DEAL_II_WITH_MPI
        MPI_Init(&argc, &argv);
#endif
        Parameters data;
        data.read_data("parameter-file.prm");

        deallog.depth_console(data.verbose ? 2 : 0);

        NavierStokesPoisson<3> test(data);
        test.run(data.verbose, data.output_interval);
    } catch (std::exception& exc) {
        std::cerr << std::endl
                  << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        std::cerr << "Exception on processing: " << std::endl
                  << exc.what() << std::endl
                  << "Aborting!" << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        return 1;
    } catch (...) {
        std::cerr << std::endl
                  << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        std::cerr << "Unknown exception!" << std::endl
                  << "Aborting!" << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        return 1;
    }

    return 0;
}
