#pragma once

#include "model_base.h"

#include <deal.II/lac/sparse_ilu.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>

using namespace dealii;

template <int dim>
class Div
{
    const ModelBase<dim>& base;

public:
    Div(const ModelBase<dim>& base)
        : base(base)
    { }

    void initialize()
    {
    }

    void div(Vector<double>& output, const Vector<double>& input, int direction)
    {
        const unsigned int dpc = base.fe_q().dofs_per_cell,
            nqp = base.q().size();
        std::vector<types::global_dof_index> ldi(dpc);

        Vector<double> loc_div(dpc);

        output = 0.0;

        std::vector<Tensor<1, dim>> grad(nqp);

        auto cell = base.dof().begin_active();
        auto end = base.dof().end();

        FEValues<dim> fe_val(
            base.fe_q(),
            base.q(),
            update_gradients | update_JxW_values | update_values);

        for (; cell != end; ++cell) {
            fe_val.reinit(cell);
            cell->get_dof_indices(ldi);
            fe_val.get_function_gradients(input, grad);
            loc_div = 0.;
            for (unsigned int q = 0; q < nqp; ++q) {
                for (unsigned int i = 0; i < dpc; ++i) {
                    auto& shape_val = fe_val.shape_value(i, q);
                    loc_div(i) += shape_val * grad[q][direction] * fe_val.JxW(q);
                }
            }

            for (unsigned int i = 0; i < dpc; ++i) {
                output(ldi[i]) += loc_div(i);
            }
        }

        base.mass_solve(output);
    }

    void div(Vector<double>& output, const Vector<double>& input)
    {
        const unsigned int dpc = base.fe_q().dofs_per_cell,
            nqp = base.q().size();
        std::vector<types::global_dof_index> ldi(dpc);

        Vector<double> loc_div(dpc);

        output = 0.0;

        std::vector<Tensor<1, dim>> grad(nqp);

        auto cell = base.dof().begin_active();
        auto end = base.dof().end();

        int n_dofs = base.dof().n_dofs();

        FEValues<dim> fe_val(
            base.fe_q(),
            base.q(),
            update_gradients | update_JxW_values | update_values);

        for (; cell != end; ++cell) {
            fe_val.reinit(cell);
            cell->get_dof_indices(ldi);

            loc_div = 0.;
            for (int direction = 0; direction < dim; direction ++) {
                Vector<double> v(input.begin()+direction*n_dofs,input.begin()+(direction+1)*n_dofs);
                fe_val.get_function_gradients(v, grad);
                for (unsigned int q = 0; q < nqp; ++q) {
                    for (unsigned int i = 0; i < dpc; ++i) {
                        auto& shape_val = fe_val.shape_value(i, q);
                        loc_div(i) += shape_val * grad[q][direction] * fe_val.JxW(q);
                    }
                }
            }

            for (unsigned int i = 0; i < dpc; ++i) {
                output(ldi[i]) += loc_div(i);
            }
        }

        base.mass_solve(output);
    }
};
