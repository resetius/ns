#pragma once
/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include "navier-stokes.h"

namespace Task {

using namespace dealii;
typedef unsigned int uint;

template<int Dim> class NavierStokesEx35 : public NavierStokes<Dim> {
    typedef NavierStokes<Dim> base;

  public:
    MethodFormulation type;

    //  SparseMatrix<double> vel_pres_Mass;

    // visualization stream

    NavierStokesEx35(const Parameters& data);

    void initialize();

    void interpolate_velocity();

    void diffusion_step(const bool reinit_prec);

    void projection_step(const bool reinit_prec);

    void update_pressure(const bool reinit_prec);

    void run(const bool verbose, const unsigned int output_interval);

    // The final few functions implement the diffusion solve as well as
    // postprocessing the output, including computing the curl of the
    // velocity:
    void diffusion_component_solve(const unsigned int d);
};

} // namespace Task

#include "navier-stokes-ex35-impl.h"
