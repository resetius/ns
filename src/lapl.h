#pragma once

#include "model_base.h"

#include <deal.II/lac/sparse_ilu.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/lac/solver_gmres.h>

using namespace dealii;

template <int dim>
class Lapl
{
    const ModelBase<dim>& base;

    SparsityPattern      sparsity_pattern_neuman; // use with mean_value_constraints

    SparseMatrix<double> Laplace;

    SparseMatrix<double> LaplaceNeuman; // use with mean_value_constraints

    SparseMatrix<double> MatrixNeumanWithAdd;
    SparseMatrix<double> MassWithMean;

    SparseILU<double> prec_Laplace;

    AffineConstraints<double>     mean_value_constraints;

public:
    Lapl(const ModelBase<dim>& base)
        : base(base)
    { }

    const AffineConstraints<double> & constraints() {
        return mean_value_constraints;
    }

    void initialize(
        const std::set< types::boundary_id > & mean_value_boundary_ids = std::set<types::boundary_id>())
    {
        const auto& dof = base.dof();
        Laplace.reinit(base.s_pattern());
        MatrixCreator::create_laplace_matrix(
            dof, base.q(), Laplace);

        prec_Laplace.initialize(
            Laplace,
            SparseILU<double>::AdditionalData(0.1, 70));


        std::vector<bool> boundary_dofs (dof.n_dofs(), false);
        DoFTools::extract_boundary_dofs (
            dof,
            ComponentMask(),
            boundary_dofs,
            mean_value_boundary_ids);

        const unsigned int first_boundary_dof
            = std::distance (
                boundary_dofs.begin(),
                std::find (
                    boundary_dofs.begin(),
                    boundary_dofs.end(),
                    true));

        mean_value_constraints.clear();
        base.constraints().print(std::cout);
        std::set<unsigned int> constrained;
        for (const auto& line : base.constraints().get_lines()) {
            for (const auto& entry : line.entries) {
                constrained.insert(entry.first);
            }
        }
        // mean_value_constraints.add_line (first_boundary_dof);
        unsigned int lineId = (unsigned)-1;
        for (unsigned int i=first_boundary_dof; i<dof.n_dofs(); ++i) {
            if (boundary_dofs[i] == true) {
                if (lineId == (unsigned)-1 && constrained.find(i) == constrained.end()) {
                    std::cout << "add line " << i << "\n";
                    mean_value_constraints.add_line(i);
                    lineId = i;
                    continue;
                }
                if (lineId != (unsigned)-1) {
                    std::cout << "constraint entry " << lineId << " " << i << "\n";
                    mean_value_constraints.add_entry (
                        lineId,
                        i, -1);
                }
            }
        }
        mean_value_constraints.close();

        mean_value_constraints.merge(base.constraints(), AffineConstraints<double>::right_object_wins);

        {
            DynamicSparsityPattern dsp (
                dof.n_dofs(), dof.n_dofs());
            DoFTools::make_sparsity_pattern (dof, dsp);
            mean_value_constraints.condense (dsp);
            sparsity_pattern_neuman.copy_from (dsp);
        }

        LaplaceNeuman.reinit(sparsity_pattern_neuman);

        MatrixNeumanWithAdd.reinit(sparsity_pattern_neuman);
        MassWithMean.reinit(sparsity_pattern_neuman);

        MatrixCreator::create_mass_matrix(
            dof, base.q(), MassWithMean);

        //const Function<dim>* f = nullptr;
        //MatrixCreator::create_laplace_matrix(
        //    dof, base.q(), LaplaceNeuman, f, mean_value_constraints);
        MatrixCreator::create_laplace_matrix(
            dof, base.q(), LaplaceNeuman);
    }

    const SparseMatrix<double>& matrix() const {
        return Laplace;
    }

    void lapl(Vector<double>& output, const Vector<double>& input)
    {
        const unsigned int dpc = base.fe_q().dofs_per_cell,
            nqp = base.q().size();
        std::vector<types::global_dof_index> ldi(dpc);

        Vector<double> loc_lapl(dpc);
        // Vector<double> lapl(input.size());
        std::vector<double> lapl(nqp);

        output = 0.0;

        std::vector<Tensor<1, dim>> grad(nqp);

        auto cell = base.dof().begin_active();
        auto end = base.dof().end();

        FEValues<dim> fe_val(
            base.fe_q(),
            base.q(),
            update_hessians | update_gradients | update_JxW_values | update_values);

        for (; cell != end; ++cell) {
            fe_val.reinit(cell);
            cell->get_dof_indices(ldi);
            fe_val.get_function_laplacians(input, lapl);
            loc_lapl = 0.;
            for (unsigned int q = 0; q < nqp; ++q) {
                for (unsigned int i = 0; i < dpc; ++i) {
                    loc_lapl(i) += fe_val.shape_value(i, q) * lapl[q] * fe_val.JxW(q);
                }
            }

            for (unsigned int i = 0; i < dpc; ++i) {
                output(ldi[i]) += loc_lapl(i);
            }
        }

        base.mass_solve(output);
    }

    void solve(
        Vector<double>& output,
        const Vector<double>& in,
        const AffineConstraints<double>& constraints,
        const std::map<types::global_dof_index, double>& bval = {})
    {
        Vector<double> input(in.size());

        SparseMatrix<double> matrix;
        SparsityPattern      sparsity_pattern;

        {
            DynamicSparsityPattern dsp (
                base.dof().n_dofs(), base.dof().n_dofs());
            DoFTools::make_sparsity_pattern (base.dof(), dsp);
            constraints.condense (dsp);
            sparsity_pattern.copy_from (dsp);
        }

        matrix.reinit(sparsity_pattern);
        MatrixCreator::create_laplace_matrix(
            base.dof(), base.q(), matrix);
        //matrix.copy_from(Laplace);

        base.mass().vmult(input, in);

        int base_max_its = 1000;
        double base_eps = 1e-8;

        input *= -1.0;

        double solve_eps = base_eps * input.l2_norm();
        if (solve_eps < 1e-15) {
            solve_eps = base_eps;
        }

        output = 0.0;

        constraints.condense(matrix, input);

        if (!bval.empty()) {
            MatrixTools::apply_boundary_values(
                bval, matrix, output, input);
        }

        SolverControl solvercontrol(base_max_its, solve_eps);
        //SolverCG<> cg(solvercontrol);
        SolverGMRES<> gmres(solvercontrol);

        SparseILU<double> prec;
        prec.initialize(
            matrix,
            SparseILU<double>::AdditionalData(0.1, 70));

        gmres.solve(
            matrix,
            output,
            input,
            prec);

        constraints.distribute(output);
    }

    void solve(Vector<double>& output, const Vector<double>& in, const std::map<types::global_dof_index, double>& bval)
    {
        Vector<double> input(in.size());

        base.mass().vmult(input, in);

        int base_max_its = 1000;
        double base_eps = 1e-8;

        input *= -1.0;

        double solve_eps = base_eps * input.l2_norm();
        if (solve_eps < 1e-15) {
            solve_eps = base_eps;
        }

        output = 0.0;

        SparseMatrix<double> matrix;
        matrix.reinit(base.s_pattern());
        matrix.copy_from(Laplace);

        MatrixTools::apply_boundary_values(
            bval, matrix, output, input);

        SolverControl solvercontrol(base_max_its, solve_eps);
        SolverCG<> cg(solvercontrol);

        cg.solve(
            matrix,
            output,
            input,
            prec_Laplace);
    }

    void solve(Vector<double>& output, const Vector<double>& in, double massFactor = 0.0, double laplFactor = 1.0)
    {
        Vector<double> input;
        Vector<double> ii = in;
        input.reinit(base.dof().n_dofs());

        base.mass().vmult(input, in);
        input *= -1.0;

/*
        Vector<double> tmp (input.size());
        VectorTools::create_boundary_right_hand_side (
            base.dof(),
            QGauss<dim-1>(3),
            Functions::ConstantFunction<dim>(0),
            tmp);

        std::cout << "?>" << tmp.l2_norm() << "\n";

        input += tmp;
*/
//        printf("cells %d\n", base.triangulation().n_active_cells());
//        printf("rhs_1 %.16le\n", base.norm_1(input));

        mean_value_constraints.condense (input);
        // mean_value_constraints.condense (LaplaceNeuman); // TODO: with should be done only 1 time!
        MatrixNeumanWithAdd.copy_from(LaplaceNeuman);
        MatrixNeumanWithAdd *= laplFactor;
        MatrixNeumanWithAdd.add(massFactor, MassWithMean);
        mean_value_constraints.condense (MatrixNeumanWithAdd);

        // mean_value_constraints.condense (LaplaceNeuman);

//        printf("rhs_condense_1 %.16le\n", base.norm_1(input));

        int base_max_its = 10000;
        double base_eps = 1e-8;
        double solve_eps = base_eps * input.l2_norm();
        if (solve_eps < 1e-15) {
            solve_eps = base_eps;
        }

        PreconditionSSOR<> preconditioner;
        // preconditioner.initialize(LaplaceNeuman, 1.2);
        preconditioner.initialize(MatrixNeumanWithAdd, 1.2);

        SolverControl solvercontrol(base_max_its, solve_eps);
        SolverCG<> cg(solvercontrol);
        SolverGMRES<> gmres(solvercontrol);


        gmres.solve(
            // LaplaceNeuman,
            MatrixNeumanWithAdd,
            output,
            input,
            preconditioner);

        /*
        gmres.solve(
            LaplaceNeuman,
            output,
            input,
            preconditioner);
        */

        mean_value_constraints.distribute(output);
    }

    void solve(Vector<double>& output, const Function<dim>& rhs)
    {
        Vector<double> input;
        input.reinit(base.dof().n_dofs());

        VectorTools::create_right_hand_side (
            base.dof(),
            base.q(),
            rhs,
            input);

/*
        Vector<double> tmp (input.size());
        VectorTools::create_boundary_right_hand_side (
            base.dof(),
            QGauss<dim-1>(3),
            Functions::ConstantFunction<dim>(0),
            tmp);

        std::cout << "?>" << tmp.l2_norm() << "\n";

        input += tmp;
*/
        printf("cells %d\n", base.triangulation().n_active_cells());
        printf("rhs_1 %.16le\n", base.norm_1(input));

        mean_value_constraints.condense (input);
        mean_value_constraints.condense (LaplaceNeuman);

        printf("rhs_condense_1 %.16le\n", base.norm_1(input));

        int base_max_its = 10000;
        double base_eps = 1e-8;
        double solve_eps = base_eps * input.l2_norm();
        if (solve_eps < 1e-15) {
            solve_eps = base_eps;
        }

        PreconditionSSOR<> preconditioner;
        preconditioner.initialize(LaplaceNeuman, 1.2);

        SolverControl solvercontrol(base_max_its, solve_eps);
        SolverCG<> cg(solvercontrol);
        SolverGMRES<> gmres(solvercontrol);

        output = 0;
        cg.solve(
            LaplaceNeuman,
            output,
            input,
            preconditioner);

        /*
        gmres.solve(
            LaplaceNeuman,
            output,
            input,
            preconditioner);
        */

        mean_value_constraints.distribute(output);
    }
};
