/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include <cmath>
#include <fstream>
#include <iostream>

#include "initial-values.h"
#include "navier-stokes-ex35.h"
#include "parameters.h"

namespace Task {

using namespace dealii;
typedef unsigned int uint;

template<int dim>
NavierStokesEx35<dim>::NavierStokesEx35(const Parameters& data)
    : NavierStokes<dim>(data), type(data.form) {
    initialize();
}

template<int dim> void NavierStokesEx35<dim>::initialize() {
    auto& vel_Laplace = base::vel_Laplace;
    auto& vel_Laplace_plus_Mass = base::vel_Laplace_plus_Mass;
    auto& vel_Mass = base::vel_Mass;
    auto& phi_n = base::phi_n;
    auto& phi_n_minus_1 = base::phi_n_minus_1;
    auto& Re = base::Re;
    auto& dt = base::dt;
    auto& sparsity_pattern_velocity = base::sparsity_pattern_velocity;
    auto& dof_handler_pressure = base::dof_handler_pressure;
    auto& dof_handler_velocity = base::dof_handler_velocity;
    auto& pres_n = base::pres_n;
    auto& pres_n_minus_1 = base::pres_n_minus_1;
    auto& u_n_minus_1 = base::u_n_minus_1;
    auto& u_n = base::u_n;

    vel_Laplace_plus_Mass.reinit(sparsity_pattern_velocity);

    vel_Laplace_plus_Mass = 0.;
    vel_Laplace_plus_Mass.add(1. / Re, vel_Laplace);
    vel_Laplace_plus_Mass.add(1.5 / dt, vel_Mass);

    VectorTools::interpolate(
        dof_handler_pressure, ZeroFunction<dim>(), pres_n_minus_1);
    VectorTools::interpolate(dof_handler_pressure, ZeroFunction<dim>(), pres_n);

    phi_n = 0.;
    phi_n_minus_1 = 0.;

    for (uint d = 0; d < dim; ++d) {
        VectorTools::interpolate(
            dof_handler_velocity, ZeroFunction<dim>(), u_n_minus_1[d]);
        VectorTools::interpolate(
            dof_handler_velocity, ZeroFunction<dim>(), u_n[d]);
    }
}

template<int dim>
void NavierStokesEx35<dim>::run(
    const bool verbose, const unsigned int output_interval) {
    auto& dof_handler_pressure = base::dof_handler_pressure;
    auto& dof_handler_velocity = base::dof_handler_velocity;

    auto& T = base::T;
    auto& t_0 = base::t_0;
    auto& dt = base::dt;
    auto& vel_update_prec = base::vel_update_prec;
    ConditionalOStream verbose_cout(std::cout, verbose);

    const unsigned int n_steps = static_cast<unsigned int>((T - t_0) / dt);
    std::cout << "u size : " << dof_handler_velocity.n_dofs() << "\n";
    std::cout << "p size : " << dof_handler_pressure.n_dofs() << "\n";

    base::output_results(1);
    for (unsigned int n = 2; n <= n_steps; ++n) {
        if (n % output_interval == 0) {
            verbose_cout << "Plotting Solution" << std::endl;
            base::output_results(n);
        }
        std::cout << "Step = " << n << " Time = " << (n * dt) << std::endl;
        verbose_cout << "  Interpolating the velocity " << std::endl;

        interpolate_velocity();
        verbose_cout << "  Diffusion Step" << std::endl;
        if (n % vel_update_prec == 0)
            verbose_cout << "    With reinitialization of the preconditioner"
                         << std::endl;
        diffusion_step((n % vel_update_prec == 0) || (n == 2));
        verbose_cout << "  Projection Step" << std::endl;
        projection_step((n == 2));
        verbose_cout << "  Updating the Pressure" << std::endl;
        update_pressure((n == 2));
    }
    base::output_results(n_steps);
}

template<int dim> void NavierStokesEx35<dim>::interpolate_velocity() {
    for (unsigned int d = 0; d < dim; ++d) {
        base::u_star[d].equ(2., base::u_n[d]);
        base::u_star[d] -= base::u_n_minus_1[d];
    }
}

template<int dim>
void NavierStokesEx35<dim>::diffusion_step(const bool reinit_prec) {
    //  Vector<double> pres_tmp2;
    //  pres_tmp2.reinit (dof_handler_pressure.n_dofs());
    //  pres_tmp2.equ(-0.000001, pres_n);

    base::pres_tmp.equ(-1., base::pres_n);
    base::pres_tmp.add(-4. / 3., base::phi_n, 1. / 3., base::phi_n_minus_1);

    base::assemble_advection_term();

    for (unsigned int d = 0; d < dim; ++d) {
        base::force[d] = 0;
        //    force[d] = F[d];
        //    force[d] *= -1;

        base::v_tmp.equ(2. / base::dt, base::u_n[d]);
        base::v_tmp.add(-.5 / base::dt, base::u_n_minus_1[d]);

        base::v_tmp.add(1.0, base::F[d]);

        base::vel_Mass.vmult_add(base::force[d], base::v_tmp);
        //    vel_Mass.vmult_add (force[d], F[d]);
        //    vel_pres_Mass.vmult_add (force[d], pres_tmp2);
        base::pres_Diff[d].vmult_add(base::force[d], base::pres_tmp);

        base::u_n_minus_1[d] = base::u_n[d];

        base::vel_it_matrix[d].copy_from(base::vel_Laplace_plus_Mass);
        base::vel_it_matrix[d].add(1., base::vel_Advection);

        base::boundary_values.clear();
        for (std::vector<types::boundary_id>::const_iterator boundaries =
                 base::boundary_ids.begin();
             boundaries != base::boundary_ids.end();
             ++boundaries) {

            // fprintf(stderr, "boundary -> %d\n", *boundaries);

            if (base::data.use_sphere_in_3d) {
                // rotating sphere
                switch (*boundaries) {
                    case 0:
                        // inner
                        VectorTools::interpolate_boundary_values(
                            base::dof_handler_velocity,
                            *boundaries,
                            ZeroFunction<dim>(),
                            base::boundary_values);
                        break;
                    case 1:
                        // outer
                        // rotating sphere
                        VectorTools::interpolate_boundary_values(
                            base::dof_handler_velocity,
                            *boundaries,
                            Force<dim>(d, base::data.use_sphere_in_3d),
                            base::boundary_values);
                        break;
                    default:
                        //        Assert (false, ExcNotImplemented());
                        break;
                }
            } else {
                switch (*boundaries) {
                    case 4:
                        VectorTools::interpolate_boundary_values(
                            base::dof_handler_velocity,
                            *boundaries,
                            RotationalCylinder<dim>(d),
                            base::boundary_values);
                        break;
                    default:
                        // outer: 5
                        // top: 2
                        // bottom: 3
                        VectorTools::interpolate_boundary_values(
                            base::dof_handler_velocity,
                            *boundaries,
                            ZeroFunction<dim>(),
                            base::boundary_values);
                        break;
                }
            }
        }
        MatrixTools::apply_boundary_values(
            base::boundary_values,
            base::vel_it_matrix[d],
            base::u_n[d],
            base::force[d]);
    }

    //  exit(0);

    Threads::TaskGroup<void> tasks;
    for (unsigned int d = 0; d < dim; ++d) {
        if (reinit_prec)
            base::prec_velocity[d].initialize(
                base::vel_it_matrix[d],
                SparseILU<double>::AdditionalData(
                    base::vel_diag_strength, base::vel_off_diagonals));
        tasks += Threads::new_task(
            &NavierStokes<dim>::diffusion_component_solve, *this, d);
    }
    tasks.join_all();
}

template<int dim>
void NavierStokesEx35<dim>::projection_step(const bool reinit_prec) {
    base::pres_iterative.copy_from(base::pres_Laplace);

    base::pres_tmp = 0.;

    //  Vector<double> pres_tmp2;
    //  pres_tmp2.reinit (dof_handler_pressure.n_dofs());
    //  pres_tmp2.equ(-0.000001, pres_n);
    //  pres_Mass.vmult_add(pres_tmp, pres_tmp2);

    //  pres_iterative.add(0.000001, pres_Mass);

    for (unsigned d = 0; d < dim; ++d)
        base::pres_Diff[d].Tvmult_add(base::pres_tmp, base::u_n[d]);

    base::phi_n_minus_1 = base::phi_n;

    static std::map<types::global_dof_index, double> bval;
    if (reinit_prec) {
        bval.clear();
        if (dim == 2 || base::data.use_sphere_in_3d) {
            bval[0] = 0;
        } else if (1) {
            VectorTools::interpolate_boundary_values(
                base::dof_handler_pressure, 2, ZeroFunction<dim>(), bval);
            VectorTools::interpolate_boundary_values(
                base::dof_handler_pressure, 3, ZeroFunction<dim>(), bval);

            // inner circle
            //VectorTools::interpolate_boundary_values(
            //    base::dof_handler_pressure, 4, ZeroFunction<dim>(), bval);
            // outer circle
            //VectorTools::interpolate_boundary_values(
            //    base::dof_handler_pressure, 5, ZeroFunction<dim>(), bval);
        }
    }

    MatrixTools::apply_boundary_values(
        bval, base::pres_iterative, base::phi_n, base::pres_tmp);

    if (reinit_prec) {
        base::prec_pres_Laplace.initialize(
            base::pres_iterative,
            SparseILU<double>::AdditionalData(
                base::vel_diag_strength, base::vel_off_diagonals));
    }

    double solve_eps = base::vel_eps * base::pres_tmp.l2_norm();
    if (solve_eps < 1e-15) {
        solve_eps = base::vel_eps;
    }

    SolverControl solvercontrol(base::vel_max_its, solve_eps);
    SolverCG<> cg(solvercontrol);
    //  SolverGMRES<> cg (solvercontrol,
    //                       SolverGMRES<>::AdditionalData (vel_Krylov_size));

    cg.solve(
        base::pres_iterative,
        base::phi_n,
        base::pres_tmp,
        base::prec_pres_Laplace);

    //  phi_n.add(-phi_n.mean_value());

    base::phi_n *= 1.5 / base::dt;
}

template<int dim>
void NavierStokesEx35<dim>::update_pressure(const bool reinit_prec) {
    base::pres_n_minus_1 = base::pres_n;
    switch (type) {
        case METHOD_STANDARD:
            base::pres_n += base::phi_n;
            break;
        case METHOD_ROTATIONAL:
            if (reinit_prec)
                base::prec_mass.initialize(base::pres_Mass);
            base::pres_n = base::pres_tmp;
            base::prec_mass.solve(base::pres_n);
            base::pres_n.sadd(1. / base::Re, 1., base::pres_n_minus_1);
            base::pres_n += base::phi_n;
            break;
        default:
            Assert(false, ExcNotImplemented());
    };
}

} // namespace Task
