#pragma once
/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include <deal.II/base/function.h>
#include <deal.II/base/point.h>

namespace Task {
using namespace dealii;

typedef unsigned int uint;

template<int dim> class RotationalCylinder : public Function<dim> {
public:
    int direction;
    RotationalCylinder(int direction): direction(direction) {}
    virtual double value(const Point<dim>& p, const uint component) const;
};

template<int dim> class Velocity : public Function<dim> {
  public:
    virtual double value(const Point<dim>& p, const uint component = 0) const;
};

template<int dim> class Pressure : public Function<dim> {
  public:
    virtual double value(const Point<dim>& p, const uint component = 0) const;
};

template<int dim> class Force : public Function<dim> {
  public:
    int direction;
    bool sphere;
    Force(int direction, bool sphere = false)
        : direction(direction), sphere(sphere) {}
    virtual double value(const Point<dim>& p, const uint component = 0) const;
};

template<int dim> class Z0Test : public Function<dim> {
  public:
    int direction;
    Z0Test(int direction) : direction(direction) {}
    virtual double value(const Point<dim>& p, const uint component = 0) const;
};

template<int dim> class U0Test : public Function<dim> {
  public:
    int direction;
    U0Test(int direction) : direction(direction) {}
    virtual double value(const Point<dim>& p, const uint component = 0) const;
};

template<int dim> class AdvP1Test : public Function<dim> {
  public:
    int direction;
    AdvP1Test(int direction) : direction(direction) {}
    virtual double value(const Point<dim>& p, const uint component = 0) const;
};

template<int dim> class AdvP2Test : public Function<dim> {
  public:
    int direction;
    AdvP2Test(int direction) : direction(direction) {}
    virtual double value(const Point<dim>& p, const uint component = 0) const;
};

} // namespace Task

#include "initial-values-impl.h"
