/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include <deal.II/base/exceptions.h>
#include <deal.II/base/utilities.h>

#include <cmath>
#include <fstream>
#include <iostream>

#include "parameters.h"

namespace Task {

using namespace dealii;

Parameters::Parameters() {

    prm.declare_entry(
        "Method_Form",
        "rotational",
        Patterns::Selection("rotational|standard"),
        " Used to select the type of method that we are going "
        "to use. ");

    prm.enter_subsection("Physical data");
    {
        prm.declare_entry(
            "initial_time",
            "0.",
            Patterns::Double(0.),
            " The initial time of the simulation. ");
        prm.declare_entry(
            "final_time",
            "1.",
            Patterns::Double(0.),
            " The final time of the simulation. ");
        prm.declare_entry(
            "Reynolds", "1.", Patterns::Double(0.), " The Reynolds number. ");
    }
    prm.leave_subsection();

    prm.enter_subsection("Time step data");
    {
        prm.declare_entry(
            "dt", "5e-4", Patterns::Double(0.), " The time step size. ");
    }
    prm.leave_subsection();

    prm.enter_subsection("Space discretization");
    {
        prm.declare_entry(
            "mesh_refines",
            "2",
            Patterns::Integer(0, 15),
            " The number of global refines we do on the mesh. ");
        prm.declare_entry(
            "mesh_initial_cells",
            "128",
            Patterns::Integer(0, 32768),
            " The number of initial cells. ");
        prm.declare_entry(
            "inner_radius", "0.25", Patterns::Double(0.), " inner radius ");
        prm.declare_entry(
            "outer_radius", "1.25", Patterns::Double(0.), " outer radius ");
        prm.declare_entry(
            "z_length", "0.25", Patterns::Double(0.), " z length ");
        prm.declare_entry(
            "z_retentions", "3", Patterns::Integer(0, 32768), " z retentions ");
        prm.declare_entry(
            "pressure_degree",
            "1",
            Patterns::Integer(1, 5),
            " The polynomial degree for the pressure space. ");

        prm.declare_entry(
            "space_mode",
            "cylinder",
            Patterns::Selection("sphere|cylinder"),
            "  ");
    }
    prm.leave_subsection();

    prm.enter_subsection("Data solve velocity");
    {
        prm.declare_entry(
            "max_iterations",
            "1000",
            Patterns::Integer(1, 5000),
            " The maximal number of iterations GMRES must make. ");
        prm.declare_entry(
            "eps", "1e-8", Patterns::Double(0.), " The stopping criterion. ");
        prm.declare_entry(
            "Krylov_size",
            "30",
            Patterns::Integer(1),
            " The size of the Krylov subspace to be used. ");
        prm.declare_entry(
            "off_diagonals",
            "60",
            Patterns::Integer(0),
            " The number of off-diagonal elements ILU must "
            "compute. ");
        prm.declare_entry(
            "diag_strength",
            "0.01",
            Patterns::Double(0.),
            " Diagonal strengthening coefficient. ");
        prm.declare_entry(
            "update_prec",
            "15",
            Patterns::Integer(1),
            " This number indicates how often we need to "
            "update the preconditioner");
    }
    prm.leave_subsection();

    prm.enter_subsection("Spectral");
    {
        prm.declare_entry(
            "vectors",
            "10",
            Patterns::Integer(1, 1000),
            " The spectral vectors");
        prm.declare_entry(
            "z0_path", "null", Patterns::FileName(), " The spectral z0 path");
    }
    prm.leave_subsection();

    prm.declare_entry(
        "verbose",
        "true",
        Patterns::Bool(),
        " This indicates whether the output of the solution "
        "process should be verbose. ");

    prm.declare_entry(
        "output_interval",
        "1",
        Patterns::Integer(1),
        " This indicates between how many time steps we print "
        "the solution. ");
}

Parameters::~Parameters() {}

void Parameters::read_data(const char* filename) {
    std::ifstream file(filename);
    AssertThrow(file, ExcFileNotOpen(filename));

    prm.parse_input(file);

    if (prm.get("Method_Form") == std::string("rotational"))
        form = METHOD_ROTATIONAL;
    else
        form = METHOD_STANDARD;

    prm.enter_subsection("Physical data");
    {
        initial_time = prm.get_double("initial_time");
        final_time = prm.get_double("final_time");
        Reynolds = prm.get_double("Reynolds");
    }
    prm.leave_subsection();

    prm.enter_subsection("Time step data");
    { dt = prm.get_double("dt"); }
    prm.leave_subsection();

    prm.enter_subsection("Space discretization");
    {
        mesh_refines = prm.get_integer("mesh_refines");
        mesh_initial_cells = prm.get_integer("mesh_initial_cells");
        pressure_degree = prm.get_double("pressure_degree");
        inner_radius = prm.get_double("inner_radius");
        outer_radius = prm.get_double("outer_radius");
        z_length = prm.get_double("z_length");
        z_retentions = prm.get_integer("z_retentions");

        if (prm.get("space_mode") == "sphere") {
            use_sphere_in_3d = true;
            std::cout << "sphere\n";
        } else { /*cylinder*/
            use_sphere_in_3d = false;
            std::cout << "cyl\n";
        }
    }
    prm.leave_subsection();

    prm.enter_subsection("Data solve velocity");
    {
        vel_max_iterations = prm.get_integer("max_iterations");
        vel_eps = prm.get_double("eps");
        vel_Krylov_size = prm.get_integer("Krylov_size");
        vel_off_diagonals = prm.get_integer("off_diagonals");
        vel_diag_strength = prm.get_double("diag_strength");
        vel_update_prec = prm.get_integer("update_prec");
    }
    prm.leave_subsection();

    prm.enter_subsection("Spectral");
    {
        spectral_vectors = prm.get_integer("vectors");
        spectral_z0_path = prm.get("z0_path");
    }
    prm.leave_subsection();

    verbose = prm.get_bool("verbose");

    output_interval = prm.get_integer("output_interval");
}

} // namespace Task
