/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include <set>
#include <deal.II/base/config.h>

#ifdef DEAL_II_WITH_HDF5
#include <hdf5.h>
#include <hdf5_hl.h>
#endif

#include "initial-values.h"
#include "navier-stokes.h"

namespace Task {

template<int dim>
void NavierStokes<dim>::assemble_vorticity(const bool reinit_prec) {
    //  Assert (dim == 2, ExcNotImplemented());
    if (reinit_prec) {
        prec_vel_mass.initialize(vel_Mass);
    }

    FEValues<dim> fe_val_vel(
        fe_velocity,
        quadrature_velocity,
        update_gradients | update_JxW_values | update_values);
    const unsigned int dpc = fe_velocity.dofs_per_cell,
                       nqp = quadrature_velocity.size();
    std::vector<types::global_dof_index> ldi(dpc);
    Vector<double> loc_rot(dpc);

    std::vector<Tensor<1, dim>> grad_u[dim];

    for (int d = 0; d < dim; ++d) {
        grad_u[d].resize(nqp);
    }

    rot_u = 0.;

    //  for (uint d = 0; d < dim; ++d) {
    //    VectorTools::interpolate (dof_handler_velocity, Force<dim>(d, 1),
    //    u_n[d]);
    //  }

    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler_velocity.begin_active(),
        end = dof_handler_velocity.end();

    // TODO:
    int i1, i2;
    if (dim == 2) {
        i1 = 0;
        i2 = 1;
    } else {
        i1 = 0;
        i2 = 1;
    }
    for (; cell != end; ++cell) {
        fe_val_vel.reinit(cell);
        cell->get_dof_indices(ldi);
        for (uint d = 0; d < dim; ++d) {
            fe_val_vel.get_function_gradients(u_n[d], grad_u[d]);
        }
        loc_rot = 0.;
        for (unsigned int q = 0; q < nqp; ++q)
            for (unsigned int i = 0; i < dpc; ++i)
                loc_rot(i) += (grad_u[i2][q][i1] - grad_u[i1][q][i2]) *
                              fe_val_vel.shape_value(i, q) * fe_val_vel.JxW(q);

        for (unsigned int i = 0; i < dpc; ++i)
            rot_u(ldi[i]) += loc_rot(i);
    }

    psi_u_tmp = rot_u;
    prec_vel_mass.solve(rot_u);

#if 1
    static std::map<types::global_dof_index, double> bval;

    if (reinit_prec) {
        psi_iterative.reinit(sparsity_pattern_velocity);
    }

    psi_iterative.copy_from(vel_Laplace);

    if (reinit_prec) {
        if (data.use_sphere_in_3d) {
            // rotating sphere

            VectorTools::interpolate_boundary_values(
                dof_handler_velocity, 0, ZeroFunction<dim>(), bval);

        } else {
            //VectorTools::interpolate_boundary_values(
            //    dof_handler_velocity, 1, ZeroFunction<dim>(), bval);

            VectorTools::interpolate_boundary_values(
                dof_handler_velocity, 2, ZeroFunction<dim>(), bval);

            VectorTools::interpolate_boundary_values(
                dof_handler_velocity, 3, ZeroFunction<dim>(), bval);

            VectorTools::interpolate_boundary_values(
                dof_handler_velocity, 4, ZeroFunction<dim>(), bval);

            VectorTools::interpolate_boundary_values(
                dof_handler_velocity, 5, ZeroFunction<dim>(), bval);
        }
    }

    MatrixTools::apply_boundary_values(bval, psi_iterative, psi_u, psi_u_tmp);

    if (reinit_prec) {
        prec_psi_iterative.initialize(
            psi_iterative,
            SparseILU<double>::AdditionalData(
                vel_diag_strength, vel_off_diagonals));
    }

    double solve_eps = vel_eps * pres_tmp.l2_norm();
    if (solve_eps < 1e-15) {
        solve_eps = vel_eps;
    }

    SolverControl solvercontrol(vel_max_its, solve_eps);
    SolverCG<> cg(solvercontrol);

    cg.solve(psi_iterative, psi_u, psi_u_tmp, prec_psi_iterative);
#endif
}

template<int dim>
void NavierStokes<dim>::output_results(int step, const std::string& prefix) {
    assemble_vorticity((step == 1));
    const FESystem<dim> joint_fe(
        fe_velocity, dim, fe_pressure, 1, fe_velocity, 1);
    DoFHandler<dim> joint_dof_handler(triangulation);
    joint_dof_handler.distribute_dofs(joint_fe);
    Assert(
        joint_dof_handler.n_dofs() ==
            ((dim + 1) * dof_handler_velocity.n_dofs() +
             dof_handler_pressure.n_dofs()),
        ExcInternalError());
    static Vector<double> joint_solution(joint_dof_handler.n_dofs());
    std::vector<types::global_dof_index> loc_joint_dof_indices(
        joint_fe.dofs_per_cell),
        loc_vel_dof_indices(fe_velocity.dofs_per_cell),
        loc_pres_dof_indices(fe_pressure.dofs_per_cell);
    typename DoFHandler<dim>::active_cell_iterator
        joint_cell = joint_dof_handler.begin_active(),
        joint_endc = joint_dof_handler.end(),
        vel_cell = dof_handler_velocity.begin_active(),
        pres_cell = dof_handler_pressure.begin_active();

    for (; joint_cell != joint_endc; ++joint_cell, ++vel_cell, ++pres_cell) {
        joint_cell->get_dof_indices(loc_joint_dof_indices);
        vel_cell->get_dof_indices(loc_vel_dof_indices);
        pres_cell->get_dof_indices(loc_pres_dof_indices);
        for (unsigned int i = 0; i < joint_fe.dofs_per_cell; ++i)
            switch (joint_fe.system_to_base_index(i).first.first) {
                case 0:
                    Assert(
                        joint_fe.system_to_base_index(i).first.second < dim,
                        ExcInternalError());
                    joint_solution(loc_joint_dof_indices[i]) =
                        u_n[joint_fe.system_to_base_index(i).first.second](
                            loc_vel_dof_indices[joint_fe.system_to_base_index(i)
                                                    .second]);
                    break;
                case 1:
                    Assert(
                        joint_fe.system_to_base_index(i).first.second == 0,
                        ExcInternalError());
                    joint_solution(loc_joint_dof_indices[i]) = pres_n(
                        loc_pres_dof_indices[joint_fe.system_to_base_index(i)
                                                 .second]);
                    break;
                case 2:
                    Assert(
                        joint_fe.system_to_base_index(i).first.second == 0,
                        ExcInternalError());
                    joint_solution(loc_joint_dof_indices[i]) = psi_u(
                        loc_vel_dof_indices[joint_fe.system_to_base_index(i)
                                                .second]);
                    break;
                default:
                    Assert(false, ExcInternalError());
            }
    }
    std::vector<std::string> joint_solution_names(dim, "v");
    joint_solution_names.push_back("p");
    joint_solution_names.push_back("rot_u");
    DataOut<dim> data_out;
    data_out.attach_dof_handler(joint_dof_handler);
    std::vector<DataComponentInterpretation::DataComponentInterpretation>
        component_interpretation(
            dim + 2, DataComponentInterpretation::component_is_part_of_vector);
    component_interpretation[dim] =
        DataComponentInterpretation::component_is_scalar;
    component_interpretation[dim + 1] =
        DataComponentInterpretation::component_is_scalar;

    //  std::cout << component_interpretation.size() << "\n";
    //  std::cout << joint_solution_names.size() << "\n";
    data_out.add_data_vector(
        joint_solution,
        joint_solution_names,
        DataOut<dim>::type_dof_data,
        component_interpretation);
    data_out.build_patches(deg + 1);

    if (1) {
        std::ofstream output(
            (prefix + "-" + Utilities::int_to_string(step, 5) + ".vtk")
                .c_str());
        data_out.write_vtk(output);
    }
#ifdef DEAL_II_WITH_HDF5
    if (1) {
        //    std::ofstream output (("solution-" +
        //                           Utilities::int_to_string (step, 5) +
        //                           ".h5").c_str());

        DataOutBase::DataOutFilter data_filter(
            DataOutBase::DataOutFilterFlags(true, true));
        //    std::ofstream output (("solution-" +
        //                           Utilities::int_to_string (step, 5) +
        //                           ".h5").c_str());
        data_out.write_filtered_data(data_filter);

        std::string fn =
            prefix + "-" + Utilities::int_to_string(step, 5) + ".h5";
        //    std::string fn = "solution.h5";
        data_out.write_hdf5_parallel(data_filter, fn, MPI_COMM_WORLD);

        // Create an XDMF entry detailing the HDF5 file
        xdmf_entries.push_back(data_out.create_xdmf_entry(
            data_filter, fn, step * dt, MPI_COMM_WORLD));

        // Create an XDMF file from all stored entries
        data_out.write_xdmf_file(
            xdmf_entries, prefix + ".h5.xdmf", MPI_COMM_WORLD);
        //    data_out.write (output, DataOutBase::OutputFormat::hdf5);
    }
#endif
}

template<int dim>
void NavierStokes<dim>::output_scalar(
    const std::string& fname, DoFHandler<dim>& dof, Vector<double>& scalar) {
    std::ofstream output1(fname);
    DataOut<dim> data_out;
    data_out.attach_dof_handler(dof);

    std::vector<std::string> solution_names(1, "scalar");
    std::vector<DataComponentInterpretation::DataComponentInterpretation>
        component_interpretation(
            1, DataComponentInterpretation::component_is_scalar);
    data_out.add_data_vector(
        scalar,
        solution_names,
        DataOut<dim>::type_dof_data,
        component_interpretation);
    data_out.build_patches();
    data_out.write_vtk(output1);
}

template<int dim>
void NavierStokes<dim>::output_vector(
    const std::string& fname, DoFHandler<dim>& dof, Vector<double>* vector) {
    std::ofstream output1(fname);
    DataOut<dim> data_out;
    data_out.attach_dof_handler(dof);

    std::vector<std::string> solution_names;
    std::vector<DataComponentInterpretation::DataComponentInterpretation>
        component_interpretation;

    for (unsigned int i = 0; i < dim; ++i) {
        solution_names.push_back("u");
        component_interpretation.push_back(
            DataComponentInterpretation::component_is_part_of_vector);
    }

    Vector<double> tmp;
    tmp.reinit(dof.n_dofs() * dim);
    for (uint i = 0; i < dim; ++i) {
        for (uint j = 0; j < dof.n_dofs(); ++j) {
            tmp[dim * i + j] = vector[i][j];
        }
    }

    data_out.add_data_vector(
        tmp,
        solution_names,
        DataOut<dim>::type_dof_data,
        component_interpretation);

    data_out.build_patches();
    data_out.write_vtk(output1);
}

template<int dim> void NavierStokes<dim>::read_data(const std::string& fn) {
#ifdef DEAL_II_WITH_HDF5
    hid_t file;
    hsize_t dims[2];
    std::vector<double> tmp;
    std::vector<double> u;
    std::vector<double> v;

    int nodes_count;
    std::vector<double> nodes;
    int cells_count;
    std::vector<int> cellsraw;

    std::vector<Point<dim>> vertices;
    std::vector<CellData<dim>> cells;

    int dofs;

    file = H5Fopen(fn.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);

    {
        // nodes
        H5LTget_dataset_info(file, "/nodes", dims, NULL, NULL);
        nodes_count = dims[0];
        nodes.resize(dims[0] * dims[1]);
        H5LTread_dataset_double(file, "/nodes", &nodes[0]);
    }

    {
        // cells
        H5LTget_dataset_info(file, "/cells", dims, NULL, NULL);
        cells_count = dims[0];
        cellsraw.resize(dims[0] * dims[1]);
        H5LTread_dataset_int(file, "/cells", &cellsraw[0]);
    }

    vertices.reserve(nodes_count);
    for (int i = 0; i < nodes_count; ++i) {
        vertices.push_back(Point<dim>(nodes[i * 2 + 0], nodes[i * 2 + 1]));
    }
    cells.reserve(cells_count);

    int vdofs = dof_handler_velocity.n_dofs();
    MappingQ1<dim, dim> mapping;
    std::vector<Point<dim, double>> dof_coords(vdofs);
    DoFTools::map_dofs_to_support_points<dim, dim>(
        mapping, dof_handler_velocity, dof_coords);
#endif

#if 0
  struct Line {
    int a;
    int b;
    bool order;

    Line(int a1, int b1):
      a(a1 < b1 ? a1 : b1),
      b(a1 < b1 ? b1 : a1),
      order(a1 < b1) {}

    bool operator < (const Line & l) const {
      if (a < l.a) {
        return true;
      } else if (a > l.a) {
        return false;
      } else {
        if (b < l.b) {
          return true;
        } else if (b > l.b) {
          return false;
        }
      }
      return false;
    }
  };

  std::set<Line> lines;
  
  for (int i = 0; i < cells_count; ++i) {
    CellData<dim> d;

    bool order = true;
    
    for (int j = 0; j < 4; ++j) {
      int a = cellsraw[i * 4 + j];
      int b = cellsraw[i * 4 + (j + 1) % 4];
      Line l(a, b);
      std::set<Line>::iterator it = lines.find(l);
      if (it != lines.end()) {
        if (it->order != l.order) {
          order = false;
          break;
        }
      }
    }

    if (order) {
      for (int j = 0; j < 4; ++j) {
        int a = cellsraw[i * 4 + j];
        int b = cellsraw[i * 4 + (j + 1) % 4];

        lines.insert(Line(a, b));
        d.vertices[j] = a;
      }
    } else {
      for (int j = 3; j >= 0; --j) {
        int a = cellsraw[i * 4 + j];
        int b = cellsraw[i * 4 + (4 + j - 1) % 4];

        lines.insert(Line(a, b));
        d.vertices[3-j] = a;
      }
    }
    cells.push_back(d);
  }

  Triangulation<dim> in_trian;
  in_trian.create_triangulation (vertices,
                                 cells,
                                 SubCellData());
#endif

#ifdef DEAL_II_WITH_HDF5
    H5LTget_dataset_info(file, "/cells", dims, NULL, NULL);

    H5LTget_dataset_info(file, "/v", dims, NULL, NULL);

    tmp.resize(dims[0] * dims[1]);
    H5LTread_dataset_double(file, "/v", &tmp[0]);
    dofs = dims[0];
    u.resize(dims[0]);
    v.resize(dims[0]);

    //  for (uint i = 0; i < dims[0]; ++i) {
    //    for (uint j = 0; j < dims[1]; ++j) {
    //      std::cerr << tmp[i * dims[1] + j] << " ";
    //    }
    //    std::cerr << "\n";
    //  }

    //  std::cout << vdofs << " " << dofs << "\n";
    Assert(vdofs == dofs, ExcInternalError());

    for (int i = 0; i < vdofs; ++i) {
        Point<dim>& p = dof_coords[i];
        int number = -1;
        for (int j = 0; j < vdofs; ++j) {
            if (p.distance(vertices[j]) < 1e-10) {
                number = j;
                break;
            }
        }
        Assert(number >= 0, ExcInternalError());
        u[i] = tmp[number * dims[1] + 0];
        v[i] = tmp[number * dims[1] + 1];
    }
    //  for (int i = 0; i < vdofs; ++i) {
    //    std::cout << u[i] << " " << v[i] << "\n";
    //  }
    set_u(&u[0], &v[0]);

    H5Fclose(file);
#endif
}

} // namespace Task
