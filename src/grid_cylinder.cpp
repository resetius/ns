//
// Created by aozeritsky on 07.05.19.
//

#include <deal.II/base/point.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/manifold_lib.h>

#define _USE_MATH_DEFINES

#include <string>
#include <iostream>
#include <fstream>

#include <math.h>

using namespace dealii;
using namespace std;

template<int dim>
void doall(int mesh_refines, const string& outbase)
{
    Triangulation<dim> triangulation;

    const double length = 2*M_PI;
    const double inner_radius = M_PI/2.;
    const double outer_radius = 3.*M_PI/2.;

    // bool colorize = true;

    // If the colorize flag is true, the boundary_ids of the boundary faces are assigned, such that the lower one in x-direction is 0, the upper one is 1.
    // The indicators for the surfaces in y-direction are 2 and 3, the ones for z are 4 and 5.
    // This corresponds to the numbers of faces of the unit square of cube as laid out in the documentation of the GeometryInfo class.
    // Importantly, however, in 3d colorization does not set boundary_ids of edges, but only of faces,
    // because each boundary edge is shared between two faces and it is not clear how the boundary id of an edge should be set in that case.

    // todo: colorize parameter patch
    GridGenerator::cylinder_shell(
        triangulation, length, inner_radius, outer_radius,
        4, // radial cells
        1 // axial cells
        );

    triangulation.refine_global(mesh_refines);

    typename Triangulation<dim>::active_cell_iterator
        cell = triangulation.begin_active(),
        endc = triangulation.end();

    const CylindricalManifold<dim> boundary_description(2); // 2 - z axis

    std::unordered_map<int, int> boundaries;
    triangulation.set_manifold(0, boundary_description);
    for (; cell != endc; ++cell) {
        cell->set_all_manifold_ids(0);
        for (uint f = 0; f < GeometryInfo<dim>::faces_per_cell; ++f) {
            auto face = cell->face(f);
            vector<int> bids(GeometryInfo<dim-1>::vertices_per_cell+5);
            int b = 0;
            for (uint v = 0; v < GeometryInfo<dim-1>::vertices_per_cell; ++v) {
                Point<dim> p = face->vertex(v);
                double x = p(0);
                double y = p(1);
                double z = p(2);

                double r = sqrt(x*x+y*y);
                std::cerr << x << " " << y << " " << z << " ";

                if (fabs(z - 0) < 1e-5) {
                    // bottom
                    bids[3]++;
                    std::cerr << 3 << " " << std::endl;
                    b++;
                } else if (fabs(z - length) < 1e-5) {
                    // top
                    bids[2]++;
                    std::cerr << 2 << " " << std::endl;
                    b++;
                }
                // circles
                if (fabs(r - inner_radius) < 1e-5) {
                    // inner
                    bids[4]++;
                    std::cerr << 4 << " " << std::endl;
                    b++;
                } else {
                    // outer
                    bids[5]++;
                    std::cerr << 5 << " " << std::endl;
                    b++;
                }
            }
            std::cerr << std::endl;
            if (face->boundary_id() == 0 && b >= (int) GeometryInfo<dim-1>::vertices_per_cell) {
                int maxId = -1;
                int maxB = -1;
                for (int i = 0; i < (int)bids.size(); ++i) {
                    if (bids[i] > 0) {
                        if (maxB < bids[i]) {
                            maxB = bids[i];
                            maxId = i;
                        }
                    }
                }
                if (face->boundary_id() == 0) {
                    boundaries[maxId]++;
                    face->set_boundary_id(maxId);
                }
            }
        }
    }

    for (const auto& [k, v] : boundaries) {
        std::cerr << k << " : " << v << std::endl;
    }

    GridOut out;

    {
        ofstream output_file(outbase + ".vtk");
        out.write_vtk(triangulation, output_file);
    }

    { // save with boundary ids
        ofstream output_file(outbase + ".ucd");
        out.set_flags (GridOutFlags::Ucd(true, true, true));
        out.write_ucd(triangulation, output_file);
    }

    if (0) {
        GridOutFlags::Svg flags;
        // flags.label_material_id = true;
        ofstream output_file(outbase + ".svg");
        out.set_flags(flags);
        out.write_svg(triangulation, output_file);
    }

    {
        ofstream output_file(outbase + ".eps");
        out.write_eps(triangulation, output_file);
    }
}

int main(int argc, char** argv) {
    int mesh_refines = 2;
    string outbase = "cyl.grid";
    if (argc > 1) {
        mesh_refines = atoi(argv[1]);
    }
    if (argc > 2) {
        outbase = argv[2];
    }
    doall<3>(mesh_refines, outbase);
    return 0;
}
