#pragma once
/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include "navier-stokes.h"

namespace Task {

using namespace dealii;
typedef unsigned int uint;

class NavierStokesLin : public NavierStokes2D {
public:
    FESystem<dim> lin_joint_fe;
    DoFHandler<dim> lin_joint_dof_handler;

    Vector<double> uv1;
    Vector<double> uv2;
    Vector<double> hu[dim];
    Vector<double> pu;
    Vector<double> zu[dim];
    Vector<double> zp;

    // must be declared before matrices
    SparsityPattern sparsity_pattern_lin;
    SparsityPattern sparsity_pattern_lin_adv;

    SparseMatrix<double> vel_lin_Advection;
    SparseMatrix<double> lin;

    explicit NavierStokesLin(const Parameters& data);

    void assemble_advection_term() override;
    void copy_advection_local_to_global(const AdvectionPerTaskData& data) override;
    void assemble_one_cell_of_advection(
        const typename DoFHandler<dim>::active_cell_iterator& cell,
        AdvectionScratchData& scratch,
        AdvectionPerTaskData& data) override;

    void initialize() override;

    void mass();
    void mass_solve();
    void vmult_mass(Vector<double>& dst, const Vector<double>& src) const override;
    void spectral() override;

    void test_adv();

    void run(const bool verbose, const unsigned int output_interval) override;
};
} // namespace Task
