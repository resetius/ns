/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set ts=4 sw=4 et: */

#include "initial-values.h"
#include "navier-stokes-poisson.h"

namespace Task {

using namespace dealii;
typedef unsigned int uint;

// http://www.math.tamu.edu/~guermond/PUBLICATIONS/guermond_quartapelle_Num_math_1998.pdf
template<int dim>
NavierStokesPoisson<dim>::NavierStokesPoisson(const Parameters& data)
    : NavierStokes<dim>(data) {
    initialize();
}

template<int dim> void NavierStokesPoisson<dim>::initialize() {
    base::vel_Laplace_plus_Mass.reinit(base::sparsity_pattern_velocity);

    base::vel_Laplace_plus_Mass = 0.;
    base::vel_Laplace_plus_Mass.add(1. / base::Re, base::vel_Laplace);
    base::vel_Laplace_plus_Mass.add(1. / base::dt, base::vel_Mass);
}

template<int dim>
void NavierStokesPoisson<dim>::diffusion_step(const bool reinit_prec) {
    base::pres_tmp.equ(-1., base::pres_n);

    base::assemble_advection_term();

    for (unsigned int d = 0; d < dim; ++d) {
        base::force[d] = 0;
        base::v_tmp.equ(1. / base::dt, base::u_n[d]);
        base::v_tmp.add(1.0, base::F[d]);
        base::vel_Mass.vmult_add(base::force[d], base::v_tmp);
        base::pres_Diff[d].vmult_add(base::force[d], base::pres_tmp);

        base::vel_it_matrix[d].copy_from(base::vel_Laplace_plus_Mass);
        base::vel_it_matrix[d].add(1., base::vel_Advection);

        base::boundary_values.clear();
        for (std::vector<types::boundary_id>::const_iterator boundaries =
                 base::boundary_ids.begin();
             boundaries != base::boundary_ids.end();
             ++boundaries) {
            switch (*boundaries) {
                case 0:
                    // inner
                    VectorTools::interpolate_boundary_values(
                        base::dof_handler_velocity,
                        *boundaries,
                        ZeroFunction<dim>(),
                        base::boundary_values);
                    break;
                case 1:
                    // outer
                    if (base::data.use_sphere_in_3d) {
                        // rotating sphere
                        VectorTools::interpolate_boundary_values(
                            base::dof_handler_velocity,
                            *boundaries,
                            Force<dim>(d, base::data.use_sphere_in_3d),
                            base::boundary_values);
                    } else {
                        VectorTools::interpolate_boundary_values(
                            base::dof_handler_velocity,
                            *boundaries,
                            ZeroFunction<dim>(),
                            base::boundary_values);
                    }
                    break;
                default:
                    // neuman
                    break;
            }
        }
        MatrixTools::apply_boundary_values(
            base::boundary_values,
            base::vel_it_matrix[d],
            base::u_n[d],
            base::force[d]);

        //    std::cout << " dd " << d << "\n";
        //    base::vel_it_matrix[d].print_formatted(std::cout);
    }

    Threads::TaskGroup<void> tasks;
    for (unsigned int d = 0; d < dim; ++d) {
        if (reinit_prec)
            base::prec_velocity[d].initialize(
                base::vel_it_matrix[d],
                SparseILU<double>::AdditionalData(
                    base::vel_diag_strength, base::vel_off_diagonals));
        tasks += Threads::new_task(
            &NavierStokes<dim>::diffusion_component_solve, *this, d);
    }
    tasks.join_all();

    for (uint d = 0; d < dim; ++d) {
        base::u_star[d] = base::u_n[d];
    }
}

template<int dim>
void NavierStokesPoisson<dim>::projection_step(const bool reinit_prec) {
    base::pres_iterative.copy_from(base::pres_Laplace);

    base::pres_tmp = 0.; // rp

    for (unsigned d = 0; d < dim; ++d) {
        base::pres_Diff[d].Tvmult_add(base::pres_tmp, base::u_n[d]);
    }
    base::pres_tmp *= 1.0 / base::dt;
    static std::map<types::global_dof_index, double> bval;
    if (reinit_prec) {
        // bval
        // bval[0] = 0;
        bval.clear();
        //    VectorTools::
        //      interpolate_boundary_values (base::dof_handler_pressure,
        //                                   0,
        //                                   ZeroFunction<dim>(),
        //                                   base::boundary_values);

        //    VectorTools::
        //      interpolate_boundary_values (base::dof_handler_pressure,
        //                                   1,
        //                                   ZeroFunction<dim>(),
        //                                  base::boundary_values);

        for (std::vector<types::boundary_id>::const_iterator boundaries =
                 base::boundary_ids.begin();
             boundaries != base::boundary_ids.end();
             ++boundaries) {
            switch (*boundaries) {
                case 3:
                    // bottom in 3D
                case 2:
                    // top in 3D
                    VectorTools::interpolate_boundary_values(
                        base::dof_handler_pressure,
                        *boundaries,
                        ZeroFunction<dim>(),
                        bval);
                    break;
            }
        }
    }

    MatrixTools::apply_boundary_values(
        bval, base::pres_iterative, base::phi_n, base::pres_tmp);

    //  std::cout << " prec " << "\n";
    //  base::pres_iterative.print_formatted(std::cout);
    if (reinit_prec) {
        base::prec_pres_Laplace.initialize(
            base::pres_iterative,
            SparseILU<double>::AdditionalData(
                base::vel_diag_strength, base::vel_off_diagonals));
    }
    //  std::cout << "solve " << base::vel_eps << " " <<
    //  base::pres_tmp.l2_norm() << "\n";
    double solve_eps = base::vel_eps * base::pres_tmp.l2_norm();
    if (solve_eps < 1e-15) {
        solve_eps = base::vel_eps;
    }
    //  solve_eps = base::vel_eps;
    SolverControl solvercontrol(base::vel_max_its, solve_eps);
    //  SolverGMRES<> cg (solvercontrol,
    //                    SolverGMRES<>::AdditionalData
    //                    (base::vel_Krylov_size));
    SolverCG<> cg(solvercontrol);
    cg.solve(
        base::pres_iterative,
        base::phi_n,
        base::pres_tmp,
        base::prec_pres_Laplace);
    //  std::cout << " done \n";
    base::pres_n += base::phi_n;

    base::pres_tmp.equ(base::dt, base::phi_n);

    if (reinit_prec) {
        base::prec_mass.initialize(base::vel_Mass);
    }

    Threads::TaskGroup<void> tasks;

    for (uint d = 0; d < dim; ++d) {
    // u_n[d] = u_star[d] - dt grad(phi_n)[d]

#if 0
    force[d] = 0.0;
    pres_Diff[d].vmult_add (force[d], pres_tmp);
    prec_mass.solve(force[d]);
    u_n[d]  = u_star[d];
    u_n[d] -= force[d];
#endif

        tasks += Threads::new_task(
            &NavierStokesPoisson<dim>::velocity_component_solve, *this, d);
    }

    tasks.join_all();
}

template<int dim>
void NavierStokesPoisson<dim>::velocity_component_solve(const uint d) {
    base::force[d] = 0.0;
    base::pres_Diff[d].vmult_add(base::force[d], base::pres_tmp);
    base::prec_mass.solve(base::force[d]);
    base::u_n[d] = base::u_star[d];
    base::u_n[d] -= base::force[d];
}

template<int dim>
void NavierStokesPoisson<dim>::run(
    const bool verbose, const unsigned int output_interval) {
    ConditionalOStream verbose_cout(std::cout, verbose);

    const unsigned int n_steps =
        static_cast<unsigned int>((base::T - base::t_0) / base::dt);
    std::cout << "u size : " << base::dof_handler_velocity.n_dofs() << "\n";
    std::cout << "p size : " << base::dof_handler_pressure.n_dofs() << "\n";
    //  std::cout << "poisson " << "\n";

    base::output_results(1);

    for (uint d = 0; d < dim; ++d) {
        base::u_star[d] = base::u_n[d];
    }

    for (unsigned int n = 2; n <= n_steps; ++n) {
        if (n % output_interval == 0) {
            verbose_cout << "Plotting Solution" << std::endl;
            base::output_results(n);
        }

        std::cout << "Step = " << n << " Time = " << (n * base::dt)
                  << std::endl;
        verbose_cout << "  Interpolating the velocity " << std::endl;

        //    interpolate_velocity();
        verbose_cout << "  Diffusion Step" << std::endl;
        if (n % base::vel_update_prec == 0)
            verbose_cout << "    With reinitialization of the preconditioner"
                         << std::endl;
        diffusion_step((n % base::vel_update_prec == 0) || (n == 2));
        verbose_cout << "  Projection Step" << std::endl;
        projection_step((n == 2));
        verbose_cout << "  Updating the Pressure" << std::endl;
        //    update_pressure ( (n == 2));
    }

    base::output_results(n_steps);
}

} // namespace Task
