#pragma once

template<int dim>
class FunctionProj : public Function<dim> {
    const Function<dim>& func;
    const int direction;

public:
    FunctionProj(const Function<dim>& func, int direction)
        : func(func)
        , direction(direction)
    { }

    virtual double value(const Point<dim>& p, const uint) const override
    {
        return func.value(p, direction);
    }
};

template<int dim>
FunctionProj<dim> funcProj(const Function<dim>& func, int direction)
{
    return FunctionProj<dim>(func, direction);
}
