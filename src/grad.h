#pragma once

#include <vector>

#include "grad.h"

template <int dim>
class Grad
{
    const ModelBase<dim>& base;
    Div<dim> div;

public:
    Grad(const ModelBase<dim>& base)
        : base(base)
        , div(base)
    { }

    void initialize()
    {
    }

    void grad(Vector<double>& output, Vector<double>& input, int component) {
        assert(component < dim);
        div.div(output, input, component);
    }

    void grad(Vector<double>& output, Vector<double>& input) {
        int n_dofs = base.dof().n_dofs();

        std::vector<double> tmp;
        tmp.reserve(n_dofs*3);

        for (int i = 0; i < dim; ++i) {
            Vector<double> u(n_dofs);
            div.div(u, input, i);

            tmp.insert(tmp.end(), u.begin(), u.end());
        }

        output = Vector<double>(tmp.begin(), tmp.end());
    }
};
