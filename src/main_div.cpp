//
// Created by aozeritsky on 16.05.19.
//

#include "div.h"

#include <deal.II/numerics/vector_tools.h>

#include <math.h>

#include <iostream>
#include <vector>

using namespace std;

// - cos(x) sin(y) e^{-2t}
//   sin(x) cos(y) e^{-2t}
template<int dim>
class InitialValue : public Function<dim> {
    int direction;
    double t;

public:

    InitialValue(int direction, double t = 0)
        : direction(direction)
        , t(t)
    { }

    virtual double value(const Point<dim>& p, const uint) const override
    {
        double x = p(0);
        double y = p(1);
        if (direction == 0) {
            return -31.145*cos(x)*sin(y)*exp(t);
        } else {
            assert(0);
        }

        return -1;
    }
};

template<int dim>
class ExactSolution : public Function<dim> {
    int direction;
    double t;

public:

    explicit ExactSolution(int direction, double t = 0)
        : direction(direction)
        , t(t)
    { }

    virtual double value(const Point<dim>& p, const uint) const override
    {
        double x = p(0);
        double y = p(1);
        if (direction == 0) {
            return  31.145*sin(x)*sin(y)*exp(t);
        } else {
            return -31.145*cos(x)*cos(y)*exp(t);
        }
    }
};

int main(int argc, char** argv)
{
    if (argc < 2) {
        cerr << "Usage: " << argv[0] << " filename\n";
        return -1;
    }
    string gridinput = argv[1];

    static const int femDeg = 2;
    static const int dim = 2;

    Grid<dim> grid;
    grid.load(gridinput);

    ModelBase<dim> base(grid, femDeg);
    base.initialize();

    Div<dim> div(base);

    div.initialize();

    auto& dof = base.dof();
    int n_dofs = dof.n_dofs();

    cout << "n_dofs " << n_dofs << "\n";

    {
        Vector<double> input(n_dofs);
        Vector<double> output(n_dofs);

        VectorTools::interpolate(
            dof,
            InitialValue<dim>(0),
            input);

        div.div(output, input, 0);

        Vector<double> difference;
        VectorTools::integrate_difference(
            dof,
            output,
            ExactSolution<dim>(0),
            difference,
            base.q(),
            VectorTools::L2_norm);
/*
        for (auto& v : difference) {
            cout << " > " << v << "\n";
        }
*/
        double norm = VectorTools::compute_global_error(
            grid.triangulation(),
            difference,
            VectorTools::H1_seminorm);

        printf("%.16le\n", norm);

        div.div(output, input, 1);

        difference = 0;
        VectorTools::integrate_difference(
            dof,
            output,
            ExactSolution<dim>(1),
            difference,
            base.q(),
            VectorTools::L2_norm);
/*
        for (auto& v : difference) {
            cout << " > " << v << "\n";
        }
*/
        norm = VectorTools::compute_global_error(
            grid.triangulation(),
            difference,
            VectorTools::H1_seminorm);

        printf("%.16le\n", norm);
    }

    return 0;
}
